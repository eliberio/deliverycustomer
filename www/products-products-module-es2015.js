(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["products-products-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/products/products.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/products/products.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar *ngIf=\"!config.appNavigationTabs\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title> {{'Shop'| translate }} </ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n        <ion-icon name=\"basket\"></ion-icon>\r\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n  <ion-toolbar class=\"segmentToolbar\" *ngIf=\"!config.appNavigationTabs\">\r\n    <ion-slides [options]=\"sliderConfig\" [class.disable]=\"products[0]==1\">\r\n      <ion-slide [class.selected]=\"selectedTab==''\" *ngIf=\"shared.allCategories!=null\" (click)=\"changeTab('')\">\r\n        {{'All'|translate}}</ion-slide>\r\n      <ion-slide [class.selected]=\"selectedTab==c.id\" *ngFor=\"let c of shared.allCategories\" (click)=\"changeTab(c)\">\r\n        {{c.name}}\r\n      </ion-slide>\r\n    </ion-slides>\r\n  </ion-toolbar>\r\n\r\n  <ion-toolbar class=\"segmentToolbar\" *ngIf=\"config.appNavigationTabs\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-slides [options]=\"sliderConfig\" [class.disable]=\"products[0]==1\">\r\n      <ion-slide [class.selected]=\"selectedTab==''\" *ngIf=\"shared.allCategories!=null\" (click)=\"changeTab('')\">\r\n        {{'All'|translate}}</ion-slide>\r\n      <ion-slide [class.selected]=\"selectedTab==c.id\" *ngFor=\"let c of shared.allCategories\" (click)=\"changeTab(c)\">\r\n        {{c.name}}\r\n      </ion-slide>\r\n    </ion-slides>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [scrollEvents]=\"true\" (ionScroll)=\"onScroll($event)\" id=\"productContent\">\r\n  <ion-grid *ngIf=\"productView=='grid'\" class=\"ion-no-padding\">\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col *ngFor=\"let p of products\" size=\"6\" class=\"ion-no-padding\">\r\n        <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <ion-list class=\"list-view\" *ngIf=\"productView=='list'\">\r\n    <span *ngFor=\"let p of products\">\r\n      <app-product [data]=\"p\" [type]=\"'list'\"></app-product>\r\n    </span>\r\n  </ion-list>\r\n  <ion-col *ngIf=\"products.length==0 && !httpRunning\" size=\"12\" class=\"animated fadeIn\">\r\n    <h6 class=\"ion-text-center\">{{'No Products Found'|translate}}</h6>!\r\n  </ion-col>\r\n\r\n\r\n  <!-- infinite scroll -->\r\n  <ion-infinite-scroll threshold=\"10px\" #infinite (ionInfinite)=\"getProducts($event)\">\r\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\r\n  </ion-infinite-scroll>\r\n</ion-content>\r\n<ion-fab vertical=\"bottom\" horizontal=\"end\" *ngIf=\"scrollTopButton\">\r\n  <ion-fab-button (click)=\"scrollToTop()\">\r\n    <ion-icon name=\"arrow-up\"></ion-icon>\r\n  </ion-fab-button>\r\n</ion-fab>\r\n<ion-footer>\r\n  <ion-toolbar color=\"light\">\r\n    <ion-item (click)=\"openSortBy()\" no-lines>\r\n      <ion-label>\r\n        <p>{{\"Sort Products\"|translate}}</p>\r\n        <h3 color=\"secondary\">\r\n          {{sortOrder| translate}}<ion-icon name=\"arrow-up\"></ion-icon>\r\n        </h3>\r\n      </ion-label>\r\n    </ion-item>\r\n\r\n    <ion-buttons slot=\"end\" color=\"light\">\r\n      <ion-button (click)=\"changeLayout()\">\r\n        <ion-icon name=\"ios-list\" [name]=\"productView=='grid'? 'list' : 'apps'\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button (click)=\"removeFilters()\" *ngIf=\"applyFilter\">\r\n        <ion-icon name=\"refresh\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button (click)=\"toggleMenu()\">\r\n        <ion-icon name=\"funnel\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n\r\n<ion-menu side=\"end\" menuId=\"menu2\" contentId=\"productContent\">\r\n  <ion-header>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-button slot=\"icon-only\" (click)=\"toggleMenu()\">\r\n          <ion-icon name=\"close\"></ion-icon>\r\n        </ion-button>\r\n      </ion-buttons>\r\n      <ion-title>{{\"Filters\"|translate}}</ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n  <ion-content class=\"side-menu ion-no-padding\">\r\n    <ion-card *ngIf=\"!hidePriceRange\">\r\n      <ion-card-header>\r\n        <ion-card-title>{{'Price Range'| translate}}</ion-card-title>\r\n      </ion-card-header>\r\n      <ion-item>\r\n        <ion-range dualKnobs=\"true\" pin=\"true\" snaps=\"true\" [(ngModel)]=\"price\" [min]=\"0\" [max]=\"maxAmount\">\r\n          <ion-label slot=\"start\">{{price.lower}}</ion-label>\r\n          <ion-label slot=\"end\">{{price.upper}}</ion-label>\r\n        </ion-range>\r\n      </ion-item>\r\n    </ion-card>\r\n    <!-- <h3>{{'Filter by attributes'| translate}}</h3> -->\r\n    <div *ngIf=\"filters.length>0\">\r\n      <ion-card *ngFor=\"let filter of filters\">\r\n        <ion-card-header>\r\n          <ion-card-title>{{filter.option.name}}</ion-card-title>\r\n        </ion-card-header>\r\n        <ion-item *ngFor=\"let v of filter.values\">\r\n          <ion-label>{{v.value}}</ion-label>\r\n          <ion-checkbox (click)=\"fillFilterArray($event,filter.option.name,v.value)\">\r\n          </ion-checkbox>\r\n        </ion-item>\r\n      </ion-card>\r\n    </div>\r\n  </ion-content>\r\n\r\n  <ion-footer>\r\n    <ion-toolbar color=\"light\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-button fill=\"clear\" color=\"secondary\" (click)=\"resetFilters()\">\r\n          {{'Reset'| translate}}\r\n        </ion-button>\r\n      </ion-buttons>\r\n      <ion-title></ion-title>\r\n      <ion-buttons slot=\"end\">\r\n        <ion-button fill=\"clear\" color=\"secondary\" (click)=\"applyFilters()\">\r\n          {{'Apply'| translate}}\r\n        </ion-button>\r\n      </ion-buttons>\r\n    </ion-toolbar>\r\n  </ion-footer>\r\n</ion-menu>");

/***/ }),

/***/ "./src/app/products/products.module.ts":
/*!*********************************************!*\
  !*** ./src/app/products/products.module.ts ***!
  \*********************************************/
/*! exports provided: ProductsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPageModule", function() { return ProductsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _products_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./products.page */ "./src/app/products/products.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");
/* harmony import */ var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/components/share/share.module */ "./src/components/share/share.module.ts");









const routes = [
    {
        path: '',
        component: _products_page__WEBPACK_IMPORTED_MODULE_6__["ProductsPage"]
    }
];
let ProductsPageModule = class ProductsPageModule {
};
ProductsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"]
        ],
        declarations: [_products_page__WEBPACK_IMPORTED_MODULE_6__["ProductsPage"]]
    })
], ProductsPageModule);



/***/ }),

/***/ "./src/app/products/products.page.scss":
/*!*********************************************!*\
  !*** ./src/app/products/products.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  min-height: inherit;\n}\nion-toolbar ion-slides {\n  background-color: white;\n}\nion-toolbar ion-slides ion-slide {\n  height: 56px;\n  width: auto;\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 14px;\n  border-top-width: 0;\n  border-left-width: 0;\n  border-right-width: 0;\n  border-bottom-style: solid;\n  border-bottom-width: 2px;\n  border-bottom-color: transparent;\n  color: var(--ion-text-color);\n}\nion-toolbar ion-slides .selected {\n  border-bottom-color: var(--ion-text-color);\n  background-color: var(accent-color);\n}\nion-toolbar .disable {\n  pointer-events: none;\n}\nion-content ion-grid ion-row {\n  margin-right: 10px;\n}\nion-content h5 {\n  margin: 0px 10px;\n  text-transform: uppercase;\n}\nion-content h4 {\n  padding-left: 10px;\n}\nion-content form ion-item {\n  --background: var(--ion-background-color);\n}\nion-content ion-button {\n  width: 100%;\n  margin: 0;\n  border: none;\n}\nion-content .capital {\n  text-transform: capitalize;\n}\nion-footer ion-toolbar ion-item {\n  margin: 0;\n}\nion-footer ion-toolbar ion-item .item-native {\n  padding-left: 10px;\n}\nion-footer ion-toolbar ion-item ion-label {\n  margin-top: 7px;\n}\nion-footer ion-toolbar ion-item ion-label p {\n  font-size: 11px;\n}\nion-footer ion-toolbar ion-item ion-label h3 {\n  text-transform: uppercase;\n  color: var(--ion-color-secondary);\n}\nion-footer ion-toolbar ion-item ion-label ion-icon {\n  vertical-align: top;\n}\nion-menu ion-range {\n  padding-top: 10px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\nion-menu ion-card ion-card-header {\n  padding-bottom: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvRDpcXERvY3VtZW50b3NcXFByb2dyYW1hY2lvblxcSmF2YXNjcmlwdFxcSW9uaWNcXGRlbGl2ZXJ5Y3VzdG9tZXIvc3JjXFxhcHBcXHByb2R1Y3RzXFxwcm9kdWN0cy5wYWdlLnNjc3MiLCJzcmMvYXBwL3Byb2R1Y3RzL3Byb2R1Y3RzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0FDQ0Y7QURBRTtFQUNFLHVCQUFBO0FDRUo7QURESTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EscUJBQUE7RUFDQSwwQkFBQTtFQUNBLHdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSw0QkFBQTtBQ0dOO0FEREk7RUFDRSwwQ0FBQTtFQUNBLG1DQUFBO0FDR047QURBRTtFQUNFLG9CQUFBO0FDRUo7QURJSTtFQUNFLGtCQUFBO0FDRE47QURLRTtFQUNFLGdCQUFBO0VBQ0EseUJBQUE7QUNISjtBRE1FO0VBQ0Usa0JBQUE7QUNKSjtBRFFJO0VBQ0UseUNBQUE7QUNOTjtBRFNFO0VBQ0UsV0FBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0FDUEo7QURVRTtFQUNFLDBCQUFBO0FDUko7QURhSTtFQUlFLFNBQUE7QUNiTjtBRFVNO0VBQ0Usa0JBQUE7QUNSUjtBRFdNO0VBQ0UsZUFBQTtBQ1RSO0FEVVE7RUFDRSxlQUFBO0FDUlY7QURVUTtFQUNFLHlCQUFBO0VBQ0EsaUNBQUE7QUNSVjtBRFVRO0VBQ0UsbUJBQUE7QUNSVjtBRGVFO0VBQ0UsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDWko7QURlSTtFQUNFLG1CQUFBO0VBQ0EsMEJBQUE7QUNiTiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RzL3Byb2R1Y3RzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFyIHtcclxuICBtaW4taGVpZ2h0OiBpbmhlcml0O1xyXG4gIGlvbi1zbGlkZXMge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBpb24tc2xpZGUge1xyXG4gICAgICBoZWlnaHQ6IDU2cHg7XHJcbiAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgYm9yZGVyLXRvcC13aWR0aDogMDtcclxuICAgICAgYm9yZGVyLWxlZnQtd2lkdGg6IDA7XHJcbiAgICAgIGJvcmRlci1yaWdodC13aWR0aDogMDtcclxuICAgICAgYm9yZGVyLWJvdHRvbS1zdHlsZTogc29saWQ7XHJcbiAgICAgIGJvcmRlci1ib3R0b20td2lkdGg6IDJweDtcclxuICAgICAgYm9yZGVyLWJvdHRvbS1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICB9XHJcbiAgICAuc2VsZWN0ZWQge1xyXG4gICAgICBib3JkZXItYm90dG9tLWNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcihhY2NlbnQtY29sb3IpO1xyXG4gICAgfVxyXG4gIH1cclxuICAuZGlzYWJsZSB7XHJcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICB9XHJcbn1cclxuXHJcbmlvbi1jb250ZW50IHtcclxuICBpb24tZ3JpZCB7XHJcbiAgICBpb24tcm93IHtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaDUge1xyXG4gICAgbWFyZ2luOiAwcHggMTBweDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgfVxyXG5cclxuICBoNCB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgfVxyXG5cclxuICBmb3JtIHtcclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGlvbi1idXR0b24ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAuY2FwaXRhbCB7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICB9XHJcbn1cclxuaW9uLWZvb3RlciB7XHJcbiAgaW9uLXRvb2xiYXIge1xyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICAuaXRlbS1uYXRpdmUge1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgfVxyXG4gICAgICBtYXJnaW46IDA7XHJcbiAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogN3B4O1xyXG4gICAgICAgIHAge1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBoMyB7XHJcbiAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5pb24tbWVudSB7XHJcbiAgaW9uLXJhbmdlIHtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgfVxyXG4gIGlvbi1jYXJkIHtcclxuICAgIGlvbi1jYXJkLWhlYWRlciB7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XHJcbiAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCJpb24tdG9vbGJhciB7XG4gIG1pbi1oZWlnaHQ6IGluaGVyaXQ7XG59XG5pb24tdG9vbGJhciBpb24tc2xpZGVzIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5pb24tdG9vbGJhciBpb24tc2xpZGVzIGlvbi1zbGlkZSB7XG4gIGhlaWdodDogNTZweDtcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItdG9wLXdpZHRoOiAwO1xuICBib3JkZXItbGVmdC13aWR0aDogMDtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiAwO1xuICBib3JkZXItYm90dG9tLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLWJvdHRvbS13aWR0aDogMnB4O1xuICBib3JkZXItYm90dG9tLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6IHZhcigtLWlvbi10ZXh0LWNvbG9yKTtcbn1cbmlvbi10b29sYmFyIGlvbi1zbGlkZXMgLnNlbGVjdGVkIHtcbiAgYm9yZGVyLWJvdHRvbS1jb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoYWNjZW50LWNvbG9yKTtcbn1cbmlvbi10b29sYmFyIC5kaXNhYmxlIHtcbiAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG59XG5cbmlvbi1jb250ZW50IGlvbi1ncmlkIGlvbi1yb3cge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5pb24tY29udGVudCBoNSB7XG4gIG1hcmdpbjogMHB4IDEwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5pb24tY29udGVudCBoNCB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbn1cbmlvbi1jb250ZW50IGZvcm0gaW9uLWl0ZW0ge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcbn1cbmlvbi1jb250ZW50IGlvbi1idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luOiAwO1xuICBib3JkZXI6IG5vbmU7XG59XG5pb24tY29udGVudCAuY2FwaXRhbCB7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG5pb24tZm9vdGVyIGlvbi10b29sYmFyIGlvbi1pdGVtIHtcbiAgbWFyZ2luOiAwO1xufVxuaW9uLWZvb3RlciBpb24tdG9vbGJhciBpb24taXRlbSAuaXRlbS1uYXRpdmUge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59XG5pb24tZm9vdGVyIGlvbi10b29sYmFyIGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIG1hcmdpbi10b3A6IDdweDtcbn1cbmlvbi1mb290ZXIgaW9uLXRvb2xiYXIgaW9uLWl0ZW0gaW9uLWxhYmVsIHAge1xuICBmb250LXNpemU6IDExcHg7XG59XG5pb24tZm9vdGVyIGlvbi10b29sYmFyIGlvbi1pdGVtIGlvbi1sYWJlbCBoMyB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbn1cbmlvbi1mb290ZXIgaW9uLXRvb2xiYXIgaW9uLWl0ZW0gaW9uLWxhYmVsIGlvbi1pY29uIHtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbn1cblxuaW9uLW1lbnUgaW9uLXJhbmdlIHtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG5pb24tbWVudSBpb24tY2FyZCBpb24tY2FyZC1oZWFkZXIge1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/products/products.page.ts":
/*!*******************************************!*\
  !*** ./src/app/products/products.page.ts ***!
  \*******************************************/
/*! exports provided: ProductsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPage", function() { return ProductsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/loading/loading.service */ "./src/providers/loading/loading.service.ts");
/* harmony import */ var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/providers/app-events/app-events.service */ "./src/providers/app-events/app-events.service.ts");








let ProductsPage = class ProductsPage {
    constructor(navCtrl, activatedRoute, config, shared, loading, appEventsService, actionSheet, menuCtrl) {
        this.navCtrl = navCtrl;
        this.activatedRoute = activatedRoute;
        this.config = config;
        this.shared = shared;
        this.loading = loading;
        this.appEventsService = appEventsService;
        this.actionSheet = actionSheet;
        this.menuCtrl = menuCtrl;
        this.scrollTopButton = false;
        this.products = new Array;
        this.selectedTab = '';
        this.categoryId = '';
        this.categoryName = '';
        this.sortOrder = 'newest';
        this.sortArray = ['Newest', 'A - Z', 'Z - A', 'Price : high - low', 'Price : low - high', 'Top Seller', 'Special Products', 'Most Liked'];
        this.page = 0;
        this.applyFilter = false;
        this.filters = [];
        this.selectedFilters = [];
        this.price = { lower: 0, upper: 500 };
        this.maxAmount = 500;
        this.side = "right";
        this.productView = 'grid';
        this.httpRunning = true;
        this.sliderConfig = {
            slidesPerView: "auto"
        };
        this.hidePriceRange = false;
        if (shared.dir == "rtl")
            this.side = "left";
        if (this.activatedRoute.snapshot.paramMap.get('id') != undefined)
            this.selectedTab = this.categoryId = this.activatedRoute.snapshot.paramMap.get('id');
        if (this.activatedRoute.snapshot.paramMap.get('name') != undefined)
            this.categoryName = this.activatedRoute.snapshot.paramMap.get('name');
        if (this.activatedRoute.snapshot.paramMap.get('type') != undefined)
            this.sortOrder = this.activatedRoute.snapshot.paramMap.get('type');
        if (parseInt(this.selectedTab) == 0) {
            this.selectedTab = '';
        }
        this.getProducts(null);
        this.getFilters(this.categoryId);
    }
    getProducts(infiniteScroll) {
        this.httpRunning = true;
        if (this.page == 0) {
            this.loading.show();
        }
        var dat = {};
        if (this.shared.customerData != null) //in case user is logged in customer id will be send to the server to get user liked products
            dat.customers_id = this.shared.customerData.customers_id;
        if (this.applyFilter == true) {
            dat.filters = this.selectedFilters;
            dat.price = { minPrice: this.price.lower, maxPrice: this.price.upper };
        }
        dat.categories_id = this.selectedTab;
        dat.page_number = this.page;
        dat.type = this.sortOrder;
        dat.language_id = this.config.langId;
        dat.currency_code = this.config.currecnyCode;
        this.config.postHttp('getallproducts', dat).then((data) => {
            this.httpRunning = false;
            // console.log(data.product_data.length + "   " + this.page);
            this.infinite.complete();
            if (this.page == 0) {
                this.products = new Array;
                this.loading.hide();
                this.scrollToTop();
            }
            if (data.success == 1) {
                this.page++;
                var prod = data.product_data;
                for (let value of prod) {
                    this.products.push(value);
                }
            }
            if (data.success == 1 && data.product_data.length == 0) {
                this.infinite.disabled = true;
            }
            if (data.success == 0) {
                this.infinite.disabled = true;
            }
        }, (error) => {
            this.httpRunning = false;
        });
    }
    //changing tab
    changeTab(c) {
        this.applyFilter = false;
        this.infinite.disabled = false;
        this.page = 0;
        if (c == '')
            this.selectedTab = c;
        else
            this.selectedTab = c.id;
        this.getProducts(null);
        this.getFilters(this.selectedTab);
    }
    //============================================================================================  
    // filling filter array for keyword search 
    fillFilterArray(fValue, fName, keyword) {
        if (!fValue.target.checked == true) {
            this.selectedFilters.push({ 'name': fName, 'value': keyword });
        }
        else {
            this.selectedFilters.forEach((value, index) => {
                if (value.value == keyword) {
                    this.selectedFilters.splice(index, 1);
                }
            });
        }
        //console.log(this.selectedFilters);
    }
    ;
    //============================================================================================  
    //getting countries from server
    getFilters(id) {
        var dat = {};
        dat.categories_id = id;
        dat.language_id = this.config.langId;
        dat.currency_code = this.config.currecnyCode;
        this.config.postHttp('getfilters', dat).then((data) => {
            //  console.log(data);
            if (data.success == 1) {
                this.filters = data.filters;
                if (data.maxPrice == "" || data.maxPrice == null)
                    this.maxAmount = 1000;
                else {
                    this.maxAmount = data.maxPrice;
                }
                this.price = { lower: 0, upper: this.maxAmount };
            }
            if (data.success == 0) {
                this.filters = data.filters;
            }
            // if (this.products.length == 0) this.hidePriceRange = true;
            // else this.hidePriceRange = false;
        });
    }
    ;
    applyFilters() {
        this.applyFilter = true;
        this.infinite.disabled = false;
        this.page = 0;
        this.getProducts(null);
        this.menuCtrl.close("menu2");
    }
    resetFilters() {
        this.getFilters(this.selectedTab);
        this.menuCtrl.close("menu2");
    }
    removeFilters() {
        this.applyFilter = false;
        this.infinite.disabled = false;
        this.page = 0;
        this.getProducts(null);
        this.getFilters(this.selectedTab);
    }
    ngOnChanges() {
    }
    getSortProducts(value) {
        if (value == 'Newest')
            value = 'newest';
        else if (value == 'A - Z')
            value = 'a to z';
        else if (value == 'Z - A')
            value = 'z to a';
        else if (value == 'Price : high - low')
            value = 'high to low';
        else if (value == 'low to high')
            value = 'low to high';
        else if (value == 'Top Seller')
            value = 'top seller';
        else if (value == 'Special Products')
            value = 'special';
        else if (value == 'Most Liked')
            value = 'most liked';
        else
            value = value;
        //console.log(value);
        if (value == this.sortOrder)
            return 0;
        else {
            this.sortOrder = value;
            this.infinite.disabled = false;
            this.page = 0;
            this.getProducts(null);
        }
    }
    openSortBy() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var buttonArray = [];
            this.shared.translateArray(this.sortArray).then((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                for (let key in res) {
                    buttonArray.push({ text: res[key], handler: () => { this.getSortProducts(key); } });
                }
                this.shared.translateString("Cancel").then((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    buttonArray.push({
                        text: res,
                        role: 'cancel',
                        handler: () => {
                        }
                    });
                }));
                var action = yield this.actionSheet.create({
                    buttons: buttonArray
                });
                yield action.present();
            }));
        });
    }
    toggleMenu() {
        this.menuCtrl.toggle("menu2");
    }
    changeLayout() {
        if (this.productView == 'list')
            this.productView = "grid";
        else
            this.productView = "list";
        this.scrollToTop();
    }
    scrollToTop() {
        try {
            this.content.scrollToTop(700);
            this.scrollTopButton = false;
        }
        catch (error) {
        }
    }
    onScroll(e) {
        if (e.scrollTop >= 1200)
            this.scrollTopButton = true;
        if (e.scrollTop < 1200)
            this.scrollTopButton = false;
        //else this.scrollTopButton=false;
        //   console.log(e);
    }
    ionViewDidEnter() {
        try {
            setTimeout(() => {
                let ind = 0;
                this.shared.allCategories.forEach((value, index) => {
                    if (this.selectedTab == value.id) {
                        ind = index;
                    }
                });
                this.slides.slideTo(ind, 1000, true);
            }, 100);
        }
        catch (error) {
        }
    }
    ngOnInit() {
    }
};
ProductsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"] },
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"] },
    { type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
    { type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_7__["AppEventsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])
], ProductsPage.prototype, "infinite", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])
], ProductsPage.prototype, "content", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"])
], ProductsPage.prototype, "slides", void 0);
ProductsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-products',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./products.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/products/products.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./products.page.scss */ "./src/app/products/products.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"],
        src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"],
        src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"],
        src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_7__["AppEventsService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]])
], ProductsPage);



/***/ })

}]);
//# sourceMappingURL=products-products-module-es2015.js.map