function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-pages-home4-home4-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home4/home4.page.html":
  /*!****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home4/home4.page.html ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomePagesHome4Home4PageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n   \r\n        <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title mode=ios>\r\n      {{config.appName|translate}}\r\n      <!-- <ion-img src=\"assets/logo_header.png\" alt=\"logo\"></ion-img> -->\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n        <ion-icon name=\"basket\"></ion-icon>\r\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    \r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding-bottom\">\r\n  <app-banner></app-banner>\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"apps\"></ion-icon>\r\n      {{'Categories'|translate}}\r\n    </ion-button>\r\n  </ion-row>\r\n  <!-- category avatar -->\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col *ngFor=\"let c of shared.categories\" (click)=\"openSubCategories(c)\" size=\"6\"\r\n        class=\"ion-no-padding animate-item\">\r\n        <ion-card>\r\n          <ion-avatar>\r\n            <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.image}}\" />\r\n          </ion-avatar>\r\n          <ion-text>\r\n            <h6>{{c.name|translate}}</h6>\r\n            <p>{{c.count}} {{'Products'| translate }}</p>\r\n          </ion-text>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <!-- Flash sale items products -->\r\n\r\n  <div class=\"module\" *ngIf=\"shared.flashSaleProducts.length!=0\">\r\n    <ion-row class=\"top-icon-header\">\r\n      <ion-button fill=\"clear\">\r\n        <ion-icon slot=\"start\" name=\"time\"></ion-icon>\r\n        {{'Flash Sale'|translate}}\r\n      </ion-button>\r\n    </ion-row>\r\n    <ion-slides [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.flashSaleProducts\">\r\n        <app-product [data]=\"p\" [type]=\"'flash'\"></app-product>\r\n      </ion-slide>\r\n    </ion-slides>\r\n  </div>\r\n\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"albums\"></ion-icon>\r\n      {{ 'Newest Products' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-slides [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.tab1\">\r\n      <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n    </ion-slide>\r\n    <ion-slide>\r\n      <ion-button fill=\"clear\" (click)=\"openProducts('top seller')\"> {{'Shop More'| translate}}\r\n        <ion-icon name=\"caret-forward\"></ion-icon>\r\n      </ion-button>\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"bookmark\"></ion-icon>\r\n      {{ 'On Sale Products' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-slides [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.tab2\">\r\n      <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n    </ion-slide>\r\n    <ion-slide>\r\n      <ion-button fill=\"clear\" (click)=\"openProducts('special')\"> {{'Shop More'| translate}}\r\n        <ion-icon name=\"caret-forward\"></ion-icon>\r\n      </ion-button>\r\n    </ion-slide>\r\n  </ion-slides>\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"star\"></ion-icon>\r\n      {{ 'Featured Products' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-slides [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.tab3\">\r\n      <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n    </ion-slide>\r\n    <ion-slide>\r\n      <ion-button fill=\"clear\" (click)=\"openProducts('most liked')\"> {{'Shop More'| translate}}\r\n        <ion-icon name=\"caret-forward\"></ion-icon>\r\n      </ion-button>\r\n    </ion-slide>\r\n  </ion-slides>\r\n  <!-- For Vendor List -->\r\n  <!--<app-vendor-list></app-vendor-list>-->\r\n  <!-- recently view Heading -->\r\n  <ion-row *ngIf=\"shared.recentViewedProducts.length!=0\" class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"list\"></ion-icon>\r\n      {{'Recently Viewed'|translate}}\r\n    </ion-button>\r\n  </ion-row>\r\n  <!-- recently viewed swipe slider -->\r\n  <ion-slides #recentSlider [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.recentViewedProducts\">\r\n      <app-product [data]=\"p\" [type]=\"'recent'\"></app-product>\r\n    </ion-slide>\r\n  </ion-slides>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/home-pages/home4/home4.module.ts":
  /*!**************************************************!*\
    !*** ./src/app/home-pages/home4/home4.module.ts ***!
    \**************************************************/

  /*! exports provided: Home4PageModule */

  /***/
  function srcAppHomePagesHome4Home4ModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Home4PageModule", function () {
      return Home4PageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _home4_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home4.page */
    "./src/app/home-pages/home4/home4.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");
    /* harmony import */


    var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/components/share/share.module */
    "./src/components/share/share.module.ts");

    var routes = [{
      path: '',
      component: _home4_page__WEBPACK_IMPORTED_MODULE_6__["Home4Page"]
    }];

    var Home4PageModule = function Home4PageModule() {
      _classCallCheck(this, Home4PageModule);
    };

    Home4PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"]],
      declarations: [_home4_page__WEBPACK_IMPORTED_MODULE_6__["Home4Page"]]
    })], Home4PageModule);
    /***/
  },

  /***/
  "./src/app/home-pages/home4/home4.page.scss":
  /*!**************************************************!*\
    !*** ./src/app/home-pages/home4/home4.page.scss ***!
    \**************************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomePagesHome4Home4PageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content .swiper-slide {\n  width: 40%;\n}\nion-content ion-grid {\n  padding-bottom: 10px !important;\n}\nion-content ion-grid ion-row {\n  background: white;\n  box-shadow: 0 0 0 2px lightgrey;\n  /* border-radius: 10px; */\n  border-right: solid var(--ion-background-color);\n}\nion-content ion-grid ion-row ion-col ion-card {\n  font-weight: bold;\n  text-align: center;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap;\n  text-align: center;\n  margin: 0;\n  border-radius: 0px;\n  border: solid var(--ion-background-color);\n  box-shadow: 0px 0px 0px 0px var(--ion-background-color);\n  border-bottom: 0px;\n  border-right: 0px;\n}\nion-content ion-grid ion-row ion-col ion-card ion-avatar {\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 10px;\n}\nion-content ion-grid ion-row ion-col ion-card h6 {\n  font-weight: bold;\n  text-align: center;\n  margin-bottom: 0;\n  font-size: var(--heading-font-size);\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\nion-content ion-grid ion-row ion-col ion-card p {\n  text-align: center;\n  margin-top: 0;\n  font-size: var(--sub-heading-font-size);\n}\nion-content ion-slides ion-slide:last-child {\n  height: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS1wYWdlcy9ob21lNC9EOlxcRG9jdW1lbnRvc1xcUHJvZ3JhbWFjaW9uXFxKYXZhc2NyaXB0XFxJb25pY1xcZGVsaXZlcnljdXN0b21lci9zcmNcXGFwcFxcaG9tZS1wYWdlc1xcaG9tZTRcXGhvbWU0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS1wYWdlcy9ob21lNC9ob21lNC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxVQUFBO0FDQUo7QURFRTtFQUNFLCtCQUFBO0FDQUo7QURDSTtFQUNFLGlCQUFBO0VBQ0EsK0JBQUE7RUFDQSx5QkFBQTtFQUNBLCtDQUFBO0FDQ047QURDUTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtFQUNBLHlDQUFBO0VBQ0EsdURBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDQ1Y7QURBVTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0VaO0FEQVU7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQ0VaO0FEQVU7RUFDRSxrQkFBQTtFQUNBLGFBQUE7RUFDQSx1Q0FBQTtBQ0VaO0FET007RUFDRSxZQUFBO0FDTFIiLCJmaWxlIjoic3JjL2FwcC9ob21lLXBhZ2VzL2hvbWU0L2hvbWU0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAuc3dpcGVyLXNsaWRlIHtcclxuICAgIHdpZHRoOiA0MCU7XHJcbiAgfVxyXG4gIGlvbi1ncmlkIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBpb24tcm93IHtcclxuICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgIGJveC1zaGFkb3c6IDAgMCAwIDJweCBsaWdodGdyZXk7XHJcbiAgICAgIC8qIGJvcmRlci1yYWRpdXM6IDEwcHg7ICovXHJcbiAgICAgIGJvcmRlci1yaWdodDogc29saWQgdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xyXG4gICAgICBpb24tY29sIHtcclxuICAgICAgICBpb24tY2FyZCB7XHJcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICAgICAgICBib3JkZXI6IHNvbGlkIHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAwcHggMHB4IDBweCB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAwcHg7XHJcbiAgICAgICAgICBib3JkZXItcmlnaHQ6IDBweDtcclxuICAgICAgICAgIGlvbi1hdmF0YXIge1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaDYge1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IHZhcigtLWhlYWRpbmctZm9udC1zaXplKTtcclxuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBwIHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IHZhcigtLXN1Yi1oZWFkaW5nLWZvbnQtc2l6ZSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBpb24tc2xpZGVzIHtcclxuICAgIGlvbi1zbGlkZSB7XHJcbiAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsImlvbi1jb250ZW50IC5zd2lwZXItc2xpZGUge1xuICB3aWR0aDogNDAlO1xufVxuaW9uLWNvbnRlbnQgaW9uLWdyaWQge1xuICBwYWRkaW5nLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgaW9uLWdyaWQgaW9uLXJvdyB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3gtc2hhZG93OiAwIDAgMCAycHggbGlnaHRncmV5O1xuICAvKiBib3JkZXItcmFkaXVzOiAxMHB4OyAqL1xuICBib3JkZXItcmlnaHQ6IHNvbGlkIHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcbn1cbmlvbi1jb250ZW50IGlvbi1ncmlkIGlvbi1yb3cgaW9uLWNvbCBpb24tY2FyZCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMDtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICBib3JkZXI6IHNvbGlkIHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAwcHggMHB4IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcbiAgYm9yZGVyLWJvdHRvbTogMHB4O1xuICBib3JkZXItcmlnaHQ6IDBweDtcbn1cbmlvbi1jb250ZW50IGlvbi1ncmlkIGlvbi1yb3cgaW9uLWNvbCBpb24tY2FyZCBpb24tYXZhdGFyIHtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbmlvbi1jb250ZW50IGlvbi1ncmlkIGlvbi1yb3cgaW9uLWNvbCBpb24tY2FyZCBoNiB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbn1cbmlvbi1jb250ZW50IGlvbi1ncmlkIGlvbi1yb3cgaW9uLWNvbCBpb24tY2FyZCBwIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAwO1xuICBmb250LXNpemU6IHZhcigtLXN1Yi1oZWFkaW5nLWZvbnQtc2l6ZSk7XG59XG5pb24tY29udGVudCBpb24tc2xpZGVzIGlvbi1zbGlkZTpsYXN0LWNoaWxkIHtcbiAgaGVpZ2h0OiBhdXRvO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/home-pages/home4/home4.page.ts":
  /*!************************************************!*\
    !*** ./src/app/home-pages/home4/home4.page.ts ***!
    \************************************************/

  /*! exports provided: Home4Page */

  /***/
  function srcAppHomePagesHome4Home4PageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Home4Page", function () {
      return Home4Page;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var Home4Page = /*#__PURE__*/function () {
      function Home4Page(nav, config, appEventsService, shared) {
        _classCallCheck(this, Home4Page);

        this.nav = nav;
        this.config = config;
        this.appEventsService = appEventsService;
        this.shared = shared;
        this.sliderConfig = {
          slidesPerView: this.config.productSlidesPerPage,
          spaceBetween: 0
        };
      }

      _createClass(Home4Page, [{
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.shared.hideSplashScreen();
        }
      }, {
        key: "openSubCategories",
        value: function openSubCategories(parent) {
          var count = 0;

          var _iterator = _createForOfIteratorHelper(this.shared.allCategories),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var value = _step.value;
              if (parent.id == value.parent_id) count++;
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          if (count != 0) this.nav.navigateForward(this.config.currentRoute + "/categories/" + parent.id + "/" + parent.name);else this.nav.navigateForward(this.config.currentRoute + "/products/" + parent.id + "/" + parent.name + "/newest");
        }
      }, {
        key: "openProducts",
        value: function openProducts(value) {
          this.nav.navigateForward(this.config.currentRoute + "/products/0/0/" + value);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewCanEnter",
        value: function ionViewCanEnter() {
          if (!this.config.appInProduction) {
            this.config.productCardStyle = "9";
          }
        }
      }]);

      return Home4Page;
    }();

    Home4Page.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]
      }];
    };

    Home4Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home4',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home4.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home4/home4.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home4.page.scss */
      "./src/app/home-pages/home4/home4.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]])], Home4Page);
    /***/
  }
}]);
//# sourceMappingURL=home-pages-home4-home4-module-es5.js.map