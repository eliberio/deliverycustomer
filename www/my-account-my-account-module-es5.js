function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-account-my-account-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/my-account/my-account.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/my-account/my-account.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMyAccountMyAccountPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      {{'Edit Profile'| translate }}\r\n    </ion-title>\r\n\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <form #userForm=\"ngForm\" (ngSubmit)=\"updateInfo()\">\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'First Name'|translate}}</ion-label>\r\n      <ion-input type=\"text\" [(ngModel)]=\"myAccountData.customers_firstname\" name=\"customers_firstname\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Last Name'|translate}}</ion-label>\r\n      <ion-input type=\"text\" [(ngModel)]=\"myAccountData.customers_lastname\" name=\"customers_lastname\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Mobile'|translate}}</ion-label>\r\n      <ion-input type=\"number\" inputmode=\"tel\" [(ngModel)]=\"myAccountData.phone\" name=\"phone\"></ion-input>\r\n    </ion-item>\r\n    <!-- <ion-item>\r\n      <ion-label>{{'Date of Birth'|translate}}</ion-label>\r\n      <ion-datetime displayFormat=\"DD/MM/YYYY\" max=\"2010\" name=\"customers_dob\"\r\n        [(ngModel)]=\"myAccountData.customers_dob\"></ion-datetime>\r\n    </ion-item> -->\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Current Password'|translate}}</ion-label>\r\n      <ion-input type=\"password\" [(ngModel)]=\"myAccountData.currentPassword\" name=\"currentPassword\"></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'New Password'|translate}}</ion-label>\r\n      <ion-input type=\"password\" [(ngModel)]=\"myAccountData.password\" name=\"password\"></ion-input>\r\n    </ion-item>\r\n    <ion-button expand=\"full\" color=\"secondary\" type=\"submit\" [disabled]=\"!userForm.form.valid\">{{'Update'|translate}}\r\n    </ion-button>\r\n  </form>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/my-account/my-account.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/my-account/my-account.module.ts ***!
    \*************************************************/

  /*! exports provided: MyAccountPageModule */

  /***/
  function srcAppMyAccountMyAccountModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyAccountPageModule", function () {
      return MyAccountPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _my_account_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./my-account.page */
    "./src/app/my-account/my-account.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _my_account_page__WEBPACK_IMPORTED_MODULE_6__["MyAccountPage"]
    }];

    var MyAccountPageModule = function MyAccountPageModule() {
      _classCallCheck(this, MyAccountPageModule);
    };

    MyAccountPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_my_account_page__WEBPACK_IMPORTED_MODULE_6__["MyAccountPage"]]
    })], MyAccountPageModule);
    /***/
  },

  /***/
  "./src/app/my-account/my-account.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/my-account/my-account.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppMyAccountMyAccountPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "form {\n  padding-left: 12px;\n  padding-right: 12px;\n  padding-top: 12px;\n}\nform ion-item {\n  --padding-start: 0;\n  --background: var(--ion-background-color);\n}\nform ion-item ion-label {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\nform ion-button {\n  text-transform: uppercase;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktYWNjb3VudC9EOlxcRG9jdW1lbnRvc1xcUHJvZ3JhbWFjaW9uXFxKYXZhc2NyaXB0XFxJb25pY1xcZGVsaXZlcnljdXN0b21lci9zcmNcXGFwcFxcbXktYWNjb3VudFxcbXktYWNjb3VudC5wYWdlLnNjc3MiLCJzcmMvYXBwL215LWFjY291bnQvbXktYWNjb3VudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNBSjtBRENJO0VBQ0ksa0JBQUE7RUFDQSx5Q0FBQTtBQ0NSO0FEQVE7RUFDSSwyQ0FBQTtBQ0VaO0FEQ0k7RUFDSSx5QkFBQTtBQ0NSIiwiZmlsZSI6InNyYy9hcHAvbXktYWNjb3VudC9teS1hY2NvdW50LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5mb3JtIHtcclxuICAgIHBhZGRpbmctbGVmdDogMTJweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDEycHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTJweDtcclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICAgICAgaW9uLWxhYmVsIHtcclxuICAgICAgICAgICAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBpb24tYnV0dG9uIHtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgfVxyXG59XHJcbiIsImZvcm0ge1xuICBwYWRkaW5nLWxlZnQ6IDEycHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEycHg7XG4gIHBhZGRpbmctdG9wOiAxMnB4O1xufVxuZm9ybSBpb24taXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMDtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG59XG5mb3JtIGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XG59XG5mb3JtIGlvbi1idXR0b24ge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/my-account/my-account.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/my-account/my-account.page.ts ***!
    \***********************************************/

  /*! exports provided: MyAccountPage */

  /***/
  function srcAppMyAccountMyAccountPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyAccountPage", function () {
      return MyAccountPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");

    var MyAccountPage = /*#__PURE__*/function () {
      function MyAccountPage(config, shared, loading) {
        _classCallCheck(this, MyAccountPage);

        this.config = config;
        this.shared = shared;
        this.loading = loading;
        this.myAccountData = {
          customers_firstname: '',
          customers_lastname: '',
          phone: '',
          currentPassword: '',
          password: '',
          customers_dob: '',
          customers_id: '',
          image_id: 0,
          customers_telephone: ""
        };
      } //============================================================================================  
      //function updating user information


      _createClass(MyAccountPage, [{
        key: "updateInfo",
        value: function updateInfo() {
          var _this = this;

          console.log(this.shared.customerData);

          if (this.myAccountData.currentPassword == "" && this.myAccountData.password != "") {
            this.shared.toast("Please Enter Your New Password!");
            return;
          } else if (this.myAccountData.currentPassword != "" && this.myAccountData.password == "") {
            this.shared.toast("Please Enter Your Current Password!");
            return;
          }

          if (this.myAccountData.currentPassword != "" && this.myAccountData.password != "") {
            if (this.shared.customerData.password != this.myAccountData.currentPassword) {
              console.log(this.shared.customerData.password + "  " + this.myAccountData.currentPassword);
              this.shared.toast("Current Password is Wrong!");
              return;
            }
          }

          this.loading.show();
          this.myAccountData.customers_id = this.shared.customerData.customers_id;
          var dat = this.myAccountData; //  console.log("post data  "+JSON.stringify(data));

          this.config.postHttp('updatecustomerinfo', dat).then(function (data) {
            _this.loading.hide();

            if (data.success == 1) {
              //   document.getElementById("updateForm").reset();
              var d = data.data[0];

              _this.shared.toast(data.message); // if (this.myAccountData.password != '')
              //   this.shared.customerData.password = this.myAccountData.password;


              console.log("data from server");
              console.log(d);

              _this.shared.login(d);

              _this.myAccountData.currentPassword = "";
              _this.myAccountData.password = "";
            } else {
              _this.shared.toast(data.message);
            }
          }, function (error) {
            _this.loading.hide();

            _this.shared.toast("Error while Updating!");
          }); //}
        } //============================================================================================

      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.myAccountData.customers_firstname = this.shared.customerData.customers_firstname;
          this.myAccountData.customers_lastname = this.shared.customerData.customers_lastname;
          this.myAccountData.phone = this.shared.customerData.phone; //this.myAccountData.password = this.shared.customerData.password;

          try {
            // console.log(this.shared.customerData.customers_dob);
            this.myAccountData.customers_dob = new Date(this.shared.customerData.customers_dob).toISOString(); // console.log(this.myAccountData.customers_dob);
          } catch (error) {
            this.myAccountData.customers_dob = new Date("1-1-2000").toISOString();
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return MyAccountPage;
    }();

    MyAccountPage.ctorParameters = function () {
      return [{
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }];
    };

    MyAccountPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-my-account',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./my-account.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/my-account/my-account.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./my-account.page.scss */
      "./src/app/my-account/my-account.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]])], MyAccountPage);
    /***/
  }
}]);
//# sourceMappingURL=my-account-my-account-module-es5.js.map