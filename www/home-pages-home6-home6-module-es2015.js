(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-pages-home6-home6-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home6/home6.page.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home6/home6.page.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title mode=ios>\r\n      {{config.appName|translate}}\r\n      <!-- <ion-img src=\"assets/logo_header.png\" alt=\"logo\"></ion-img> -->\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n        <ion-icon name=\"basket\"></ion-icon>\r\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\r\n      </ion-button>\r\n    </ion-buttons>\r\n\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding-bottom\">\r\n  <app-banner></app-banner>\r\n  <!-- Flash sale items products -->\r\n\r\n  <div class=\"module\" *ngIf=\"shared.flashSaleProducts.length!=0\">\r\n    <ion-row class=\"top-icon-header\">\r\n      <ion-button fill=\"clear\">\r\n        <ion-icon slot=\"start\" name=\"time\"></ion-icon>\r\n        {{'Flash Sale'|translate}}\r\n      </ion-button>\r\n    </ion-row>\r\n\r\n    <ion-slides [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.flashSaleProducts\">\r\n        <app-product [data]=\"p\" [type]=\"'flash'\"></app-product>\r\n      </ion-slide>\r\n    </ion-slides>\r\n  </div>\r\n  <!-- categories swipe slider -->\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"apps\"></ion-icon>\r\n      {{'Categories' | translate }}\r\n    </ion-button>\r\n    <ion-button id=\"second\" fill=\"clear\" (click)=\"openCategoryPage()\">\r\n      <ion-icon slot=\"end\" name=\"caret-forward\"></ion-icon>\r\n      {{ 'Shop More' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n  <!-- categories component -->\r\n  <app-categories [type]=\"'withName'\"></app-categories>\r\n  <!-- On Sale Header -->\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"bookmark\"></ion-icon>\r\n      {{'Deals' | translate }}\r\n    </ion-button>\r\n    <ion-button id=\"second\" fill=\"clear\" (click)=\"openProducts('special')\">\r\n      <ion-icon slot=\"end\" name=\"caret-forward\"></ion-icon>\r\n      {{ 'Shop More' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n  <!-- On Sale Products swipe slider -->\r\n  <ion-slides [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.tab2\">\r\n      <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n    </ion-slide>\r\n  </ion-slides>\r\n  <!-- For Vendor List -->\r\n  <!--<app-vendor-list></app-vendor-list>-->\r\n  <!-- banner image -->\r\n  <ion-img *ngIf=\"shared.banners.length!=0\" style=\"margin-top: 10px;\"\r\n    src=\"{{config.imgUrl+shared.banners[shared.banners.length-1].image}}\"></ion-img>\r\n  <!-- All Products Header -->\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"albums\"></ion-icon>\r\n      {{'Most Liked' | translate }}\r\n    </ion-button>\r\n    <ion-button id=\"second\" fill=\"clear\" (click)=\"openProducts('most liked')\">\r\n      <ion-icon slot=\"end\" name=\"caret-forward\"></ion-icon>\r\n      {{ 'Shop More' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n  <!-- scrolling products component -->\r\n  <app-scrolling-featured-products></app-scrolling-featured-products>\r\n\r\n  <!-- fab button to move at top of page  -->\r\n\r\n</ion-content>\r\n<ion-fab vertical=\"bottom\" horizontal=\"end\" *ngIf=\"scrollTopButton\">\r\n  <ion-fab-button (click)=\"scrollToTop()\">\r\n    <ion-icon name=\"arrow-up\"></ion-icon>\r\n  </ion-fab-button>\r\n</ion-fab>");

/***/ }),

/***/ "./src/app/home-pages/home6/home6.module.ts":
/*!**************************************************!*\
  !*** ./src/app/home-pages/home6/home6.module.ts ***!
  \**************************************************/
/*! exports provided: Home6PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home6PageModule", function() { return Home6PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _home6_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home6.page */ "./src/app/home-pages/home6/home6.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");
/* harmony import */ var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/components/share/share.module */ "./src/components/share/share.module.ts");









const routes = [
    {
        path: '',
        component: _home6_page__WEBPACK_IMPORTED_MODULE_6__["Home6Page"]
    }
];
let Home6PageModule = class Home6PageModule {
};
Home6PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"],
        ],
        declarations: [_home6_page__WEBPACK_IMPORTED_MODULE_6__["Home6Page"]]
    })
], Home6PageModule);



/***/ }),

/***/ "./src/app/home-pages/home6/home6.page.scss":
/*!**************************************************!*\
  !*** ./src/app/home-pages/home6/home6.page.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content .swiper-slide {\n  width: 40%;\n}\nion-content ion-item {\n  --inner-padding-end: 0;\n  --padding-start: 4px;\n  padding-top: 3px;\n  margin-bottom: -15px;\n}\nion-content ion-item ion-icon {\n  margin-right: 5px;\n  zoom: 0.9;\n  color: #51698f;\n}\nion-content ion-item ion-label p {\n  font-size: 13.5px;\n}\nion-content ion-item ion-button {\n  color: #557f5f;\n}\nion-content app-product {\n  width: 100%;\n}\nion-content ion-fab {\n  position: fixed;\n}\nion-content ion-fab ion-fab-button {\n  --background: #51698f;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS1wYWdlcy9ob21lNi9EOlxcRG9jdW1lbnRvc1xcUHJvZ3JhbWFjaW9uXFxKYXZhc2NyaXB0XFxJb25pY1xcZGVsaXZlcnljdXN0b21lci9zcmNcXGFwcFxcaG9tZS1wYWdlc1xcaG9tZTZcXGhvbWU2LnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS1wYWdlcy9ob21lNi9ob21lNi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxVQUFBO0FDQVI7QURFSTtFQUNRLHNCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0FDQVo7QURDWTtFQUNJLGlCQUFBO0VBQ0EsU0FBQTtFQUNBLGNBQUE7QUNDaEI7QURFZ0I7RUFDQSxpQkFBQTtBQ0FoQjtBREdRO0VBQ0ksY0FBQTtBQ0RaO0FESUk7RUFDSSxXQUFBO0FDRlI7QURJSTtFQUNJLGVBQUE7QUNGUjtBREdRO0VBQ0kscUJBQUE7QUNEWiIsImZpbGUiOiJzcmMvYXBwL2hvbWUtcGFnZXMvaG9tZTYvaG9tZTYucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XHJcbiAgICAuc3dpcGVyLXNsaWRlIHtcclxuICAgICAgICB3aWR0aDogNDAlO1xyXG4gICAgICB9XHJcbiAgICBpb24taXRlbXtcclxuICAgICAgICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcclxuICAgICAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiA0cHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAzcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IC0xNXB4O1xyXG4gICAgICAgICAgICBpb24taWNvbntcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gICAgICAgICAgICAgICAgem9vbTogMC45O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1MTY5OGY7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaW9uLWxhYmVse1xyXG4gICAgICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTMuNXB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlvbi1idXR0b257XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU3ZjVmO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGFwcC1wcm9kdWN0e1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gICAgaW9uLWZhYntcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgaW9uLWZhYi1idXR0b257XHJcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogIzUxNjk4ZjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCJpb24tY29udGVudCAuc3dpcGVyLXNsaWRlIHtcbiAgd2lkdGg6IDQwJTtcbn1cbmlvbi1jb250ZW50IGlvbi1pdGVtIHtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiA0cHg7XG4gIHBhZGRpbmctdG9wOiAzcHg7XG4gIG1hcmdpbi1ib3R0b206IC0xNXB4O1xufVxuaW9uLWNvbnRlbnQgaW9uLWl0ZW0gaW9uLWljb24ge1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgem9vbTogMC45O1xuICBjb2xvcjogIzUxNjk4Zjtcbn1cbmlvbi1jb250ZW50IGlvbi1pdGVtIGlvbi1sYWJlbCBwIHtcbiAgZm9udC1zaXplOiAxMy41cHg7XG59XG5pb24tY29udGVudCBpb24taXRlbSBpb24tYnV0dG9uIHtcbiAgY29sb3I6ICM1NTdmNWY7XG59XG5pb24tY29udGVudCBhcHAtcHJvZHVjdCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuaW9uLWNvbnRlbnQgaW9uLWZhYiB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbn1cbmlvbi1jb250ZW50IGlvbi1mYWIgaW9uLWZhYi1idXR0b24ge1xuICAtLWJhY2tncm91bmQ6ICM1MTY5OGY7XG59Il19 */");

/***/ }),

/***/ "./src/app/home-pages/home6/home6.page.ts":
/*!************************************************!*\
  !*** ./src/app/home-pages/home6/home6.page.ts ***!
  \************************************************/
/*! exports provided: Home6Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home6Page", function() { return Home6Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/app-events/app-events.service */ "./src/providers/app-events/app-events.service.ts");






let Home6Page = class Home6Page {
    constructor(nav, config, appEventsService, shared) {
        this.nav = nav;
        this.config = config;
        this.appEventsService = appEventsService;
        this.shared = shared;
        this.scrollTopButton = false; //for scroll down fab 
        //for product slider after banner
        this.sliderConfig = {
            slidesPerView: 2.5,
            spaceBetween: 0
        };
    }
    ionViewDidEnter() {
        this.shared.hideSplashScreen();
    }
    // For FAB Scroll
    onScroll(e) {
        if (e.detail.scrollTop >= 500) {
            this.scrollTopButton = true;
        }
        if (e.detail.scrollTop < 500) {
            this.scrollTopButton = false;
        }
    }
    // For Scroll To Top Content
    scrollToTop() {
        this.content.scrollToTop(700);
        this.scrollTopButton = false;
    }
    openProducts(value) {
        this.nav.navigateForward(this.config.currentRoute + "/products/0/0/" + value);
    }
    openCategoryPage() {
        this.nav.navigateForward(this.config.currentRoute + "/" + this.config.getCurrentCategoriesPage() + "/0/0");
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        if (!this.config.appInProduction) {
            this.config.productCardStyle = "13";
        }
    }
};
Home6Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"] },
    { type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"] },
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])
], Home6Page.prototype, "content", void 0);
Home6Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home6',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home6.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home6/home6.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home6.page.scss */ "./src/app/home-pages/home6/home6.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"],
        src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"],
        src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]])
], Home6Page);



/***/ })

}]);
//# sourceMappingURL=home-pages-home6-home6-module-es2015.js.map