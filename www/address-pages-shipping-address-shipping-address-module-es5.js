function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["address-pages-shipping-address-shipping-address-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/address-pages/shipping-address/shipping-address.page.html":
  /*!*****************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/address-pages/shipping-address/shipping-address.page.html ***!
    \*****************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAddressPagesShippingAddressShippingAddressPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title> {{'Shipping Address'| translate }} </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <form #loginForm=\"ngForm\">\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'First Name'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"shipping_firstname\" [(ngModel)]=\"shared.orderDetails.delivery_firstname\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Last Name'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"shipping_lastname\" [(ngModel)]=\"shared.orderDetails.delivery_lastname\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <div *ngIf=\"config.enableAddressMap\">\r\n      <ion-item (click)=\"showGoogleMap()\">\r\n        <ion-label position=\"floating\">{{'Location'|translate}}</ion-label>\r\n        <ion-input type=\"text\" name=\"location\" [(ngModel)]=\"shared.orderDetails.delivery_location\" readonly required>\r\n        </ion-input>\r\n        <ion-icon name=\"location\" slot=\"end\"></ion-icon>\r\n      </ion-item>\r\n      <app-google-map [page]=\"'shipping'\" *ngIf=\"showMap\" (locationUpdated)=\"locationUpdated()\"></app-google-map>\r\n    </div>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Address'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"address\" [(ngModel)]=\"shared.orderDetails.delivery_street_address\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n\r\n    <!--\r\n      <ion-item>\r\n        <ion-label position=\"floating\">{{'Country'|translate}}</ion-label>\r\n        <ion-input type=\"text\" name=\"shipping_country\" tappable (click)=\"selectCountryPage()\" readonly\r\n          [(ngModel)]=\"shared.orderDetails.delivery_country\" required></ion-input>\r\n      </ion-item>\r\n\r\n      <ion-item>\r\n      <ion-label position=\"floating\">{{'State'|translate}}</ion-label>\r\n      <ion-input type=\"text\" required name=\"shipping_zone\" tappable (click)=\"selectZonePage()\" readonly\r\n        [(ngModel)]=\"shared.orderDetails.delivery_zone\"></ion-input>\r\n    </ion-item>-->\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'City'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"shipping_city\" [(ngModel)]=\"shared.orderDetails.delivery_city\" required></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Post code'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"shipping_postcode\" [(ngModel)]=\"shared.orderDetails.delivery_postcode\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Phone'|translate}}</ion-label>\r\n      <ion-input type=\"number\" inputmode=\"tel\" name=\"Phone\" [(ngModel)]=\"shared.orderDetails.delivery_phone\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <ion-item *ngIf=\"shared.orderDetails.guest_status == 1\">\r\n      <ion-label position=\"floating\">{{'Email'|translate}}</ion-label>\r\n      <ion-input type=\"email\" name=\"email\" [(ngModel)]=\"shared.orderDetails.email\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n  </form>\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-button expand=\"full\" color=\"secondary\" (click)=\"submit()\" [disabled]=\"!loginForm.form.valid\">\r\n    {{'Next'|translate}}\r\n  </ion-button>\r\n</ion-footer>\r\n";
    /***/
  },

  /***/
  "./src/app/address-pages/shipping-address/shipping-address.module.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/address-pages/shipping-address/shipping-address.module.ts ***!
    \***************************************************************************/

  /*! exports provided: ShippingAddressPageModule */

  /***/
  function srcAppAddressPagesShippingAddressShippingAddressModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShippingAddressPageModule", function () {
      return ShippingAddressPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _shipping_address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./shipping-address.page */
    "./src/app/address-pages/shipping-address/shipping-address.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");
    /* harmony import */


    var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/components/share/share.module */
    "./src/components/share/share.module.ts"); // For Translation Language


    var routes = [{
      path: '',
      component: _shipping_address_page__WEBPACK_IMPORTED_MODULE_6__["ShippingAddressPage"]
    }];

    var ShippingAddressPageModule = function ShippingAddressPageModule() {
      _classCallCheck(this, ShippingAddressPageModule);
    };

    ShippingAddressPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"]],
      declarations: [_shipping_address_page__WEBPACK_IMPORTED_MODULE_6__["ShippingAddressPage"]]
    })], ShippingAddressPageModule);
    /***/
  },

  /***/
  "./src/app/address-pages/shipping-address/shipping-address.page.scss":
  /*!***************************************************************************!*\
    !*** ./src/app/address-pages/shipping-address/shipping-address.page.scss ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAddressPagesShippingAddressShippingAddressPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content p {\n  font-size: 20px;\n  text-align: center;\n}\nion-content form ion-item {\n  --background: var(--ion-background-color);\n}\nion-content form ion-item ion-label {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\nion-content form ion-item ion-icon {\n  margin: auto;\n}\nion-footer .toolbar {\n  padding-left: 10px;\n}\nion-footer ion-button {\n  height: 45px;\n  margin: 0;\n  margin-bottom: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRkcmVzcy1wYWdlcy9zaGlwcGluZy1hZGRyZXNzL0Q6XFxEb2N1bWVudG9zXFxQcm9ncmFtYWNpb25cXEphdmFzY3JpcHRcXElvbmljXFxkZWxpdmVyeWN1c3RvbWVyL3NyY1xcYXBwXFxhZGRyZXNzLXBhZ2VzXFxzaGlwcGluZy1hZGRyZXNzXFxzaGlwcGluZy1hZGRyZXNzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvYWRkcmVzcy1wYWdlcy9zaGlwcGluZy1hZGRyZXNzL3NoaXBwaW5nLWFkZHJlc3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0FDQUo7QURHSTtFQUNFLHlDQUFBO0FDRE47QURFTTtFQUNFLDJDQUFBO0FDQVI7QURFTTtFQUNFLFlBQUE7QUNBUjtBRE1FO0VBQ0Usa0JBQUE7QUNISjtBREtFO0VBQ0UsWUFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtBQ0hKIiwiZmlsZSI6InNyYy9hcHAvYWRkcmVzcy1wYWdlcy9zaGlwcGluZy1hZGRyZXNzL3NoaXBwaW5nLWFkZHJlc3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIHAge1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICBmb3JtIHtcclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcclxuICAgICAgfVxyXG4gICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbmlvbi1mb290ZXIge1xyXG4gIC50b29sYmFyIHtcclxuICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICB9XHJcbiAgaW9uLWJ1dHRvbiB7XHJcbiAgICBoZWlnaHQ6IDQ1cHg7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gIH1cclxufVxyXG4iLCJpb24tY29udGVudCBwIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIGlvbi1pY29uIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG5pb24tZm9vdGVyIC50b29sYmFyIHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuaW9uLWZvb3RlciBpb24tYnV0dG9uIHtcbiAgaGVpZ2h0OiA0NXB4O1xuICBtYXJnaW46IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/address-pages/shipping-address/shipping-address.page.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/address-pages/shipping-address/shipping-address.page.ts ***!
    \*************************************************************************/

  /*! exports provided: ShippingAddressPage */

  /***/
  function srcAppAddressPagesShippingAddressShippingAddressPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShippingAddressPage", function () {
      return ShippingAddressPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_user_address_user_address_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/providers/user-address/user-address.service */
    "./src/providers/user-address/user-address.service.ts");
    /* harmony import */


    var src_app_modals_select_country_select_country_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/modals/select-country/select-country.page */
    "./src/app/modals/select-country/select-country.page.ts");
    /* harmony import */


    var src_app_modals_select_zones_select_zones_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/modals/select-zones/select-zones.page */
    "./src/app/modals/select-zones/select-zones.page.ts");

    var ShippingAddressPage = /*#__PURE__*/function () {
      function ShippingAddressPage(navCtrl, config, http, shared, modalCtrl, loading, userAddress) {
        _classCallCheck(this, ShippingAddressPage);

        this.navCtrl = navCtrl;
        this.config = config;
        this.http = http;
        this.shared = shared;
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.userAddress = userAddress;
        this.showMap = false;

        if (this.shared.orderDetails.guest_status == 0) {
          if (this.shared.orderDetails.delivery_firstname == "") this.getUserAddress();
        }
      }

      _createClass(ShippingAddressPage, [{
        key: "showGoogleMap",
        value: function showGoogleMap() {
          this.showMap = true;
        }
      }, {
        key: "locationUpdated",
        value: function locationUpdated() {
          this.showMap = false;
        }
      }, {
        key: "getUserAddress",
        value: function getUserAddress() {
          var _this = this;

          this.loading.show();
          var dat = {};
          dat.customers_id = this.shared.customerData.customers_id;
          this.config.postHttp('getalladdress', dat).then(function (data) {
            _this.loading.hide();

            if (data.success == 1) {
              var allShippingAddress = data.data;

              var _iterator = _createForOfIteratorHelper(allShippingAddress),
                  _step;

              try {
                for (_iterator.s(); !(_step = _iterator.n()).done;) {
                  var value = _step.value;

                  if (value.default_address != null || allShippingAddress.length == 1) {
                    _this.shared.orderDetails.tax_zone_id = value.zone_id;
                    _this.shared.orderDetails.delivery_firstname = value.firstname;
                    _this.shared.orderDetails.delivery_lastname = value.lastname;
                    _this.shared.orderDetails.delivery_state = value.state;
                    _this.shared.orderDetails.delivery_city = value.city;
                    _this.shared.orderDetails.delivery_postcode = value.postcode;
                    _this.shared.orderDetails.delivery_zone = value.zone_name;
                    _this.shared.orderDetails.delivery_country = value.country_name;
                    _this.shared.orderDetails.delivery_country_id = value.countries_id;
                    _this.shared.orderDetails.delivery_street_address = value.street; // if ($rootScope.zones.length == 0)

                    if (value.zone_code == null) {
                      //  console.log(c);
                      _this.shared.orderDetails.delivery_zone = 'other';
                      _this.shared.orderDetails.delivery_state = 'other';
                      _this.shared.orderDetails.tax_zone_id = null;
                    }
                  }
                }
              } catch (err) {
                _iterator.e(err);
              } finally {
                _iterator.f();
              }
            }

            if (data.success == 0) {}
          });
          this.shared.orderDetails.delivery_phone = this.shared.customerData.customers_telephone;
        }
      }, {
        key: "selectCountryPage",
        value: function selectCountryPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.modalCtrl.create({
                      component: src_app_modals_select_country_select_country_page__WEBPACK_IMPORTED_MODULE_8__["SelectCountryPage"],
                      componentProps: {
                        page: 'shipping'
                      }
                    });

                  case 2:
                    modal = _context.sent;
                    _context.next = 5;
                    return modal.present();

                  case 5:
                    return _context.abrupt("return", _context.sent);

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "selectZonePage",
        value: function selectZonePage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var modal;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.modalCtrl.create({
                      component: src_app_modals_select_zones_select_zones_page__WEBPACK_IMPORTED_MODULE_9__["SelectZonesPage"],
                      componentProps: {
                        page: 'shipping',
                        id: this.shared.orderDetails.delivery_country_id
                      }
                    });

                  case 2:
                    modal = _context2.sent;
                    _context2.next = 5;
                    return modal.present();

                  case 5:
                    return _context2.abrupt("return", _context2.sent);

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "submit",
        value: function submit() {
          this.navCtrl.navigateForward(this.config.currentRoute + "/billing-address");
        }
      }, {
        key: "getLocationAddress",
        value: function getLocationAddress() {
          var _this2 = this;

          //this.loading.show();
          var locationEnable = false;
          this.userAddress.getCordinates().then(function (value) {
            locationEnable = true; // this.loading.hide();

            _this2.shared.orderDetails.delivery_lat = value.lat;
            _this2.shared.orderDetails.delivery_long = value["long"];
            _this2.shared.orderDetails.delivery_location = value.lat + ", " + value["long"];
          });
          setTimeout(function () {
            if (locationEnable == false) {
              _this2.shared.showAlert("Please Turn On Device Location");
            }
          }, 10000);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          if (this.config.enableAddressMap && this.shared.orderDetails.delivery_location == "") this.getLocationAddress();
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          if (this.shared.customerData.customers_id == null) {
            this.shared.orderDetails.tax_zone_id = '';
            this.shared.orderDetails.delivery_firstname = '';
            this.shared.orderDetails.delivery_lastname = '';
            this.shared.orderDetails.delivery_state = '';
            this.shared.orderDetails.delivery_city = '';
            this.shared.orderDetails.delivery_postcode = '';
            this.shared.orderDetails.delivery_zone = '';
            this.shared.orderDetails.delivery_country = '';
            this.shared.orderDetails.delivery_country_id = '';
            this.shared.orderDetails.delivery_street_address = '';
          }
        }
      }]);

      return ShippingAddressPage;
    }();

    ShippingAddressPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]
      }, {
        type: src_providers_user_address_user_address_service__WEBPACK_IMPORTED_MODULE_7__["UserAddressService"]
      }];
    };

    ShippingAddressPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-shipping-address',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./shipping-address.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/address-pages/shipping-address/shipping-address.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./shipping-address.page.scss */
      "./src/app/address-pages/shipping-address/shipping-address.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"], src_providers_user_address_user_address_service__WEBPACK_IMPORTED_MODULE_7__["UserAddressService"]])], ShippingAddressPage);
    /***/
  }
}]);
//# sourceMappingURL=address-pages-shipping-address-shipping-address-module-es5.js.map