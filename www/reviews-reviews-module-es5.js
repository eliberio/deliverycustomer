function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["reviews-reviews-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/reviews/reviews.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/reviews/reviews.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppReviewsReviewsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>{{'Ratings & Reviews'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"review-page\">\r\n  <ion-grid class=\"rating-gird\">\r\n    <ion-row class=\"ion-align-items-center\">\r\n      <ion-col size=\"5\">\r\n        <h1>{{average| number:'1.2-2'}}\r\n          <ion-icon name=\"star\"></ion-icon>\r\n        </h1>\r\n        <h3>{{reviews.length}} {{'rating'|translate}}</h3>\r\n      </ion-col>\r\n      <ion-col size=\"7\">\r\n        <ul>\r\n          <li>5\r\n            <ion-icon name=\"star\"></ion-icon>\r\n            <span class=\"block animate\" [style.width]=\"r5+'%'\" style=\"background-color:green;\"></span>\r\n          </li>\r\n          <li>4\r\n            <ion-icon name=\"star\"></ion-icon>\r\n            <span class=\"block animate\" [style.width]=\"r4+'%'\" style=\"background-color:lightgreen;\"></span>\r\n          </li>\r\n          <li>3\r\n            <ion-icon name=\"star\"></ion-icon>\r\n            <span class=\"block animate\" [style.width]=\"r3+'%'\" style=\"background-color:yellow;\"></span>\r\n          </li>\r\n          <li>2\r\n            <ion-icon name=\"star\"></ion-icon>\r\n            <span class=\"block animate\" [style.width]=\"r2+'%'\" style=\"background-color:orange;\"></span>\r\n          </li>\r\n          <li>1\r\n            <ion-icon name=\"star\"></ion-icon>\r\n            <span class=\"block animate\" [style.width]=\"r1+'%'\" style=\"background-color:darkorange;\"></span>\r\n          </li>\r\n        </ul>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-button expand=\"full\" color=\"danger\" (click)=\"openReviewsPage()\">{{'Rate and write a review'|translate}}\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <ion-list class=\"rating-users\">\r\n    <ion-item *ngFor=\"let r of reviews\">\r\n      <ion-avatar slot=\"start\">\r\n        <img src=\"assets/avatar.png\">\r\n      </ion-avatar>\r\n      <ion-label>\r\n        <h2>{{r.first_name}} {{r.last_name}}</h2>\r\n        <h3>{{r.created_at|date:'dd-MM-yyyy'}}</h3>\r\n        <p>{{r.comments}}</p>\r\n      </ion-label>\r\n      <div class=\"stars-outer\" slot=\"end\">\r\n        <div class=\"stars-inner\" [style.width]=\"(r.rating*20)+'%'\"></div>\r\n      </div>\r\n    </ion-item>\r\n  </ion-list>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/reviews/reviews.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/reviews/reviews.module.ts ***!
    \*******************************************/

  /*! exports provided: ReviewsPageModule */

  /***/
  function srcAppReviewsReviewsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReviewsPageModule", function () {
      return ReviewsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _reviews_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./reviews.page */
    "./src/app/reviews/reviews.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _reviews_page__WEBPACK_IMPORTED_MODULE_6__["ReviewsPage"]
    }];

    var ReviewsPageModule = function ReviewsPageModule() {
      _classCallCheck(this, ReviewsPageModule);
    };

    ReviewsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_reviews_page__WEBPACK_IMPORTED_MODULE_6__["ReviewsPage"]]
    })], ReviewsPageModule);
    /***/
  },

  /***/
  "./src/app/reviews/reviews.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/reviews/reviews.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppReviewsReviewsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "@charset \"UTF-8\";\n.review-page {\n  background-color: white;\n}\n.review-page .rating-gird h4 {\n  margin-top: 0;\n  margin-bottom: 0;\n}\n.review-page .rating-gird h1 {\n  font-size: 30px;\n  margin-top: 0;\n  text-align: center;\n}\n.review-page .rating-gird h3 {\n  color: var(--ion-color-secondary);\n  margin-bottom: 0;\n  text-align: center;\n}\n.review-page .rating-gird ul {\n  padding-left: 15px;\n  margin-bottom: 0;\n  border-left: 1px solid #ccc;\n}\n.review-page .rating-gird ul li {\n  display: flex;\n  align-items: center;\n  font-size: 16px;\n  list-style: none;\n  position: relative;\n}\n.review-page .rating-gird ul li .icon {\n  margin-left: 5px;\n}\n.review-page .rating-gird ul li .block {\n  height: 11px;\n  display: inline-block;\n  margin-left: 14px;\n  position: relative;\n  z-index: 2;\n}\n.review-page .rating-gird ul li::after {\n  content: \"\";\n  background-color: #f4f4f4;\n  height: 11px;\n  width: calc(100% - 36px);\n  position: absolute;\n  left: 36px;\n  z-index: 1;\n  top: 5px;\n}\n.review-page .rating-gird .button {\n  min-height: 45px;\n}\n.review-page .rating-users .item {\n  background-color: transparent;\n  align-items: flex-start;\n}\n.review-page .rating-users .item ion-avatar {\n  margin-top: 10px;\n}\n.review-page .rating-users .item h2 {\n  font-weight: bold;\n}\n.review-page .rating-users .item p p {\n  margin-top: 2px;\n  white-space: normal;\n}\n.review-page .animate {\n  -webkit-animation: fadeInRight 0.75s;\n          animation: fadeInRight 0.75s;\n}\n.review-page .stars-outer {\n  display: inline-block;\n  position: relative;\n  font-size: 15px;\n}\n.review-page .stars-outer::before {\n  content: \"☆☆☆☆☆\";\n  color: #ccc;\n}\n.review-page .stars-outer .stars-inner {\n  font-size: 15px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  white-space: nowrap;\n  overflow: hidden;\n}\n.review-page .stars-outer .stars-inner::before {\n  content: \"★★★★★\";\n  color: #f8ce0b;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmV2aWV3cy9yZXZpZXdzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcmV2aWV3cy9EOlxcRG9jdW1lbnRvc1xcUHJvZ3JhbWFjaW9uXFxKYXZhc2NyaXB0XFxJb25pY1xcZGVsaXZlcnljdXN0b21lci9zcmNcXGFwcFxccmV2aWV3c1xccmV2aWV3cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDQWhCO0VBQ0UsdUJBQUE7QURFRjtBQ0FJO0VBRUUsYUFBQTtFQUNBLGdCQUFBO0FEQ047QUNFSTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QURBTjtBQ0VJO0VBRUUsaUNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FERE47QUNHSTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwyQkFBQTtBREROO0FDRU07RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBREFSO0FDQ1E7RUFDRSxnQkFBQTtBRENWO0FDQ1E7RUFDRSxZQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBRENWO0FDQ1E7RUFDRSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBRENWO0FDR0k7RUFDRSxnQkFBQTtBREROO0FDS0k7RUFDRSw2QkFBQTtFQUNBLHVCQUFBO0FESE47QUNLTTtFQUNFLGdCQUFBO0FESFI7QUNLTTtFQUNFLGlCQUFBO0FESFI7QUNNUTtFQUNFLGVBQUE7RUFDQSxtQkFBQTtBREpWO0FDVUU7RUFDRSxvQ0FBQTtVQUFBLDRCQUFBO0FEUko7QUNXRTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FEVEo7QUNVSTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBRFJOO0FDVUk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QURSTjtBQ1VNO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FEUlIiLCJmaWxlIjoic3JjL2FwcC9yZXZpZXdzL3Jldmlld3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLnJldmlldy1wYWdlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG4ucmV2aWV3LXBhZ2UgLnJhdGluZy1naXJkIGg0IHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLWdpcmQgaDEge1xuICBmb250LXNpemU6IDMwcHg7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLWdpcmQgaDMge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLWdpcmQgdWwge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2NjYztcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLWdpcmQgdWwgbGkge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLWdpcmQgdWwgbGkgLmljb24ge1xuICBtYXJnaW4tbGVmdDogNXB4O1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctZ2lyZCB1bCBsaSAuYmxvY2sge1xuICBoZWlnaHQ6IDExcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLWxlZnQ6IDE0cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgei1pbmRleDogMjtcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLWdpcmQgdWwgbGk6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y0ZjRmNDtcbiAgaGVpZ2h0OiAxMXB4O1xuICB3aWR0aDogY2FsYygxMDAlIC0gMzZweCk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMzZweDtcbiAgei1pbmRleDogMTtcbiAgdG9wOiA1cHg7XG59XG4ucmV2aWV3LXBhZ2UgLnJhdGluZy1naXJkIC5idXR0b24ge1xuICBtaW4taGVpZ2h0OiA0NXB4O1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctdXNlcnMgLml0ZW0ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG59XG4ucmV2aWV3LXBhZ2UgLnJhdGluZy11c2VycyAuaXRlbSBpb24tYXZhdGFyIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLXVzZXJzIC5pdGVtIGgyIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4ucmV2aWV3LXBhZ2UgLnJhdGluZy11c2VycyAuaXRlbSBwIHAge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG59XG4ucmV2aWV3LXBhZ2UgLmFuaW1hdGUge1xuICBhbmltYXRpb246IGZhZGVJblJpZ2h0IDAuNzVzO1xufVxuLnJldmlldy1wYWdlIC5zdGFycy1vdXRlciB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IDE1cHg7XG59XG4ucmV2aWV3LXBhZ2UgLnN0YXJzLW91dGVyOjpiZWZvcmUge1xuICBjb250ZW50OiBcIuKYhuKYhuKYhuKYhuKYhlwiO1xuICBjb2xvcjogI2NjYztcbn1cbi5yZXZpZXctcGFnZSAuc3RhcnMtb3V0ZXIgLnN0YXJzLWlubmVyIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5yZXZpZXctcGFnZSAuc3RhcnMtb3V0ZXIgLnN0YXJzLWlubmVyOjpiZWZvcmUge1xuICBjb250ZW50OiBcIuKYheKYheKYheKYheKYhVwiO1xuICBjb2xvcjogI2Y4Y2UwYjtcbn0iLCIucmV2aWV3LXBhZ2Uge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIC5yYXRpbmctZ2lyZCB7XHJcbiAgICBoNCB7XHJcbiAgICAgIC8vZm9udC1zaXplOiAkaDItZm9udC1zaXplO1xyXG4gICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgfVxyXG5cclxuICAgIGgxIHtcclxuICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBoMyB7XHJcbiAgICAgIC8vZm9udC1zaXplOiAkaDItZm9udC1zaXplO1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICAgIHVsIHtcclxuICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgIGxpIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5pY29uIHtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5ibG9jayB7XHJcbiAgICAgICAgICBoZWlnaHQ6IDExcHg7XHJcbiAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMTRweDtcclxuICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgIHotaW5kZXg6IDI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjRmNGY0O1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMXB4O1xyXG4gICAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDM2cHgpO1xyXG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgbGVmdDogMzZweDtcclxuICAgICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgICB0b3A6IDVweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5idXR0b24ge1xyXG4gICAgICBtaW4taGVpZ2h0OiA0NXB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAucmF0aW5nLXVzZXJzIHtcclxuICAgIC5pdGVtIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG5cclxuICAgICAgaW9uLWF2YXRhciB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgfVxyXG4gICAgICBoMiB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgIH1cclxuICAgICAgcCB7XHJcbiAgICAgICAgcCB7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgICAgICAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmFuaW1hdGUge1xyXG4gICAgYW5pbWF0aW9uOiBmYWRlSW5SaWdodCAwLjc1cztcclxuICB9XHJcblxyXG4gIC5zdGFycy1vdXRlciB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAmOjpiZWZvcmUge1xyXG4gICAgICBjb250ZW50OiBcIlxcMjYwNlxcMjYwNlxcMjYwNlxcMjYwNlxcMjYwNlwiO1xyXG4gICAgICBjb2xvcjogI2NjYztcclxuICAgIH1cclxuICAgIC5zdGFycy1pbm5lciB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB0b3A6IDA7XHJcbiAgICAgIGxlZnQ6IDA7XHJcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcblxyXG4gICAgICAmOjpiZWZvcmUge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XCI7XHJcbiAgICAgICAgY29sb3I6ICNmOGNlMGI7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19 */";
    /***/
  },

  /***/
  "./src/app/reviews/reviews.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/reviews/reviews.page.ts ***!
    \*****************************************/

  /*! exports provided: ReviewsPage */

  /***/
  function srcAppReviewsReviewsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReviewsPage", function () {
      return ReviewsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var ReviewsPage = /*#__PURE__*/function () {
      function ReviewsPage(navCtrl, activatedRoute, applicationRef, config, modalCtrl, loading, shared) {
        _classCallCheck(this, ReviewsPage);

        this.navCtrl = navCtrl;
        this.activatedRoute = activatedRoute;
        this.applicationRef = applicationRef;
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.shared = shared;
        this.reviews = [];
        this.r1 = null;
        this.r2 = null;
        this.r3 = null;
        this.r4 = null;
        this.r5 = null;
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
      } //===============================================================================================================================


      _createClass(ReviewsPage, [{
        key: "getProductReviews",
        value: function getProductReviews() {
          var _this = this;

          this.loading.show();
          this.config.getHttp("getreviews?languages_id=" + this.config.langId + "&products_id=" + this.id).then(function (data) {
            _this.reviews = data.data;
            var total = 0;

            var _iterator = _createForOfIteratorHelper(_this.reviews),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var value = _step.value;
                total = total + value.rating;
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }

            _this.average = total / _this.reviews.length;
            if (_this.reviews.length == 0) _this.average = 0;

            _this.calculateAll();

            _this.loading.hide();
          });
        } //===============================================================================================================================

      }, {
        key: "openReviewsPage",
        value: function openReviewsPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    this.navCtrl.navigateForward("/add-review/" + this.id);

                  case 1:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        } //===============================================================================================================================
        // <!-- 2.0 updates -->

      }, {
        key: "totalRating",
        value: function totalRating() {
          var total = 0;

          var _iterator2 = _createForOfIteratorHelper(this.reviews),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var value = _step2.value;
              total = total + value.rating;
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }

          var result = total;
          if (total == 0) result = 0;
          return result;
        }
      }, {
        key: "calculateAll",
        value: function calculateAll() {
          var r1 = 0,
              r2 = 0,
              r3 = 0,
              r4 = 0,
              r5 = 0;
          var total = this.reviews.length;

          var _iterator3 = _createForOfIteratorHelper(this.reviews),
              _step3;

          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var value = _step3.value;
              if (value.rating == 1) r1++;
              if (value.rating == 2) r2++;
              if (value.rating == 3) r3++;
              if (value.rating == 4) r4++;
              if (value.rating == 5) r5++;
              console.log(value.rating);
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }

          console.log();
          this.r1 = 100 / total * r1;
          if (r1 == 0) this.r1 = 0;
          this.r2 = 100 / total * r2;
          if (r2 == 0) this.r2 = 0;
          this.r3 = 100 / total * r3;
          if (r3 == 0) this.r3 = 0;
          this.r4 = 100 / total * r4;
          if (r4 == 0) this.r4 = 0;
          this.r5 = 100 / total * r5;
          if (r5 == 0) this.r5 = 0;
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.getProductReviews();
        }
      }]);

      return ReviewsPage;
    }();

    ReviewsPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }];
    };

    ReviewsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-reviews',
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./reviews.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/reviews/reviews.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./reviews.page.scss */
      "./src/app/reviews/reviews.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]])], ReviewsPage);
    /***/
  }
}]);
//# sourceMappingURL=reviews-reviews-module-es5.js.map