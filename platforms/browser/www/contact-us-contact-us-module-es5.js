function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-us-contact-us-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppContactUsContactUsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      {{'Contact Us'| translate }}\r\n    </ion-title>\r\n\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div #map id=\"map\"></div>\r\n  <ion-card class=\"ion-no-padding\">\r\n    <ion-card-content class=\"ion-no-padding\">\r\n      <ion-item lines=\"none\">\r\n        <ion-avatar slot=\"start\">\r\n          <ion-icon name=\"pin\"></ion-icon>\r\n        </ion-avatar>\r\n        <ion-label>\r\n          <p wrap-text>\r\n            {{config.address}}\r\n          </p>\r\n        </ion-label>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <ion-avatar slot=\"start\">\r\n          <ion-icon name=\"mail\"></ion-icon>\r\n        </ion-avatar>\r\n        <ion-label>\r\n          <p>\r\n            {{config.email}}\r\n          </p>\r\n        </ion-label>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <ion-avatar slot=\"start\">\r\n          <ion-icon name=\"call\"></ion-icon>\r\n        </ion-avatar>\r\n\r\n        <ion-label>\r\n          <p>\r\n            {{config.phoneNo}}\r\n          </p>\r\n        </ion-label>\r\n      </ion-item>\r\n\r\n      <form #contactForm=\"ngForm\" (ngSubmit)=\"submit()\">\r\n        <ion-item>\r\n          <ion-label position=\"floating\">{{'Name'|translate}}</ion-label>\r\n          <ion-input type=\"text\" name=\"name\" [(ngModel)]=\"contact.name\" required>\r\n          </ion-input>\r\n        </ion-item>\r\n        <ion-item>\r\n          <ion-label position=\"floating\">{{'Email'|translate}}</ion-label>\r\n          <ion-input type=\"email\" name=\"email\" [(ngModel)]=\"contact.email\" required>\r\n          </ion-input>\r\n        </ion-item>\r\n        <ion-item>\r\n          <ion-label position=\"floating\">{{'Your Messsage'|translate}}</ion-label>\r\n          <ion-input type=\"text\" name=\"message\" [(ngModel)]=\"contact.message\" required>\r\n          </ion-input>\r\n        </ion-item>\r\n\r\n        <ion-col col-12>\r\n          <label *ngIf=\"errorMessage!=''\">\r\n            <span>{{errorMessage}}</span>\r\n          </label>\r\n        </ion-col>\r\n\r\n        <ion-button expand=\"block\" type=\"submit\" [disabled]=\"!contactForm.form.valid\">{{'Send'|translate}}</ion-button>\r\n\r\n      </form>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/contact-us/contact-us.module.ts ***!
    \*************************************************/

  /*! exports provided: ContactUsPageModule */

  /***/
  function srcAppContactUsContactUsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function () {
      return ContactUsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _contact_us_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./contact-us.page */
    "./src/app/contact-us/contact-us.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _contact_us_page__WEBPACK_IMPORTED_MODULE_6__["ContactUsPage"]
    }];

    var ContactUsPageModule = function ContactUsPageModule() {
      _classCallCheck(this, ContactUsPageModule);
    };

    ContactUsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_contact_us_page__WEBPACK_IMPORTED_MODULE_6__["ContactUsPage"]]
    })], ContactUsPageModule);
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/contact-us/contact-us.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppContactUsContactUsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content ion-card ion-card-content ion-item {\n  --inner-padding-end: 0;\n}\nion-content ion-card ion-card-content ion-item ion-avatar {\n  background: #eee;\n}\nion-content ion-card ion-card-content ion-item ion-avatar ion-icon {\n  width: 100%;\n  margin-top: 8px;\n  color: var(--ion-color-primary);\n  zoom: 1.3;\n}\nion-content ion-card ion-card-content ion-item ion-label p {\n  white-space: normal;\n  font-size: 14px;\n}\nion-content ion-card ion-card-content ion-item:last-child {\n  --inner-padding-end: 10px !important;\n  --padding-start: 10px !important;\n}\nion-content ion-card ion-card-content ion-item:last-child ion-button {\n  --background: #83af8c;\n  width: 100%;\n}\nion-content form ion-item {\n  --background: var(--ion-background-color);\n}\nion-content form ion-item ion-label {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\nion-content form ion-item ion-icon {\n  margin: auto;\n}\n#map {\n  width: 100%;\n  height: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFjdC11cy9EOlxcRG9jdW1lbnRvc1xcUHJvZ3JhbWFjaW9uXFxKYXZhc2NyaXB0XFxJb25pY1xcZGVsaXZlcnljdXN0b21lci9zcmNcXGFwcFxcY29udGFjdC11c1xcY29udGFjdC11cy5wYWdlLnNjc3MiLCJzcmMvYXBwL2NvbnRhY3QtdXMvY29udGFjdC11cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR007RUFDRSxzQkFBQTtBQ0ZSO0FER1E7RUFDRSxnQkFBQTtBQ0RWO0FERVU7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLCtCQUFBO0VBQ0EsU0FBQTtBQ0FaO0FESVU7RUFDRSxtQkFBQTtFQUNBLGVBQUE7QUNGWjtBREtRO0VBQ0Usb0NBQUE7RUFDQSxnQ0FBQTtBQ0hWO0FESVU7RUFDRSxxQkFBQTtFQUNBLFdBQUE7QUNGWjtBRFVJO0VBQ0UseUNBQUE7QUNSTjtBRFNNO0VBQ0UsMkNBQUE7QUNQUjtBRFNNO0VBQ0UsWUFBQTtBQ1BSO0FEYUE7RUFDRSxXQUFBO0VBQ0EsYUFBQTtBQ1ZGIiwiZmlsZSI6InNyYy9hcHAvY29udGFjdC11cy9jb250YWN0LXVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICBpb24tY2FyZCB7XHJcbiAgICBpb24tY2FyZC1jb250ZW50IHtcclxuICAgICAgaW9uLWl0ZW0ge1xyXG4gICAgICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XHJcbiAgICAgICAgaW9uLWF2YXRhciB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjZWVlO1xyXG4gICAgICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogOHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgICAgICAgICB6b29tOiAxLjM7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgICBwIHtcclxuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICBpb24tYnV0dG9uIHtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiAjODNhZjhjO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGZvcm0ge1xyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuICAgICAgaW9uLWxhYmVsIHtcclxuICAgICAgICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xyXG4gICAgICB9XHJcbiAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbiNtYXAge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMzAwcHg7XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCBpb24taXRlbSB7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG59XG5pb24tY29udGVudCBpb24tY2FyZCBpb24tY2FyZC1jb250ZW50IGlvbi1pdGVtIGlvbi1hdmF0YXIge1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xufVxuaW9uLWNvbnRlbnQgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCBpb24taXRlbSBpb24tYXZhdGFyIGlvbi1pY29uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDhweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgem9vbTogMS4zO1xufVxuaW9uLWNvbnRlbnQgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCBpb24taXRlbSBpb24tbGFiZWwgcCB7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbmlvbi1jb250ZW50IGlvbi1jYXJkIGlvbi1jYXJkLWNvbnRlbnQgaW9uLWl0ZW06bGFzdC1jaGlsZCB7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDEwcHggIWltcG9ydGFudDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4ICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCBpb24tY2FyZCBpb24tY2FyZC1jb250ZW50IGlvbi1pdGVtOmxhc3QtY2hpbGQgaW9uLWJ1dHRvbiB7XG4gIC0tYmFja2dyb3VuZDogIzgzYWY4YztcbiAgd2lkdGg6IDEwMCU7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIGlvbi1pY29uIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4jbWFwIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMzAwcHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/contact-us/contact-us.page.ts ***!
    \***********************************************/

  /*! exports provided: ContactUsPage */

  /***/
  function srcAppContactUsContactUsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactUsPage", function () {
      return ContactUsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/google-maps */
    "./node_modules/@ionic-native/google-maps/index.js");

    var ContactUsPage = /*#__PURE__*/function () {
      function ContactUsPage(http, config, loading, shared) {
        _classCallCheck(this, ContactUsPage);

        this.http = http;
        this.config = config;
        this.loading = loading;
        this.shared = shared;
        this.contact = {
          name: '',
          email: '',
          message: ''
        };
        this.errorMessage = '';
      }

      _createClass(ContactUsPage, [{
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.loadMap();
        }
      }, {
        key: "submit",
        value: function submit() {
          var _this = this;

          this.loading.autoHide(3000);
          var dat = {};
          dat = this.contact;
          this.config.postHttp('contactus', dat).then(function (data) {
            _this.loading.hide();

            if (data.success == 1) {
              _this.contact.name = '';
              _this.contact.email = '';
              _this.contact.message = '';

              _this.shared.toast(data.message);
            }
          });
        }
      }, {
        key: "loadMap",
        value: function loadMap() {
          var _this2 = this;

          /* The create() function will take the ID of your map element */
          var map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_6__["GoogleMaps"].create('map');

          map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_6__["GoogleMapsEvent"].MAP_READY).then(function (data) {
            var coordinates = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_6__["LatLng"](_this2.config.latitude, _this2.config.longitude);
            map.setCameraTarget(coordinates);
            map.setCameraZoom(15);
          });
          var marker = map.addMarkerSync({
            position: {
              lat: this.config.latitude,
              lng: this.config.longitude
            },
            title: this.config.address
          });
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ContactUsPage;
    }();

    ContactUsPage.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], ContactUsPage.prototype, "mapElement", void 0);
    ContactUsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-contact-us',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./contact-us.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./contact-us.page.scss */
      "./src/app/contact-us/contact-us.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]])], ContactUsPage);
    /***/
  }
}]);
//# sourceMappingURL=contact-us-contact-us-module-es5.js.map