function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["news-news-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppNewsNewsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      {{'News'| translate }}\r\n    </ion-title>\r\n\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"news-page\">\r\n  <!-- *ngIf=\"post.image\" -->\r\n  <ion-slides pager=true paginationType=\"bullets\" class=\"animate-item\">\r\n    <ion-slide *ngFor=\"let post of featuredPosts\" (click)=\"showPostDetail(post)\">\r\n      <ion-img src=\"{{config.imgUrl+post.news_image}}\" class=\"animate-item\"></ion-img>\r\n    </ion-slide>\r\n  </ion-slides>\r\n  <!-- top Segments  -->\r\n  <ion-segment [(ngModel)]=\"segments\">\r\n    <ion-segment-button value=\"newest\">{{'Newest' |translate}}</ion-segment-button>\r\n    <ion-segment-button value=\"categories\">{{ 'Categories' | translate }} </ion-segment-button>\r\n  </ion-segment>\r\n\r\n  <div [ngSwitch]=\"segments\">\r\n\r\n    <div *ngSwitchCase=\"'newest'\">\r\n      <ion-list class=\"posts-list\">\r\n        <ion-item lines=\"full\" *ngFor=\"let post of posts\" (click)=\"showPostDetail(post)\" class=\"animate-item\">\r\n          <ion-thumbnail slot=\"start\">\r\n            <ion-img src=\"{{config.imgUrl+post.news_image}}\"></ion-img>\r\n          </ion-thumbnail>\r\n          <ion-label>\r\n            <h2>{{post.news_name}}\r\n              <br>\r\n              <small>\r\n                <ion-icon name=\"time\"></ion-icon>\r\n                <ion-label>\r\n                  {{post.created_at|date}}\r\n                </ion-label>\r\n              </small>\r\n            </h2>\r\n            <p [innerHTML]=\"post.news_description\"></p>\r\n          </ion-label>\r\n        </ion-item>\r\n        <ion-infinite-scroll #infinite (ionInfinite)=\"getPosts()\">\r\n          <ion-infinite-scroll-content></ion-infinite-scroll-content>\r\n        </ion-infinite-scroll>\r\n      </ion-list>\r\n    </div>\r\n\r\n    <div *ngSwitchCase=\"'categories'\">\r\n      <ion-list class=\"categories-list\">\r\n        <ion-card *ngFor=\"let cat of categories\" (click)=\"openPostsPage(cat.name,cat.id)\" class=\"animate-item\">\r\n          <img *ngIf=\"cat.image\" src=\"{{config.imgUrl+cat.image}}\">\r\n          <div>\r\n            <h2>{{cat.name }}</h2>\r\n            <p>{{cat.total_news}} {{'Posts'|translate}}</p>\r\n          </div>\r\n        </ion-card>\r\n      </ion-list>\r\n    </div>\r\n  </div>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/news/news.module.ts":
  /*!*************************************!*\
    !*** ./src/app/news/news.module.ts ***!
    \*************************************/

  /*! exports provided: NewsPageModule */

  /***/
  function srcAppNewsNewsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsPageModule", function () {
      return NewsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _news_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./news.page */
    "./src/app/news/news.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _news_page__WEBPACK_IMPORTED_MODULE_6__["NewsPage"]
    }];

    var NewsPageModule = function NewsPageModule() {
      _classCallCheck(this, NewsPageModule);
    };

    NewsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_news_page__WEBPACK_IMPORTED_MODULE_6__["NewsPage"]]
    })], NewsPageModule);
    /***/
  },

  /***/
  "./src/app/news/news.page.scss":
  /*!*************************************!*\
    !*** ./src/app/news/news.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppNewsNewsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".news-page ion-segment {\n  border-bottom: 1px solid #dedede;\n}\n.news-page ion-segment ion-segment-button {\n  --indicator-color-checked: var(--ion-color-primary) !important;\n  --color-checked: var(--ion-color-primary);\n  --color: var(--ion-color-primary);\n}\n.news-page ion-slides ion-img {\n  width: 100%;\n}\n.news-page .posts-list ion-item ion-thumbnail {\n  margin-bottom: auto;\n}\n.news-page .posts-list ion-item ion-label h2 {\n  font-size: 16px;\n  font-weight: bold;\n  white-space: normal;\n}\n.news-page .posts-list ion-item ion-label h2 small {\n  display: flex;\n  align-items: center;\n  font-size: 14px;\n  color: #747474;\n  font-weight: normal;\n  margin-top: 5px;\n}\n.news-page .posts-list ion-item ion-label h2 small ion-label {\n  padding-left: 4px;\n}\n.news-page .posts-list ion-item ion-label p p:not(:first-child) {\n  display: none;\n}\n.news-page .posts-list ion-item ion-label p p:first-child {\n  white-space: normal;\n  line-height: 1.4;\n  -webkit-line-clamp: 4;\n  display: -webkit-box;\n  box-sizing: border-box;\n  -webkit-box-orient: vertical;\n}\n.news-page .categories-list {\n  padding-top: 0;\n  padding-bottom: 0;\n}\n.news-page .categories-list ion-card {\n  height: 235px;\n}\n.news-page .categories-list ion-card img {\n  -webkit-filter: brightness(0.6);\n          filter: brightness(0.6);\n  width: 100%;\n}\n.news-page .categories-list ion-card div {\n  position: absolute;\n  top: 32%;\n  text-align: center;\n  width: 100%;\n  color: white;\n  font-weight: bold;\n  height: 250px;\n}\n.news-page .categories-list ion-card div h2 {\n  margin-top: 18px;\n  font-size: var(--heading-font-size);\n  font-weight: bold;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.news-page .categories-list ion-card div p {\n  font-size: var(--sub-heading-font-size);\n  margin-top: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3cy9EOlxcRG9jdW1lbnRvc1xcUHJvZ3JhbWFjaW9uXFxKYXZhc2NyaXB0XFxJb25pY1xcZGVsaXZlcnljdXN0b21lci9zcmNcXGFwcFxcbmV3c1xcbmV3cy5wYWdlLnNjc3MiLCJzcmMvYXBwL25ld3MvbmV3cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxnQ0FBQTtBQ0FKO0FEQ0k7RUFDRSw4REFBQTtFQUNBLHlDQUFBO0VBQ0EsaUNBQUE7QUNDTjtBREdJO0VBQ0UsV0FBQTtBQ0ROO0FETU07RUFDRSxtQkFBQTtBQ0pSO0FET1E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQ0xWO0FETVU7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0paO0FES1k7RUFDRSxpQkFBQTtBQ0hkO0FEUVU7RUFDRSxhQUFBO0FDTlo7QURRVTtFQUNFLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ05aO0FEYUU7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7QUNYSjtBRGFJO0VBQ0UsYUFBQTtBQ1hOO0FEWU07RUFDRSwrQkFBQTtVQUFBLHVCQUFBO0VBQ0EsV0FBQTtBQ1ZSO0FEWU07RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0FDVlI7QURXUTtFQUNFLGdCQUFBO0VBQ0EsbUNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQ1RWO0FEV1E7RUFDRSx1Q0FBQTtFQUNBLGFBQUE7QUNUViIsImZpbGUiOiJzcmMvYXBwL25ld3MvbmV3cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmV3cy1wYWdlIHtcclxuICBpb24tc2VnbWVudCB7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RlZGVkZTtcclxuICAgIGlvbi1zZWdtZW50LWJ1dHRvbiB7XHJcbiAgICAgIC0taW5kaWNhdG9yLWNvbG9yLWNoZWNrZWQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgICAtLWNvbG9yLWNoZWNrZWQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgfVxyXG4gIH1cclxuICBpb24tc2xpZGVzIHtcclxuICAgIGlvbi1pbWcge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICB9XHJcbiAgLnBvc3RzLWxpc3Qge1xyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICBpb24tdGh1bWJuYWlsIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiBhdXRvO1xyXG4gICAgICB9XHJcbiAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgaDIge1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xyXG4gICAgICAgICAgc21hbGwge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNzQ3NDc0O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgICAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA0cHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcCB7XHJcbiAgICAgICAgICBwOm5vdCg6Zmlyc3QtY2hpbGQpIHtcclxuICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHA6Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMS40O1xyXG4gICAgICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDQ7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmNhdGVnb3JpZXMtbGlzdCB7XHJcbiAgICBwYWRkaW5nLXRvcDogMDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG5cclxuICAgIGlvbi1jYXJkIHtcclxuICAgICAgaGVpZ2h0OiAyMzVweDtcclxuICAgICAgaW1nIHtcclxuICAgICAgICBmaWx0ZXI6IGJyaWdodG5lc3MoMC42KTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgfVxyXG4gICAgICBkaXYge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDMyJTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGhlaWdodDogMjUwcHg7XHJcbiAgICAgICAgaDIge1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogMThweDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpO1xyXG4gICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogdmFyKC0tc3ViLWhlYWRpbmctZm9udC1zaXplKTtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsIi5uZXdzLXBhZ2UgaW9uLXNlZ21lbnQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RlZGVkZTtcbn1cbi5uZXdzLXBhZ2UgaW9uLXNlZ21lbnQgaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgLS1pbmRpY2F0b3ItY29sb3ItY2hlY2tlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpICFpbXBvcnRhbnQ7XG4gIC0tY29sb3ItY2hlY2tlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG4ubmV3cy1wYWdlIGlvbi1zbGlkZXMgaW9uLWltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm5ld3MtcGFnZSAucG9zdHMtbGlzdCBpb24taXRlbSBpb24tdGh1bWJuYWlsIHtcbiAgbWFyZ2luLWJvdHRvbTogYXV0bztcbn1cbi5uZXdzLXBhZ2UgLnBvc3RzLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIGgyIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbn1cbi5uZXdzLXBhZ2UgLnBvc3RzLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIGgyIHNtYWxsIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzc0NzQ3NDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuLm5ld3MtcGFnZSAucG9zdHMtbGlzdCBpb24taXRlbSBpb24tbGFiZWwgaDIgc21hbGwgaW9uLWxhYmVsIHtcbiAgcGFkZGluZy1sZWZ0OiA0cHg7XG59XG4ubmV3cy1wYWdlIC5wb3N0cy1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCBwIHA6bm90KDpmaXJzdC1jaGlsZCkge1xuICBkaXNwbGF5OiBub25lO1xufVxuLm5ld3MtcGFnZSAucG9zdHMtbGlzdCBpb24taXRlbSBpb24tbGFiZWwgcCBwOmZpcnN0LWNoaWxkIHtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgbGluZS1oZWlnaHQ6IDEuNDtcbiAgLXdlYmtpdC1saW5lLWNsYW1wOiA0O1xuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcbn1cbi5uZXdzLXBhZ2UgLmNhdGVnb3JpZXMtbGlzdCB7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbn1cbi5uZXdzLXBhZ2UgLmNhdGVnb3JpZXMtbGlzdCBpb24tY2FyZCB7XG4gIGhlaWdodDogMjM1cHg7XG59XG4ubmV3cy1wYWdlIC5jYXRlZ29yaWVzLWxpc3QgaW9uLWNhcmQgaW1nIHtcbiAgZmlsdGVyOiBicmlnaHRuZXNzKDAuNik7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm5ld3MtcGFnZSAuY2F0ZWdvcmllcy1saXN0IGlvbi1jYXJkIGRpdiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAzMiU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGhlaWdodDogMjUwcHg7XG59XG4ubmV3cy1wYWdlIC5jYXRlZ29yaWVzLWxpc3QgaW9uLWNhcmQgZGl2IGgyIHtcbiAgbWFyZ2luLXRvcDogMThweDtcbiAgZm9udC1zaXplOiB2YXIoLS1oZWFkaW5nLWZvbnQtc2l6ZSk7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbn1cbi5uZXdzLXBhZ2UgLmNhdGVnb3JpZXMtbGlzdCBpb24tY2FyZCBkaXYgcCB7XG4gIGZvbnQtc2l6ZTogdmFyKC0tc3ViLWhlYWRpbmctZm9udC1zaXplKTtcbiAgbWFyZ2luLXRvcDogMDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/news/news.page.ts":
  /*!***********************************!*\
    !*** ./src/app/news/news.page.ts ***!
    \***********************************/

  /*! exports provided: NewsPage */

  /***/
  function srcAppNewsNewsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsPage", function () {
      return NewsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var NewsPage = /*#__PURE__*/function () {
      function NewsPage(navCtrl, config, loading, shared) {
        var _this2 = this;

        _classCallCheck(this, NewsPage);

        this.navCtrl = navCtrl;
        this.config = config;
        this.loading = loading;
        this.shared = shared;
        this.featuredPosts = new Array();
        this.segments = 'newest'; //WordPress intergation

        this.categories = new Array(); //page varible

        this.page = 0; //WordPress intergation

        this.posts = new Array(); //page varible

        this.page2 = 0;
        this.httpRunning = true; //========================================= tab newest categories ===============================================================================

        this.getCategories = function () {
          var _this = this;

          var dat = {};
          dat.language_id = this.config.langId;
          dat.currency_code = this.config.currecnyCode;
          dat.page_number = this.page2;
          this.config.postHttp('allnewscategories', dat).then(function (data) {
            if (_this.page2 == 0) {
              _this.categories = [];
            }

            if (data.success == 1) {
              _this.page2++;
              data.data.forEach(function (value, index) {
                _this.categories.push(value);
              }); // console.log(data.data.length);

              _this.getCategories();
            }

            if (data.data.length < 9) {
              // if we get less than 10 products then infinite scroll will de disabled
              if (_this.categories.length != 0) {//this.shared.toast('All Categories Loaded!');
              }
            }
          }, function (response) {// console.log("Error while loading categories from the server");
            // console.log(response);
          });
        };

        var dat = {};
        dat.language_id = this.config.langId;
        dat.currency_code = this.config.currecnyCode;
        dat.is_feature = 1;
        this.config.postHttp('getallnews', dat).then(function (data) {
          _this2.featuredPosts = data.news_data;
        });
        this.getPosts();
        this.getCategories();
      } //============================================================================================  
      //getting list of posts


      _createClass(NewsPage, [{
        key: "getPosts",
        value: function getPosts() {
          var _this3 = this;

          this.httpRunning = true;
          var dat = {};
          dat.language_id = this.config.langId;
          dat.currency_code = this.config.currecnyCode;
          dat.page_number = this.page;
          this.config.postHttp('getallnews', dat).then(function (data) {
            _this3.httpRunning = false;

            _this3.infinite.complete(); //stopping infinite scroll loader


            if (_this3.page == 0) {
              _this3.posts = [];
              _this3.infinite.disabled == false;
            }

            if (data.success == 1) {
              _this3.page++;
              data.news_data.forEach(function (value, index) {
                _this3.posts.push(value);
              });
            }

            if (data.news_data.length < 9) {
              // if we get less than 10 products then infinite scroll will de disabled
              //disabling infinite scroll
              _this3.infinite.disabled == true;

              if (_this3.posts.length != 0) {// this.shared.toast('All Posts Loaded!');
              }
            }
          }, function (response) {// console.log("Error while loading posts from the server");
            // console.log(response);
          });
        }
      }, {
        key: "showPostDetail",
        //============================================================================================  
        //getting list of sub categories from the server
        value: function showPostDetail(post) {
          this.shared.singlePostData = post;
          console.log(this.shared.singlePostData);
          this.navCtrl.navigateForward(this.config.currentRoute + "/news-detail");
        }
      }, {
        key: "openPostsPage",
        value: function openPostsPage(name, id) {
          this.navCtrl.navigateForward(this.config.currentRoute + "/news-list/" + id + "/" + name);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return NewsPage;
    }();

    NewsPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"], {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])], NewsPage.prototype, "infinite", void 0);
    NewsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-news',
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./news.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./news.page.scss */
      "./src/app/news/news.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]])], NewsPage);
    /***/
  }
}]);
//# sourceMappingURL=news-news-module-es5.js.map