(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["address-pages-billing-address-billing-address-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/address-pages/billing-address/billing-address.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/address-pages/billing-address/billing-address.page.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title> {{'Billing Address'| translate }} </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <form #loginForm=\"ngForm\">\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'First Name'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"shipping_firstname\" [(ngModel)]=\"shared.orderDetails.billing_firstname\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Last Name'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"shipping_lastname\" [(ngModel)]=\"shared.orderDetails.billing_lastname\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <!-- <div *ngIf=\"config.enableAddressMap\">\r\n      <ion-item (click)=\"showGoogleMap()\">\r\n        <ion-label position=\"floating\">{{'Location'|translate}}</ion-label>\r\n        <ion-input type=\"text\" name=\"location\" [(ngModel)]=\"shared.orderDetails.billing_location\" readonly required>\r\n        </ion-input>\r\n        <ion-icon name=\"location\" slot=\"end\"></ion-icon>\r\n      </ion-item>\r\n      <app-google-map [page]=\"'billing'\" *ngIf=\"showMap\" (locationUpdated)=\"locationUpdated()\"></app-google-map>\r\n    </div> -->\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Address'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"address\" [(ngModel)]=\"shared.orderDetails.billing_street_address\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n   <!-- <ion-item>\r\n      <ion-label position=\"floating\">{{'Country'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"shipping_country\" tappable (click)=\"selectCountryPage()\" readonly\r\n        [(ngModel)]=\"shared.orderDetails.billing_country\" required></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'State'|translate}}</ion-label>\r\n      <ion-input type=\"text\" required name=\"shipping_zone\" tappable (click)=\"selectZonePage()\" readonly\r\n        [(ngModel)]=\"shared.orderDetails.billing_zone\"></ion-input>\r\n    </ion-item>-->\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'City'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"shipping_city\" [(ngModel)]=\"shared.orderDetails.billing_city\" required></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Post code'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"shipping_postcode\" [(ngModel)]=\"shared.orderDetails.billing_postcode\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Phone'|translate}}</ion-label>\r\n      <ion-input type=\"number\" inputmode=\"tel\" name=\"Phone\" [(ngModel)]=\"shared.orderDetails.billing_phone\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n  </form>\r\n  <ion-item>\r\n    <ion-label>{{'Same as Shipping Address'|translate}}</ion-label>\r\n    <ion-checkbox [(ngModel)]=\"defaultAddress\" (click)=\"checkBoxOnChange()\"></ion-checkbox>\r\n  </ion-item>\r\n</ion-content>\r\n<ion-footer>\r\n  <ion-button expand=\"full\" color=\"secondary\" (click)=\"submit()\" [disabled]=\"!loginForm.form.valid\">{{'Next'|translate}}\r\n  </ion-button>\r\n</ion-footer>\r\n");

/***/ }),

/***/ "./src/app/address-pages/billing-address/billing-address.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/address-pages/billing-address/billing-address.module.ts ***!
  \*************************************************************************/
/*! exports provided: BillingAddressPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillingAddressPageModule", function() { return BillingAddressPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _billing_address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./billing-address.page */ "./src/app/address-pages/billing-address/billing-address.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");
/* harmony import */ var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/components/share/share.module */ "./src/components/share/share.module.ts");







// For Translation Language


const routes = [
    {
        path: '',
        component: _billing_address_page__WEBPACK_IMPORTED_MODULE_6__["BillingAddressPage"]
    }
];
let BillingAddressPageModule = class BillingAddressPageModule {
};
BillingAddressPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"]
        ],
        declarations: [_billing_address_page__WEBPACK_IMPORTED_MODULE_6__["BillingAddressPage"]]
    })
], BillingAddressPageModule);



/***/ }),

/***/ "./src/app/address-pages/billing-address/billing-address.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/address-pages/billing-address/billing-address.page.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content p {\n  font-size: 20px;\n  text-align: center;\n}\nion-content form ion-item {\n  --background: var(--ion-background-color);\n}\nion-content form ion-item ion-label {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\nion-content form ion-item ion-icon {\n  margin: auto;\n}\nion-footer .toolbar {\n  padding-left: 10px;\n}\nion-footer ion-button {\n  height: 45px;\n  margin: 0;\n  margin-bottom: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRkcmVzcy1wYWdlcy9iaWxsaW5nLWFkZHJlc3MvRDpcXERvY3VtZW50b3NcXFByb2dyYW1hY2lvblxcSmF2YXNjcmlwdFxcSW9uaWNcXGRlbGl2ZXJ5Y3VzdG9tZXIvc3JjXFxhcHBcXGFkZHJlc3MtcGFnZXNcXGJpbGxpbmctYWRkcmVzc1xcYmlsbGluZy1hZGRyZXNzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvYWRkcmVzcy1wYWdlcy9iaWxsaW5nLWFkZHJlc3MvYmlsbGluZy1hZGRyZXNzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFRTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQ0RKO0FESUk7RUFDRSx5Q0FBQTtBQ0ZOO0FER007RUFDRSwyQ0FBQTtBQ0RSO0FER007RUFDRSxZQUFBO0FDRFI7QURPRTtFQUNFLGtCQUFBO0FDSko7QURNRTtFQUNFLFlBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7QUNKSiIsImZpbGUiOiJzcmMvYXBwL2FkZHJlc3MtcGFnZXMvYmlsbGluZy1hZGRyZXNzL2JpbGxpbmctYWRkcmVzcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuaW9uLWNvbnRlbnQge1xyXG4gIHAge1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICBmb3JtIHtcclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcclxuICAgICAgfVxyXG4gICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbmlvbi1mb290ZXIge1xyXG4gIC50b29sYmFyIHtcclxuICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICB9XHJcbiAgaW9uLWJ1dHRvbiB7XHJcbiAgICBoZWlnaHQ6IDQ1cHg7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gIH1cclxufVxyXG4iLCJpb24tY29udGVudCBwIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIGlvbi1pY29uIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG5pb24tZm9vdGVyIC50b29sYmFyIHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuaW9uLWZvb3RlciBpb24tYnV0dG9uIHtcbiAgaGVpZ2h0OiA0NXB4O1xuICBtYXJnaW46IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59Il19 */");

/***/ }),

/***/ "./src/app/address-pages/billing-address/billing-address.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/address-pages/billing-address/billing-address.page.ts ***!
  \***********************************************************************/
/*! exports provided: BillingAddressPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillingAddressPage", function() { return BillingAddressPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_modals_select_country_select_country_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/modals/select-country/select-country.page */ "./src/app/modals/select-country/select-country.page.ts");
/* harmony import */ var src_app_modals_select_zones_select_zones_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/modals/select-zones/select-zones.page */ "./src/app/modals/select-zones/select-zones.page.ts");
/* harmony import */ var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/providers/loading/loading.service */ "./src/providers/loading/loading.service.ts");
/* harmony import */ var src_providers_user_address_user_address_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/providers/user-address/user-address.service */ "./src/providers/user-address/user-address.service.ts");









let BillingAddressPage = class BillingAddressPage {
    constructor(config, shared, modalCtrl, navCtrl, applicationRef, loading, userAddress) {
        this.config = config;
        this.shared = shared;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.applicationRef = applicationRef;
        this.loading = loading;
        this.userAddress = userAddress;
        this.defaultAddress = true;
        this.showMap = false;
        if (this.shared.orderDetails.billing_firstname == "")
            this.setAddress(true);
    }
    checkBoxOnChange() {
        this.setAddress(!this.defaultAddress);
    }
    setAddress(value) {
        if (value == true) {
            this.shared.orderDetails.billing_firstname = this.shared.orderDetails.delivery_firstname;
            this.shared.orderDetails.billing_lastname = this.shared.orderDetails.delivery_lastname;
            this.shared.orderDetails.billing_state = this.shared.orderDetails.delivery_state;
            this.shared.orderDetails.billing_city = this.shared.orderDetails.delivery_city;
            this.shared.orderDetails.billing_postcode = this.shared.orderDetails.delivery_postcode;
            this.shared.orderDetails.billing_zone = this.shared.orderDetails.delivery_zone;
            this.shared.orderDetails.billing_country = this.shared.orderDetails.delivery_country;
            this.shared.orderDetails.billing_country_id = this.shared.orderDetails.delivery_country_id;
            this.shared.orderDetails.billing_street_address = this.shared.orderDetails.delivery_street_address;
            this.shared.orderDetails.billing_phone = this.shared.orderDetails.delivery_phone;
            this.shared.orderDetails.billing_lat = this.shared.orderDetails.delivery_lat;
            this.shared.orderDetails.billing_long = this.shared.orderDetails.delivery_long;
            this.shared.orderDetails.billing_location = this.shared.orderDetails.delivery_location;
        }
        else {
            this.shared.orderDetails.billing_firstname = '';
            this.shared.orderDetails.billing_lastname = '';
            this.shared.orderDetails.billing_state = '';
            this.shared.orderDetails.billing_city = '';
            this.shared.orderDetails.billing_postcode = '';
            this.shared.orderDetails.billing_zone = '';
            this.shared.orderDetails.billing_country = '';
            this.shared.orderDetails.billing_country_id = '';
            this.shared.orderDetails.billing_street_address = '';
            this.shared.orderDetails.billing_phone = "";
            this.shared.orderDetails.billing_lat = "";
            this.shared.orderDetails.billing_long = "";
            this.shared.orderDetails.billing_location = "";
        }
    }
    submit() {
        this.navCtrl.navigateForward(this.config.currentRoute + "/shipping-method");
        this.applicationRef.tick();
    }
    selectCountryPage() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let modal = yield this.modalCtrl.create({
                component: src_app_modals_select_country_select_country_page__WEBPACK_IMPORTED_MODULE_5__["SelectCountryPage"],
                componentProps: { page: 'billing' }
            });
            return yield modal.present();
        });
    }
    selectZonePage() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let modal = yield this.modalCtrl.create({
                component: src_app_modals_select_zones_select_zones_page__WEBPACK_IMPORTED_MODULE_6__["SelectZonesPage"],
                componentProps: { page: 'billing', id: this.shared.orderDetails.billing_country_id }
            });
            return yield modal.present();
        });
    }
    showGoogleMap() {
        this.showMap = true;
    }
    locationUpdated() {
        this.showMap = false;
    }
    getLocationAddress() {
        //this.loading.show();
        let locationEnable = false;
        this.userAddress.getCordinates().then((value) => {
            locationEnable = true;
            //this.loading.hide();
            this.shared.orderDetails.billing_lat = value.lat;
            this.shared.orderDetails.billing_long = value.long;
            this.shared.orderDetails.billing_location = value.lat + ", " + value.long;
        });
        setTimeout(() => {
            if (locationEnable == false) {
                this.shared.showAlert("Please Turn On Device Location");
            }
        }, 10000);
    }
    ngOnInit() {
        if (this.config.enableAddressMap && this.shared.orderDetails.billing_location == "")
            this.getLocationAddress();
    }
    ionViewWillEnter() {
        // if (this.shared.customerData.customers_id == null) {
        //   this.setAddress(false);
        // }
    }
};
BillingAddressPage.ctorParameters = () => [
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"] },
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"] },
    { type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_7__["LoadingService"] },
    { type: src_providers_user_address_user_address_service__WEBPACK_IMPORTED_MODULE_8__["UserAddressService"] }
];
BillingAddressPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-billing-address',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./billing-address.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/address-pages/billing-address/billing-address.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./billing-address.page.scss */ "./src/app/address-pages/billing-address/billing-address.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"],
        src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"],
        src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_7__["LoadingService"],
        src_providers_user_address_user_address_service__WEBPACK_IMPORTED_MODULE_8__["UserAddressService"]])
], BillingAddressPage);



/***/ })

}]);
//# sourceMappingURL=address-pages-billing-address-billing-address-module-es2015.js.map