function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-pages-home10-home10-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home10/home10.page.html":
  /*!******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home10/home10.page.html ***!
    \******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomePagesHome10Home10PageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n   \r\n        <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title mode=ios>\r\n      {{config.appName|translate}}\r\n      <!-- <ion-img src=\"assets/logo_header.png\" alt=\"logo\"></ion-img> -->\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n        <ion-icon name=\"basket\"></ion-icon>\r\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    \r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"apps\"></ion-icon>\r\n      {{'Categories' | translate }}\r\n    </ion-button>\r\n    <ion-button id=\"second\" fill=\"clear\" (click)=\"openCategoryPage()\">\r\n      <ion-icon slot=\"end\" name=\"caret-forward\"></ion-icon>\r\n      {{ 'Shop More' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n  <!-- categories component -->\r\n  <app-categories [type]=\"'withName'\"></app-categories>\r\n\r\n  <!-- For Banner -->\r\n  <app-banner></app-banner>\r\n\r\n  <!-- For Vendor List -->\r\n  <!--<app-vendor-list></app-vendor-list>-->\r\n\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"albums\"></ion-icon>\r\n      {{'Top Seller Products'|translate}}\r\n    </ion-button>\r\n    <ion-button id=\"second\" fill=\"clear\" (click)=\"openProducts('top seller')\">\r\n      <ion-icon slot=\"end\" name=\"caret-forward\"></ion-icon>\r\n      {{ 'Shop More' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <!-- Newest Products swipe slider -->\r\n  <ion-slides [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.tab1\">\r\n      <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n  <!-- For Segments Products -->\r\n  <ion-segment [(ngModel)]=\"segments\">\r\n    <ion-segment-button value=\"sale\">{{ 'Deals' | translate }} </ion-segment-button>\r\n    <ion-segment-button value=\"mostLiked\"> {{ 'Most Liked' | translate }}</ion-segment-button>\r\n  </ion-segment>\r\n\r\n  <div [ngSwitch]=\"segments\">\r\n    <ion-grid class=\"ion-no-padding\" *ngSwitchCase=\"'sale'\">\r\n      <ion-row class=\"row-segment ion-no-padding\">\r\n        <ion-col *ngFor=\"let p of shared.tab2\" size=\"6\" class=\"ion-no-padding\">\r\n          <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row class=\"ion-text-center\">\r\n        <ion-col>\r\n          <ion-button fill=\"clear\" color=\"secondary\" (click)=\"openProducts('special')\">\r\n            {{ 'Shop More' | translate}}\r\n            <ion-icon name=\"caret-forward\"></ion-icon>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n\r\n    <ion-grid class=\"ion-no-padding\" *ngSwitchCase=\"'mostLiked'\">\r\n      <ion-row class=\"ion-no-padding\">\r\n        <ion-col *ngFor=\"let p of shared.tab3\" size=\"6\" class=\"ion-no-padding\">\r\n          <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row class=\"ion-text-center\">\r\n        <ion-col>\r\n          <ion-button fill=\"clear\" color=\"secondary\" (click)=\"openProducts('most liked')\">{{ 'Shop More' | translate}}\r\n            <ion-icon name=\"caret-forward\"></ion-icon>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </div>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/home-pages/home10/home10.module.ts":
  /*!****************************************************!*\
    !*** ./src/app/home-pages/home10/home10.module.ts ***!
    \****************************************************/

  /*! exports provided: Home10PageModule */

  /***/
  function srcAppHomePagesHome10Home10ModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Home10PageModule", function () {
      return Home10PageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _home10_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home10.page */
    "./src/app/home-pages/home10/home10.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");
    /* harmony import */


    var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/components/share/share.module */
    "./src/components/share/share.module.ts");

    var routes = [{
      path: '',
      component: _home10_page__WEBPACK_IMPORTED_MODULE_6__["Home10Page"]
    }];

    var Home10PageModule = function Home10PageModule() {
      _classCallCheck(this, Home10PageModule);
    };

    Home10PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"]],
      declarations: [_home10_page__WEBPACK_IMPORTED_MODULE_6__["Home10Page"]]
    })], Home10PageModule);
    /***/
  },

  /***/
  "./src/app/home-pages/home10/home10.page.scss":
  /*!****************************************************!*\
    !*** ./src/app/home-pages/home10/home10.page.scss ***!
    \****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomePagesHome10Home10PageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content .swiper-slide {\n  width: 40%;\n}\nion-content ion-segment ion-segment-button {\n  font-size: 11.3px;\n}\nion-content ion-slides ion-slide:last-child {\n  height: auto;\n}\nion-content app-product {\n  width: 100%;\n}\nion-content .row-segment {\n  margin-right: 10px;\n}\nion-content ion-grid ion-row {\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS1wYWdlcy9ob21lMTAvRDpcXERvY3VtZW50b3NcXFByb2dyYW1hY2lvblxcSmF2YXNjcmlwdFxcSW9uaWNcXGRlbGl2ZXJ5Y3VzdG9tZXIvc3JjXFxhcHBcXGhvbWUtcGFnZXNcXGhvbWUxMFxcaG9tZTEwLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS1wYWdlcy9ob21lMTAvaG9tZTEwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLFVBQUE7QUNBSjtBRElJO0VBQ0UsaUJBQUE7QUNGTjtBRE9NO0VBQ0UsWUFBQTtBQ0xSO0FEU0U7RUFDRSxXQUFBO0FDUEo7QURTRTtFQUNFLGtCQUFBO0FDUEo7QURVSTtFQUNFLGtCQUFBO0FDUk4iLCJmaWxlIjoic3JjL2FwcC9ob21lLXBhZ2VzL2hvbWUxMC9ob21lMTAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIC5zd2lwZXItc2xpZGUge1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICB9XHJcbiAgLy8gaWYgZGF0YSBsb2FkXHJcbiAgaW9uLXNlZ21lbnQge1xyXG4gICAgaW9uLXNlZ21lbnQtYnV0dG9uIHtcclxuICAgICAgZm9udC1zaXplOiAxMS4zcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGlvbi1zbGlkZXMge1xyXG4gICAgaW9uLXNsaWRlIHtcclxuICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgYXBwLXByb2R1Y3Qge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIC5yb3ctc2VnbWVudCB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgfVxyXG4gIGlvbi1ncmlkIHtcclxuICAgIGlvbi1yb3cge1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsImlvbi1jb250ZW50IC5zd2lwZXItc2xpZGUge1xuICB3aWR0aDogNDAlO1xufVxuaW9uLWNvbnRlbnQgaW9uLXNlZ21lbnQgaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgZm9udC1zaXplOiAxMS4zcHg7XG59XG5pb24tY29udGVudCBpb24tc2xpZGVzIGlvbi1zbGlkZTpsYXN0LWNoaWxkIHtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuaW9uLWNvbnRlbnQgYXBwLXByb2R1Y3Qge1xuICB3aWR0aDogMTAwJTtcbn1cbmlvbi1jb250ZW50IC5yb3ctc2VnbWVudCB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbmlvbi1jb250ZW50IGlvbi1ncmlkIGlvbi1yb3cge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/home-pages/home10/home10.page.ts":
  /*!**************************************************!*\
    !*** ./src/app/home-pages/home10/home10.page.ts ***!
    \**************************************************/

  /*! exports provided: Home10Page */

  /***/
  function srcAppHomePagesHome10Home10PageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Home10Page", function () {
      return Home10Page;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var Home10Page = /*#__PURE__*/function () {
      function Home10Page(nav, config, appEventsService, shared) {
        _classCallCheck(this, Home10Page);

        this.nav = nav;
        this.config = config;
        this.appEventsService = appEventsService;
        this.shared = shared;
        this.segments = "sale"; //first segment by default 
        //for product slider after banner

        this.sliderConfig = {
          slidesPerView: this.config.productSlidesPerPage,
          spaceBetween: 0
        };
      }

      _createClass(Home10Page, [{
        key: "openCategoryPage",
        value: function openCategoryPage() {
          this.nav.navigateForward(this.config.currentRoute + "/" + this.config.getCurrentCategoriesPage() + "/0/0");
        }
      }, {
        key: "openProducts",
        value: function openProducts(value) {
          this.nav.navigateForward(this.config.currentRoute + "/products/0/0/" + value);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.shared.hideSplashScreen();
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          if (!this.config.appInProduction) {
            this.config.productCardStyle = "22";
          }
        }
      }]);

      return Home10Page;
    }();

    Home10Page.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]
      }];
    };

    Home10Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home10',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home10.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home10/home10.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home10.page.scss */
      "./src/app/home-pages/home10/home10.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]])], Home10Page);
    /***/
  }
}]);
//# sourceMappingURL=home-pages-home10-home10-module-es5.js.map