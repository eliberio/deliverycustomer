function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/components/banner/banner.component.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/banner/banner.component.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcComponentsBannerBannerComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"banner-component\">\r\n  <div *ngIf=\"shared.banners.length==0\">\r\n    <ion-skeleton-text animated style=\"height: 211px;\">\r\n    </ion-skeleton-text>\r\n  </div>\r\n  <div *ngIf=\"shared.banners.length!=0\">\r\n\r\n    <ion-slides pager=\"true\" [options]=\"slideOpts\" [class]=\"config.bannerStyle\">\r\n      <ion-slide *ngFor=\"let b of shared.banners\" class=\"ion-no-padding animate-item\" (click)=\"bannerClick(b)\">\r\n        <img src=\"{{config.imgUrl+b.image}}\">\r\n      </ion-slide>\r\n    </ion-slides>\r\n  </div>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/components/categories/categories.component.html":
  /*!***************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/categories/categories.component.html ***!
    \***************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcComponentsCategoriesCategoriesComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- For Home-6 -->\r\n<!-- =========================================================== with name and product -->\r\n<div class=\"name\" *ngIf=\"type=='withName'\">\r\n\r\n  <ion-slides class=\"animate-item\" [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let c of shared.categories\" (click)=\"openSubCategories(c)\">\r\n      <!-- For Real Products -->\r\n      <ion-card *ngIf=\"c!=1\">\r\n\r\n        <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.image}}\" />\r\n\r\n        <ion-text>\r\n          <h6>{{c.name}}</h6>\r\n          <p>{{c.total_products}} {{'Products'|translate}}</p>\r\n        </ion-text>\r\n      </ion-card>\r\n      <!-- For Skeleton -->\r\n      <ion-card *ngIf=\"c==1\" class=\"skeleton-name\">\r\n        <ion-skeleton-text animated>\r\n        </ion-skeleton-text>\r\n        <p>\r\n          <ion-skeleton-text animated>\r\n          </ion-skeleton-text>\r\n        </p>\r\n      </ion-card>\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n</div>\r\n\r\n<!-- For Home-7 -->\r\n<!-- =========================================================== with name and product count -->\r\n<div class=\"name-count\" *ngIf=\"type=='name&count'\">\r\n  <ion-slides class=\"animate-item\" [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let c of shared.categories\" (click)=\"openSubCategories(c)\">\r\n      <!-- For Real Products -->\r\n      <ion-card *ngIf=\"c!=1\" class=\"card-h7\">\r\n        <ion-avatar>\r\n          <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.image}}\" />\r\n        </ion-avatar>\r\n        <ion-text>\r\n          <h6>{{c.name}}</h6>\r\n          <p>{{c.total_products}} {{'Products'|translate}}</p>\r\n        </ion-text>\r\n      </ion-card>\r\n      <!-- For Skeleton -->\r\n      <ion-card *ngIf=\"c==1\">\r\n        <ion-skeleton-text animated>\r\n        </ion-skeleton-text>\r\n        <p>\r\n          <ion-skeleton-text animated>\r\n          </ion-skeleton-text>\r\n        </p>\r\n      </ion-card>\r\n    </ion-slide>\r\n  </ion-slides>\r\n</div>\r\n\r\n<!-- For Home-8 -->\r\n<!-- =========================================================== with name and round image -->\r\n<div class=\"round\" *ngIf=\"type=='roundImage'\">\r\n  <ion-slides class=\"animate-item\" [options]=\"sliderConfig2\">\r\n    <ion-slide *ngFor=\"let c of shared.categories\" (click)=\"openSubCategories(c)\">\r\n      <!-- For Real Products -->\r\n      <ion-card *ngIf=\"c!=1\" class=\"card-h8\">\r\n        <ion-avatar>\r\n          <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.image}}\" />\r\n        </ion-avatar>\r\n        <ion-text>\r\n          <h6>{{c.name}}</h6>\r\n          <p>{{c.total_products}} {{'Products'|translate}}</p>\r\n        </ion-text>\r\n\r\n      </ion-card>\r\n      <!-- For Skeleton -->\r\n      <ion-card *ngIf=\"c==1\" class=\"card-skeleton-h8\">\r\n        <ion-avatar>\r\n          <ion-skeleton-text class=\"skeleton-h8\" animated>\r\n          </ion-skeleton-text>\r\n        </ion-avatar>\r\n        <p>\r\n          <ion-skeleton-text animated>\r\n          </ion-skeleton-text>\r\n        </p>\r\n      </ion-card>\r\n    </ion-slide>\r\n  </ion-slides>\r\n</div>\r\n\r\n<!-- // For Home-9 -->\r\n<!-- =========================================================== with name and round image -->\r\n<div class=\"grid\" *ngIf=\"type=='grid'\">\r\n  <ion-grid class=\"ion-no-padding categories-grid\">\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col size=\"4\" *ngFor=\"let c of shared.categories | slice:0:6;\" (click)=\"openSubCategories(c)\">\r\n        <ion-card class=\"card-h9\">\r\n          <ion-avatar class=\"avatar-h9\">\r\n            <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.image}}\" />\r\n          </ion-avatar>\r\n          <ion-text>\r\n            <h6>{{c.name}}</h6>\r\n            <p>{{c.total_products}} {{'Products'|translate}}</p>\r\n          </ion-text>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/components/google-map/google-map.component.html":
  /*!***************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/google-map/google-map.component.html ***!
    \***************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcComponentsGoogleMapGoogleMapComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"map-component ion-text-center\">\r\n  <ion-button (click)=\"setThisAddress()\">\r\n    {{'Set this Location'|translate}}\r\n  </ion-button>\r\n  <ion-row>\r\n    <ion-text color=\"danger\">\r\n      {{'Press And Hold The Marker To set Location'|translate}}\r\n    </ion-text>\r\n  </ion-row>\r\n  <div id=\"mapcomponent\"></div>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/components/product/product.component.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/product/product.component.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcComponentsProductProductComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"product-component animate-item\">\r\n  <!-- skeleton text until data not loaded -->\r\n  <div *ngIf=\"p==1\" style=\"padding-left: 10px;\" class=\"ion-text-center\">\r\n    <ion-skeleton-text animated style=\"width: 100%; height: 130px;\"></ion-skeleton-text>\r\n    <ion-skeleton-text animated style=\"width: 80%; height: 15px;\"></ion-skeleton-text>\r\n    <ion-skeleton-text animated style=\"width: 100%; height: 15px;\">\r\n    </ion-skeleton-text>\r\n  </div>\r\n\r\n  <div *ngIf=\"p!=1\">\r\n    <div *ngIf=\"type=='normal' || type=='recent' || type=='wishList' || type=='flash'\">\r\n      <ion-card class=\"default animated fadeIn\" *ngIf=\"config.productCardStyle==1\">\r\n        <div>\r\n          <div *ngIf=\"p.discount_price!=null\">{{pDiscount()}} {{'OFF'|translate}}</div>\r\n        </div>\r\n        <!-- <ion-img id=\"newimage\" src=\"assets/badge_new.svg\" *ngIf=\"checkProductNew()\"></ion-img> -->\r\n        <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n        <ion-row class=\"ion-no-padding\">\r\n          <ion-col size=\"10\" class=\"ion-no-padding ion-text-start\">\r\n            <div [innerHTML]=\"price_html\" class=\"price\"></div>\r\n          </ion-col>\r\n          <ion-col size=\"2\" class=\"ion-text-center ion-no-padding\">\r\n            <ion-icon (click)=\"clickWishList()\" (click)=\"clickWishList()\" [name]=\"getHeartName()\"></ion-icon>\r\n          </ion-col>\r\n        </ion-row>\r\n\r\n        <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n          <ion-button style=\"z-index: 10;\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n            {{'REMOVE'|translate}}</ion-button>\r\n        </ion-row>\r\n\r\n        <ion-row class=\"ion-no-padding\" *ngIf=\"type=='normal'\">\r\n          <ion-button [color]=\"getButtonColor()\" (click)=\"buttonClick()\">{{getButtonText()|translate}}\r\n          </ion-button>\r\n        </ion-row>\r\n\r\n        <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n          <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n            <app-timer [data]=\"p\"></app-timer>\r\n          </ion-button>\r\n          <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n          </ion-button>\r\n        </ion-row>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n\r\n\r\n      <!---------------------------------------------------------  style 1-------------------------------->\r\n      <ion-card class=\"style21 ion-text-center\" *ngIf=\"config.productCardStyle==21\">\r\n\r\n        <div class=\"main-img\"><img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\"></div>\r\n        <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n\r\n      <!---------------------------------------------------------  style 2 -------------------------------->\r\n      <ion-card class=\"style11 ion-text-center\" *ngIf=\"config.productCardStyle==11\">\r\n        <div class=\"main-img\"><img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\"></div>\r\n        <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n\r\n        <ion-icon (click)=\"clickWishList()\" color=\"secondary\" class=\"heart-top-right\" [name]=\"getHeartName()\">\r\n        </ion-icon>\r\n      </ion-card>\r\n\r\n      <!---------------------------------------------------------  style  -------------------------------->\r\n      <ion-card class=\"default\" *ngIf=\"config.productCardStyle==2\">\r\n        <div>\r\n          <div *ngIf=\"p.on_sale==true\">{{ 'Sale' | translate }}</div>\r\n          <div *ngIf=\"p.featured\">{{'Featured' | translate }}</div>\r\n        </div>\r\n        <!-- <ion-img id=\"newimage\" src=\"assets/badge_new.svg\" *ngIf=\"checkProductNew()\"></ion-img> -->\r\n        <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col size=\"10\" class=\"ion-no-padding\">\r\n              <h4 [innerHTML]=\"price_html\" class=\"price\"></h4>\r\n            </ion-col>\r\n            <ion-col size=\"2\" class=\"ion-text-center ion-no-padding\">\r\n              <ion-icon (click)=\"clickWishList()\" [name]=\"getHeartName()\"></ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button style=\"z-index: 10;\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style3 ion-text-start\" *ngIf=\"config.productCardStyle==3\">\r\n\r\n        <div class=\"main-image\">\r\n          <ion-badge color=\"primary\" class=\"ion-text-start\" mode=ios\r\n            *ngIf=\"p.discount_price!=null || p.flash_price!=null\">{{pDiscount()}}\r\n            <span>{{'OFF'|translate}}</span>\r\n          </ion-badge>\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n              <p class=\"category\">({{getCategoryName()}})</p>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col size=\"10\">\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n            <ion-col size=\"2\">\r\n              <ion-icon (click)=\"buttonClick()\" class=\"ion-float-end\" [color]=\"getButtonColor()\" name=\"basket\">\r\n              </ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n\r\n\r\n      </ion-card>\r\n\r\n\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style6 ion-text-start\" *ngIf=\"config.productCardStyle==6\">\r\n\r\n        <div class=\"main-image\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n          <ion-fab-button size=small color=\"danger\">\r\n            <ion-icon (click)=\"buttonClick()\" name=\"basket\">\r\n            </ion-icon>\r\n          </ion-fab-button>\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span class=\"product-ratings\">\r\n                <div class=\"stars-outer\">\r\n                  <div class=\"stars-inner\" [style.width]=\"ratingPercentage()\"></div>\r\n                </div>\r\n              </span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n\r\n\r\n      </ion-card>\r\n\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style8 ion-text-start\" *ngIf=\"config.productCardStyle==8\">\r\n        <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span class=\"product-ratings\">\r\n                <div class=\"stars-outer\">\r\n                  <div class=\"stars-inner\" [style.width]=\"ratingPercentage()\"></div>\r\n                </div>\r\n                <div class=\"rating-value\">({{p.total_user_rated}})</div>\r\n              </span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n\r\n        <ion-icon (click)=\"clickWishList()\" color=\"secondary\" class=\"heart-top-right\" [name]=\"getHeartName()\">\r\n        </ion-icon>\r\n      </ion-card>\r\n\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style9 ion-text-start\" *ngIf=\"config.productCardStyle==9\">\r\n\r\n        <div class=\"main-image\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n          <ion-fab-button size=small color=\"danger\">\r\n            <ion-icon (click)=\"clickWishList()\" [name]=\"getHeartName()\">\r\n            </ion-icon>\r\n          </ion-fab-button>\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span class=\"product-ratings\">\r\n                <div class=\"stars-outer\">\r\n                  <div class=\"stars-inner\" [style.width]=\"ratingPercentage()\"></div>\r\n                </div>\r\n              </span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n\r\n\r\n      </ion-card>\r\n\r\n      <!---------------------------------------------------------  style  -------------------------------->\r\n      <ion-card class=\"style12\" *ngIf=\"config.productCardStyle==12\">\r\n        <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name ion-text-start\">{{p.products_name}}</p>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price ion-text-end\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n\r\n        <ion-icon (click)=\"clickWishList()\" color=\"secondary\" class=\"heart-top-right\" [name]=\"getHeartName()\">\r\n        </ion-icon>\r\n      </ion-card>\r\n\r\n\r\n      <!---------------------------------------------------------  style  -------------------------------->\r\n\r\n      <ion-card class=\"style5 ion-text-center\" *ngIf=\"config.productCardStyle==5\">\r\n        <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col size=\"2\">\r\n              <ion-icon (click)=\"clickWishList()\" color=\"secondary\" [name]=\"getHeartName()\">\r\n              </ion-icon>\r\n            </ion-col>\r\n            <ion-col size=\"8\">\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n            <ion-col size=\"2\">\r\n              <ion-icon (click)=\"buttonClick()\" [color]=\"getButtonColor()\" name=\"basket\">\r\n              </ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n\r\n\r\n      </ion-card>\r\n\r\n      <!---------------------------------------------------------  style  -------------------------------->\r\n      <ion-card class=\"style7\" *ngIf=\"config.productCardStyle==7\">\r\n        <div class=\"floating-tags\">\r\n          <span class=\"ion-no-padding ion-text-left\" *ngIf=\"p.on_sale\">\r\n            <ion-badge color=\"secondary\" class=\"first\">\r\n              {{ 'Sale' | translate }}</ion-badge>\r\n          </span>\r\n          <span class=\"ion-no-padding ion-text-left\" *ngIf=\"p.featured\">\r\n            <ion-badge color=\"danger\" class=\"second\">\r\n              {{'Featured' | translate }}</ion-badge>\r\n          </span>\r\n        </div>\r\n        <!-- <ion-img id=\"newimage\" src=\"assets/badge_new.svg\" *ngIf=\"checkProductNew()\"></ion-img> -->\r\n        <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col size=\"10\" class=\"ion-no-padding\">\r\n              <h4 [innerHTML]=\"price_html\" class=\"price\"></h4>\r\n            </ion-col>\r\n            <ion-col size=\"2\" class=\"ion-text-center ion-no-padding\">\r\n              <ion-icon (click)=\"clickWishList()\" [name]=\"getHeartName()\"></ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button style=\"z-index: 10;\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style13 ion-text-center\" *ngIf=\"config.productCardStyle==13\">\r\n\r\n        <ion-badge color=\"light\" class=\"heart-top-right price\">\r\n          <span [innerHTML]=\"price_html\" class=\"innerprice\"></span>\r\n        </ion-badge>\r\n\r\n        <div class=\"main-image\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n          <ion-grid class=\"floating-tags\">\r\n            <ion-row class=\"ion-no-padding\" *ngIf=\"p.on_sale==true\">\r\n              <ion-badge color=\"secondary\">\r\n                {{ 'Sale' | translate }}</ion-badge>\r\n            </ion-row>\r\n            <ion-row class=\"ion-no-padding\" *ngIf=\"p.featured\">\r\n              <ion-badge color=\"danger\">\r\n                {{'Featured' | translate }}</ion-badge>\r\n            </ion-row>\r\n          </ion-grid>\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding paddingb5\">\r\n            <ion-col>\r\n              <span class=\"product-ratings\">\r\n                <div class=\"stars-outer\">\r\n                  <div class=\"stars-inner\" [style.width]=\"ratingPercentage()\"></div>\r\n                </div>\r\n              </span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n\r\n\r\n      </ion-card>\r\n\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style15 ion-text-center\" *ngIf=\"config.productCardStyle==15\">\r\n\r\n        <ion-row class=\"ion-no-padding\">\r\n          <ion-col>\r\n            <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n          </ion-col>\r\n        </ion-row>\r\n        <div class=\"main-image\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span class=\"product-ratings\">\r\n                <div class=\"stars-outer\">\r\n                  <div class=\"stars-inner\" [style.width]=\"ratingPercentage()\"></div>\r\n                </div>\r\n              </span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n\r\n\r\n      </ion-card>\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style16 ion-text-start\" *ngIf=\"config.productCardStyle==16\">\r\n\r\n        <div class=\"main-image\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n\r\n\r\n          <ion-grid class=\"ion-no-padding\">\r\n            <ion-row class=\"ion-no-padding\">\r\n              <ion-col>\r\n                <span [innerHTML]=\"price_html\" class=\"price ellipsis\"></span>\r\n              </ion-col>\r\n            </ion-row>\r\n\r\n            <ion-row class=\"ion-no-padding\">\r\n              <ion-col>\r\n                <span class=\"product-ratings ion-float-left\">\r\n                  <div class=\"stars-outer\">\r\n                    <div class=\"stars-inner\" [style.width]=\"ratingPercentage()\"></div>\r\n                  </div>\r\n                </span>\r\n\r\n                <ion-icon (click)=\"clickWishList()\" color=\"secondary\" class=\"ion-float-right\" [name]=\"getHeartName()\">\r\n                </ion-icon>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n              <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n                {{'REMOVE'|translate}}</ion-button>\r\n            </ion-row>\r\n            <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n              <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n                <app-timer [data]=\"p\"></app-timer>\r\n              </ion-button>\r\n              <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n              </ion-button>\r\n            </ion-row>\r\n          </ion-grid>\r\n        </div>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n\r\n      <!---------------------------------------------------------  style default -------------------------------->\r\n      <ion-card class=\"default style17\" *ngIf=\"config.productCardStyle==17\">\r\n\r\n        <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        <ion-icon (click)=\"clickWishList()\" color=\"secondary\" class=\"heart-top-right\" [name]=\"getHeartName()\">\r\n        </ion-icon>\r\n        <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col size=9 class=\"ion-no-padding\">\r\n              <div class=\"price\" [innerHTML]=\"price_html\"></div>\r\n            </ion-col>\r\n            <ion-col size=3 class=\"ion-text-center ion-no-padding\">\r\n              <ion-text class=\"price\">\r\n                <ion-icon name=\"star\"></ion-icon>{{p.total_user_rated}}\r\n              </ion-text>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button style=\"z-index: 10;\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='normal'\">\r\n            <ion-button [color]=\"getButtonColor()\" (click)=\"buttonClick()\">{{getButtonText()|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n\r\n\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style3 style18 ion-text-start\" *ngIf=\"config.productCardStyle==18\">\r\n\r\n        <div class=\"main-image\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n              <p class=\"category ellipsis\">({{getCategoryName()}})</p>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type!='flash'\">\r\n            <ion-col size=7>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n            <ion-col size=5 class=\"cart\">\r\n              <ion-icon name=\"remove\" (click)=\"removingToCart()\"></ion-icon>\r\n              <ion-text>\r\n                <p>{{cartQuantity}}</p>\r\n              </ion-text>\r\n              <ion-icon name=\"add\" (click)=\"addingToCart()\"></ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-card>\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style9 style19 ion-text-start\" *ngIf=\"config.productCardStyle==19\">\r\n\r\n        <div class=\"main-image\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n          <ion-icon (click)=\"clickWishList()\" color=\"secondary\" class=\"heart-top-right\" [name]=\"getHeartName()\">\r\n          </ion-icon>\r\n          <ion-grid class=\"floating-tags\">\r\n            <ion-row class=\"ion-no-padding\" *ngIf=\"p.on_sale==true\">\r\n              <ion-badge>\r\n                {{ 'Sale' | translate }}</ion-badge>\r\n            </ion-row>\r\n            <ion-row class=\"ion-no-padding\" *ngIf=\"p.featured\">\r\n              <ion-badge>\r\n                {{'Featured' | translate }}</ion-badge>\r\n            </ion-row>\r\n          </ion-grid>\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n              <span class=\"product-ratings\">\r\n                <div class=\"stars-outer\">\r\n                  <div class=\"stars-inner\" [style.width]=\"ratingPercentage()\"></div>\r\n                </div>\r\n              </span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n      <!---------------------------------------------------------  style  -------------------------------->\r\n\r\n      <ion-card class=\"style4 ion-text-center\" *ngIf=\"config.productCardStyle==4\">\r\n\r\n        <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col size=\"6\">\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n            <ion-col size=\"6\">\r\n              <ion-badge [color]=\"getButtonColor()\" (click)=\"buttonClick()\" class=\"ellipsis\">\r\n                <span>{{getButtonText()|translate}}</span>\r\n              </ion-badge>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n\r\n      <!---------------------------------------------------------  style -------------------------------->\r\n      <ion-card class=\"style20 ion-text-center\" *ngIf=\"config.productCardStyle==20\">\r\n        <div class=\"main-img\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n\r\n          <div class=\"buttons-img\">\r\n            <ion-button size=\"small\" [color]=\"getButtonColor()\" (click)=\"buttonClick()\">\r\n              {{getButtonText()|translate}}\r\n              <ion-icon slot=\"end\" name=\"basket\"></ion-icon>\r\n            </ion-button>\r\n            <ion-button size=\"small\" color=\"secondary\" (click)=\"clickWishList()\">\r\n              <ion-icon slot=\"icon-only\" [name]=\"getHeartName()\"></ion-icon>\r\n            </ion-button>\r\n          </div>\r\n        </div>\r\n\r\n        <p class=\"category\">{{getCategoryName()}}</p>\r\n        <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style3 style18 ion-text-start\" *ngIf=\"config.productCardStyle==22\">\r\n\r\n        <div class=\"main-image\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n              <p class=\"category\">({{getCategoryName()}})</p>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type!='flash'\">\r\n            <ion-col size=7>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n            <ion-col size=5 class=\"cart\">\r\n              <ion-icon name=\"remove-circle\" (click)=\"removingToCart()\"></ion-icon>\r\n              <ion-text>\r\n                <p>{{cartQuantity}}</p>\r\n              </ion-text>\r\n              <ion-icon name=\"add-circle\" (click)=\"addingToCart()\"></ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-card>\r\n\r\n\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\" style23 ion-text-center\" *ngIf=\"config.productCardStyle==23\">\r\n\r\n        <div class=\"main-image\">\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type!='flash'\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding justify-content-center\" *ngIf=\"type!='flash'\">\r\n            <ion-col class=\"cart\" size=auto>\r\n              <ion-icon name=\"remove-circle\" (click)=\"removingToCart()\"></ion-icon>\r\n              <ion-text>\r\n                <p>{{cartQuantity}}</p>\r\n              </ion-text>\r\n              <ion-icon name=\"add-circle\" (click)=\"addingToCart()\"></ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-col>\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-card>\r\n      <!--------------------------------------------------------- style   -------------------------------->\r\n\r\n      <ion-card class=\"style10 ion-text-center\" *ngIf=\"config.productCardStyle==10\">\r\n\r\n        <div class=\"main-image\">\r\n          <ion-badge color=\"light\" mode=ios [innerHTML]=\"price_html\" class=\"price\">\r\n          </ion-badge>\r\n          <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        </div>\r\n\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col>\r\n              <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n              <p class=\"category\">({{getCategoryName()}})</p>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button class=\"bottom-big-button\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n\r\n\r\n      <!---------------------------------------------------------  style  -------------------------------->\r\n      <ion-card class=\"default style14 ion-text-center\" *ngIf=\"config.productCardStyle==14\">\r\n        <img id=\"image\" [src]=\"getProductImage()\" (click)=\"showProductDetail()\">\r\n        <ion-icon (click)=\"clickWishList()\" color=\"secondary\" class=\"heart-top-right\" [name]=\"getHeartName()\">\r\n        </ion-icon>\r\n        <p (click)=\"showProductDetail()\" class=\"name\">{{p.products_name}}</p>\r\n        <ion-grid class=\"ion-no-padding\">\r\n          <ion-row class=\"ion-no-padding\">\r\n            <ion-col size=\"12\" class=\"ion-no-padding\">\r\n              <span [innerHTML]=\"price_html\" class=\"price\"></span>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='recent' || type=='wishList'\">\r\n            <ion-button style=\"z-index: 10;\" color=\"danger\" (click)=\"removeProduct(type)\">\r\n              {{'REMOVE'|translate}}</ion-button>\r\n          </ion-row>\r\n          <ion-row class=\"ion-no-padding\" *ngIf=\"type=='flash'\">\r\n            <ion-button color=\"secondary\" *ngIf=\"!is_upcomming\" class=\"timer-button\">\r\n              <app-timer [data]=\"p\"></app-timer>\r\n            </ion-button>\r\n            <ion-button color=\"secondary\" *ngIf=\"is_upcomming\" (click)=\"showProductDetail()\">{{'Up Coming'|translate}}\r\n            </ion-button>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n        </ion-row>\r\n      </ion-card>\r\n    </div>\r\n    <!-- //====================================  list view for shop page ==================================== -->\r\n    <ion-item lines=\"none\" *ngIf=\"type=='list'\" class=\"animate-item\">\r\n      <ion-thumbnail slot=\"start\">\r\n        <!-- <ion-img class=\"badge-img\" src=\"assets/badge_new.svg\" *ngIf=\"checkProductNew()\"></ion-img> -->\r\n        <img src=\"{{config.imgUrl+p.products_image}}\" (click)=\"showProductDetail()\">\r\n      </ion-thumbnail>\r\n      <ion-label>\r\n        <p (click)=\"showProductDetail()\">{{p.products_name}}</p>\r\n        <p class=\"list-price-normal\" *ngIf=\"p.discount_price==null\">{{p.products_price|curency}}</p>\r\n        <p class=\"list-price-normal\" *ngIf=\"p.discount_price!=null\"><span class=\"list-price-normal-through\"\r\n            *ngIf=\"p.discount_price!=null\">{{p.products_price|curency}}</span>{{p.discount_price|curency}}</p>\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-button color=\"secondary\" *ngIf=\"config.cartButton==1 && p.defaultStock>0 && p.products_type==0\"\r\n              (click)=\"addToCart(p)\" item-start>{{'ADD TO CART'|translate}}</ion-button>\r\n            <ion-button color=\"secondary\" (click)=\"showProductDetail()\"\r\n              *ngIf=\"config.cartButton==1 && p.products_type!=0\" item-start>\r\n              {{'DETAILS'|translate}}</ion-button>\r\n            <ion-button color=\"danger\" *ngIf=\"config.cartButton==1 && p.defaultStock<=0 && p.products_type==0\"\r\n              item-start>\r\n              {{'OUT OF STOCK'|translate}}</ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-icon id=\"list-heart-icon\" *ngIf=\"p.isLiked=='0'\" (click)=\"clickWishList()\" name=\"heart-outline\"></ion-icon>\r\n        <ion-icon id=\"list-heart-icon\" *ngIf=\"p.isLiked!='0'\" (click)=\"clickWishList()\" name=\"heart\"></ion-icon>\r\n      </ion-label>\r\n\r\n      <div class=\"img-div\">\r\n        <div *ngIf=\"p.discount_price!=null\" class=\"sale\">{{pDiscount()}}<br>{{'OFF'|translate}}</div>\r\n      </div>\r\n      <ion-row class=\"card-add-cart\" *ngIf=\"isInCart()\" (click)=\"showProductDetail()\">\r\n        <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n      </ion-row>\r\n    </ion-item>\r\n  </div>\r\n\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/components/scrolling-featured-products/scrolling-featured-products.component.html":
  /*!*************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/scrolling-featured-products/scrolling-featured-products.component.html ***!
    \*************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcComponentsScrollingFeaturedProductsScrollingFeaturedProductsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "    <ion-grid class=\"ion-no-padding\">\r\n      <ion-row class=\"ion-no-padding\">\r\n        <ion-col *ngFor=\"let p of products\" size=\"6\" class=\"ion-no-padding\">\r\n          <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  <ion-infinite-scroll #infinite (ionInfinite)=\"getProducts()\">\r\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\r\n    </ion-infinite-scroll>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/components/sliding-tabs/sliding-tabs.component.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/sliding-tabs/sliding-tabs.component.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcComponentsSlidingTabsSlidingTabsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- Home-1 For Segments With Not Image -->\r\n\r\n<div class=\"sliding-tabs\">\r\n  <ion-slides *ngIf=\"type!='image'\" [options]=\"sliderConfig\">\r\n    <ion-slide class=\"first-slide\" [class.selected]=\"selected==0\" *ngIf=\"shared.allCategories!=null\"\r\n      (click)=\"changeTab('0')\">\r\n      {{'All'|translate}}</ion-slide>\r\n    <ion-slide [class.selected]=\"selected==c.id\" *ngFor=\"let c of shared.allCategories\" (click)=\"changeTab(c)\">\r\n      {{c.name}}\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n\r\n  <ion-slides *ngIf=\"type=='image'\" [options]=\"sliderConfig\">\r\n    <ion-slide class=\"first-slide\" [class.selected]=\"selected==0\" *ngIf=\"shared.allCategories!=null\"\r\n      (click)=\"changeTab('0')\">\r\n      <img src=\"assets/home-page/category.png\">\r\n      {{'All'|translate}}</ion-slide>\r\n    <ion-slide [class.selected]=\"selected==c.id\" *ngFor=\"let c of shared.allCategories\" (click)=\"changeTab(c)\">\r\n      <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.icon}}\">\r\n      {{c.name}}\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n\r\n  <!-- Home-2 For Segments With Image -->\r\n  <!-- <ion-segment *ngIf=\"type=='image'\" [(ngModel)]=\"selected\" scrollable>\r\n    <ion-segment-button value=\"0\" (click)=\"changeTab('0')\">\r\n      <img src=\"assets/home-page/category.png\">\r\n      {{'All'|translate}}</ion-segment-button>\r\n    <ion-segment-button value=\"{{c.id}}\" *ngFor=\"let c of shared.allCategories\" (click)=\"changeTab(c)\">\r\n      <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.icon}}\">\r\n      {{c.name}} </ion-segment-button>\r\n  </ion-segment> -->\r\n\r\n  <!-- scrollable segment divisions -->\r\n  <div [ngSwitch]=\"selected\">\r\n  </div>\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col *ngFor=\"let p of products\" size=\"6\" class=\"ion-no-padding\">\r\n        <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n  <!-- infinite scroll -->\r\n  <ion-infinite-scroll threshold=\"10px\" #infinite (ionInfinite)=\"getProducts($event)\">\r\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\r\n  </ion-infinite-scroll>\r\n\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/components/timer/timer.component.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/timer/timer.component.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcComponentsTimerTimerComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<span *ngIf=\"timer\">\r\n  <div *ngIf=\"timeInSeconds && timeInSeconds > 0\">{{timer.displayTime}}</div>\r\n  <div *ngIf=\"!timeInSeconds || timeInSeconds == 0\">Timer set up incorrectly</div>\r\n</span>";
    /***/
  },

  /***/
  "./src/components/banner/banner.component.scss":
  /*!*****************************************************!*\
    !*** ./src/components/banner/banner.component.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcComponentsBannerBannerComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".banner-component ion-slides {\n  --bullet-background: var(--ion-color-primary);\n  --bullet-background-active: var(--ion-color-secondary);\n  --progress-bar-background: var(--ion-color-secondary-contrast);\n  --progress-bar-background-active: var(--ion-color-secondary);\n  min-height: 180;\n}\n.banner-component ion-slides img {\n  width: 100%;\n}\n.banner-component .squareBullets .swiper-pagination-bullet {\n  width: 12px;\n  border-radius: 3px;\n}\n.banner-component .bottomBulletsWhiteBackground .swiper-pagination-bullet {\n  padding: 6px;\n}\n.banner-component .bottomBulletsWhiteBackground .swiper-pagination-bullets {\n  position: unset;\n  margin-top: 10px;\n  margin-left: 50%;\n  margin-right: auto;\n}\n.banner-component .progressBar .swiper-pagination-progressbar {\n  top: auto;\n  bottom: 0;\n}\n.banner-component .verticalRightBullets .swiper-pagination-bullet {\n  display: block;\n  margin: 5px;\n  border-radius: 3px;\n  padding: 8px 0;\n}\n.banner-component .verticalRightBullets .swiper-pagination-bullets {\n  left: auto;\n  right: 5px;\n  width: auto;\n  bottom: auto;\n  margin: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n.banner-component .verticalLeftBullets .swiper-pagination-bullet {\n  display: block;\n  margin: 5px;\n  border-radius: 3px;\n  padding: 8px 0;\n}\n.banner-component .verticalLeftBullets .swiper-pagination-bullets {\n  left: 5px;\n  width: auto;\n  bottom: auto;\n  margin: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n.banner-component .numberBullets .swiper-pagination-bullet {\n  width: 20px;\n  height: 20px;\n  text-align: center;\n  line-height: 20px;\n  font-size: 12px;\n  opacity: 1;\n  color: var(--ion-color-medium-contrast);\n  background: var(--ion-color-medium);\n}\n.banner-component .numberBullets .swiper-pagination-bullet-active {\n  color: var(--ion-color-secondary-contrast);\n  background: var(--ion-color-secondary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2Jhbm5lci9EOlxcRG9jdW1lbnRvc1xcUHJvZ3JhbWFjaW9uXFxKYXZhc2NyaXB0XFxJb25pY1xcZGVsaXZlcnljdXN0b21lci9zcmNcXGNvbXBvbmVudHNcXGJhbm5lclxcYmFubmVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2NvbXBvbmVudHMvYmFubmVyL2Jhbm5lci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLDZDQUFBO0VBQ0Esc0RBQUE7RUFDQSw4REFBQTtFQUNBLDREQUFBO0VBQ0EsZUFBQTtBQ0FKO0FER0k7RUFDRSxXQUFBO0FDRE47QURNSTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtBQ0pOO0FEUUk7RUFDRSxZQUFBO0FDTk47QURRSTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNOTjtBRFVJO0VBQ0UsU0FBQTtFQUNBLFNBQUE7QUNSTjtBRGFJO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUNYTjtBRGFJO0VBQ0UsVUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBRUEsMkJBQUE7QUNYTjtBRGdCSTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FDZE47QURnQkk7RUFDRSxTQUFBO0VBRUEsV0FBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUVBLDJCQUFBO0FDZk47QURvQkk7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLHVDQUFBO0VBQ0EsbUNBQUE7QUNsQk47QURvQkk7RUFDRSwwQ0FBQTtFQUNBLHNDQUFBO0FDbEJOIiwiZmlsZSI6InNyYy9jb21wb25lbnRzL2Jhbm5lci9iYW5uZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmFubmVyLWNvbXBvbmVudCB7XHJcbiAgaW9uLXNsaWRlcyB7XHJcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAtLXByb2dyZXNzLWJhci1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5LWNvbnRyYXN0KTtcclxuICAgIC0tcHJvZ3Jlc3MtYmFyLWJhY2tncm91bmQtYWN0aXZlOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcclxuICAgIG1pbi1oZWlnaHQ6IDE4MDtcclxuICAgIC8vYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG5cclxuICAgIGltZ3tcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuc3F1YXJlQnVsbGV0cyB7XHJcbiAgICAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcclxuICAgICAgd2lkdGg6IDEycHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmJvdHRvbUJ1bGxldHNXaGl0ZUJhY2tncm91bmQge1xyXG4gICAgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XHJcbiAgICAgIHBhZGRpbmc6IDZweDtcclxuICAgIH1cclxuICAgIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzIHtcclxuICAgICAgcG9zaXRpb246IHVuc2V0O1xyXG4gICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICBtYXJnaW4tbGVmdDogNTAlO1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5wcm9ncmVzc0JhciB7XHJcbiAgICAuc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIge1xyXG4gICAgICB0b3A6IGF1dG87XHJcbiAgICAgIGJvdHRvbTogMDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC52ZXJ0aWNhbFJpZ2h0QnVsbGV0cyB7XHJcbiAgICAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIG1hcmdpbjogNXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgICAgIHBhZGRpbmc6IDhweCAwO1xyXG4gICAgfVxyXG4gICAgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMge1xyXG4gICAgICBsZWZ0OiBhdXRvO1xyXG4gICAgICByaWdodDogNXB4O1xyXG4gICAgICB3aWR0aDogYXV0bztcclxuICAgICAgYm90dG9tOiBhdXRvO1xyXG4gICAgICBtYXJnaW46IDA7XHJcbiAgICAgIHRvcDogNTAlO1xyXG4gICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xyXG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAudmVydGljYWxMZWZ0QnVsbGV0cyB7XHJcbiAgICAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIG1hcmdpbjogNXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgICAgIHBhZGRpbmc6IDhweCAwO1xyXG4gICAgfVxyXG4gICAgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMge1xyXG4gICAgICBsZWZ0OiA1cHg7XHJcbiAgICAgIC8vcmlnaHQ6IDVweDtcclxuICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgIGJvdHRvbTogYXV0bztcclxuICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICB0b3A6IDUwJTtcclxuICAgICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcclxuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLm51bWJlckJ1bGxldHMge1xyXG4gICAgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XHJcbiAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tY29udHJhc3QpO1xyXG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuICAgIH1cclxuICAgIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQtYWN0aXZlIHtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3QpO1xyXG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiLmJhbm5lci1jb21wb25lbnQgaW9uLXNsaWRlcyB7XG4gIC0tYnVsbGV0LWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmU6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAtLXByb2dyZXNzLWJhci1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5LWNvbnRyYXN0KTtcbiAgLS1wcm9ncmVzcy1iYXItYmFja2dyb3VuZC1hY3RpdmU6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICBtaW4taGVpZ2h0OiAxODA7XG59XG4uYmFubmVyLWNvbXBvbmVudCBpb24tc2xpZGVzIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmJhbm5lci1jb21wb25lbnQgLnNxdWFyZUJ1bGxldHMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XG4gIHdpZHRoOiAxMnB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG4uYmFubmVyLWNvbXBvbmVudCAuYm90dG9tQnVsbGV0c1doaXRlQmFja2dyb3VuZCAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcbiAgcGFkZGluZzogNnB4O1xufVxuLmJhbm5lci1jb21wb25lbnQgLmJvdHRvbUJ1bGxldHNXaGl0ZUJhY2tncm91bmQgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMge1xuICBwb3NpdGlvbjogdW5zZXQ7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1sZWZ0OiA1MCU7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbn1cbi5iYW5uZXItY29tcG9uZW50IC5wcm9ncmVzc0JhciAuc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIge1xuICB0b3A6IGF1dG87XG4gIGJvdHRvbTogMDtcbn1cbi5iYW5uZXItY29tcG9uZW50IC52ZXJ0aWNhbFJpZ2h0QnVsbGV0cyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogNXB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIHBhZGRpbmc6IDhweCAwO1xufVxuLmJhbm5lci1jb21wb25lbnQgLnZlcnRpY2FsUmlnaHRCdWxsZXRzIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzIHtcbiAgbGVmdDogYXV0bztcbiAgcmlnaHQ6IDVweDtcbiAgd2lkdGg6IGF1dG87XG4gIGJvdHRvbTogYXV0bztcbiAgbWFyZ2luOiAwO1xuICB0b3A6IDUwJTtcbiAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuLmJhbm5lci1jb21wb25lbnQgLnZlcnRpY2FsTGVmdEJ1bGxldHMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDVweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBwYWRkaW5nOiA4cHggMDtcbn1cbi5iYW5uZXItY29tcG9uZW50IC52ZXJ0aWNhbExlZnRCdWxsZXRzIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzIHtcbiAgbGVmdDogNXB4O1xuICB3aWR0aDogYXV0bztcbiAgYm90dG9tOiBhdXRvO1xuICBtYXJnaW46IDA7XG4gIHRvcDogNTAlO1xuICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG4uYmFubmVyLWNvbXBvbmVudCAubnVtYmVyQnVsbGV0cyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMjBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBvcGFjaXR5OiAxO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1jb250cmFzdCk7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xufVxuLmJhbm5lci1jb21wb25lbnQgLm51bWJlckJ1bGxldHMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldC1hY3RpdmUge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeS1jb250cmFzdCk7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/components/banner/banner.component.ts":
  /*!***************************************************!*\
    !*** ./src/components/banner/banner.component.ts ***!
    \***************************************************/

  /*! exports provided: BannerComponent */

  /***/
  function srcComponentsBannerBannerComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BannerComponent", function () {
      return BannerComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/http/ngx */
    "./node_modules/@ionic-native/http/ngx/index.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");

    var BannerComponent = /*#__PURE__*/function () {
      function BannerComponent(shared, navCtrl, config, http, loading) {
        _classCallCheck(this, BannerComponent);

        this.shared = shared;
        this.navCtrl = navCtrl;
        this.config = config;
        this.http = http;
        this.loading = loading;
        this.slideOpts = {
          autoplay: {
            delay: 2500
          }
        }; //===============================================================================================
        //on click image banners

        this.bannerClick = function (image) {
          if (image.type == 'category') {
            this.navCtrl.navigateForward("tabs/" + this.config.getCurrentHomePage() + "/products/" + image.url + "/0/newest");
          } else if (image.type == 'product') {
            this.getSingleProductDetail(parseInt(image.url));
          } else {
            this.navCtrl.navigateForward(this.config.currentRoute + "/products/0/0/" + image.type);
          }
        };
      } //===============================================================================================
      //getting single product data


      _createClass(BannerComponent, [{
        key: "getSingleProductDetail",
        value: function getSingleProductDetail(id) {
          var _this2 = this;

          this.loading.show();
          var dat = {};
          if (this.shared.customerData != null) dat.customers_id = this.shared.customerData.customers_id;else dat.customers_id = null;
          dat.products_id = id;
          dat.language_id = this.config.langId;
          dat.currency_code = this.config.currecnyCode;
          this.config.postHttp('getallproducts', dat).then(function (data) {
            _this2.loading.hide();

            if (data.success == 1) {
              var product = data.product_data[0];

              _this2.shared.singleProductPageData.push(product);

              _this2.navCtrl.navigateForward(_this2.config.currentRoute + "/product-detail/" + product.products_id);
            }
          });
        }
      }, {
        key: "applyFlipEffect",
        value: function applyFlipEffect() {
          this.slideOpts = {
            autoplay: {
              delay: 2500,
              disableOnInteraction: false
            },
            on: {
              beforeInit: function beforeInit() {
                var swiper = this;
                swiper.classNames.push("".concat(swiper.params.containerModifierClass, "flip"));
                swiper.classNames.push("".concat(swiper.params.containerModifierClass, "3d"));
                var overwriteParams = {
                  slidesPerView: 1,
                  slidesPerColumn: 1,
                  slidesPerGroup: 1,
                  watchSlidesProgress: true,
                  spaceBetween: 0,
                  virtualTranslate: true
                };
                swiper.params = Object.assign(swiper.params, overwriteParams);
                swiper.originalParams = Object.assign(swiper.originalParams, overwriteParams);
              },
              setTranslate: function setTranslate() {
                var swiper = this;
                var $ = swiper.$,
                    slides = swiper.slides,
                    rtl = swiper.rtlTranslate;

                for (var i = 0; i < slides.length; i += 1) {
                  var $slideEl = slides.eq(i);
                  var progress = $slideEl[0].progress;

                  if (swiper.params.flipEffect.limitRotation) {
                    progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
                  }

                  var offset$$1 = $slideEl[0].swiperSlideOffset;
                  var rotate = -180 * progress;
                  var rotateY = rotate;
                  var rotateX = 0;
                  var tx = -offset$$1;
                  var ty = 0;

                  if (!swiper.isHorizontal()) {
                    ty = tx;
                    tx = 0;
                    rotateX = -rotateY;
                    rotateY = 0;
                  } else if (rtl) {
                    rotateY = -rotateY;
                  }

                  $slideEl[0].style.zIndex = -Math.abs(Math.round(progress)) + slides.length;

                  if (swiper.params.flipEffect.slideShadows) {
                    // Set shadows
                    var shadowBefore = swiper.isHorizontal() ? $slideEl.find('.swiper-slide-shadow-left') : $slideEl.find('.swiper-slide-shadow-top');
                    var shadowAfter = swiper.isHorizontal() ? $slideEl.find('.swiper-slide-shadow-right') : $slideEl.find('.swiper-slide-shadow-bottom');

                    if (shadowBefore.length === 0) {
                      shadowBefore = swiper.$("<div class=\"swiper-slide-shadow-".concat(swiper.isHorizontal() ? 'left' : 'top', "\"></div>"));
                      $slideEl.append(shadowBefore);
                    }

                    if (shadowAfter.length === 0) {
                      shadowAfter = swiper.$("<div class=\"swiper-slide-shadow-".concat(swiper.isHorizontal() ? 'right' : 'bottom', "\"></div>"));
                      $slideEl.append(shadowAfter);
                    }

                    if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
                    if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
                  }

                  $slideEl.transform("translate3d(".concat(tx, "px, ").concat(ty, "px, 0px) rotateX(").concat(rotateX, "deg) rotateY(").concat(rotateY, "deg)"));
                }
              },
              setTransition: function setTransition(duration) {
                var swiper = this;
                var slides = swiper.slides,
                    activeIndex = swiper.activeIndex,
                    $wrapperEl = swiper.$wrapperEl;
                slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);

                if (swiper.params.virtualTranslate && duration !== 0) {
                  var eventTriggered = false; // eslint-disable-next-line

                  slides.eq(activeIndex).transitionEnd(function onTransitionEnd() {
                    if (eventTriggered) return;
                    if (!swiper || swiper.destroyed) return;
                    eventTriggered = true;
                    swiper.animating = false;
                    var triggerEvents = ['webkitTransitionEnd', 'transitionend'];

                    for (var i = 0; i < triggerEvents.length; i += 1) {
                      $wrapperEl.trigger(triggerEvents[i]);
                    }
                  });
                }
              }
            }
          };
        }
      }, {
        key: "applyCubeEffect",
        value: function applyCubeEffect() {
          this.slideOpts = {
            autoplay: {
              delay: 2500,
              disableOnInteraction: false
            },
            cubeEffect: {
              shadow: true,
              slideShadows: true,
              shadowOffset: 20,
              shadowScale: 0.94
            },
            on: {
              beforeInit: function beforeInit() {
                var swiper = this;
                swiper.classNames.push("".concat(swiper.params.containerModifierClass, "cube"));
                swiper.classNames.push("".concat(swiper.params.containerModifierClass, "3d"));
                var overwriteParams = {
                  slidesPerView: 1,
                  slidesPerColumn: 1,
                  slidesPerGroup: 1,
                  watchSlidesProgress: true,
                  resistanceRatio: 0,
                  spaceBetween: 0,
                  centeredSlides: false,
                  virtualTranslate: true
                };
                this.params = Object.assign(this.params, overwriteParams);
                this.originalParams = Object.assign(this.originalParams, overwriteParams);
              },
              setTranslate: function setTranslate() {
                var swiper = this;
                var $el = swiper.$el,
                    $wrapperEl = swiper.$wrapperEl,
                    slides = swiper.slides,
                    swiperWidth = swiper.width,
                    swiperHeight = swiper.height,
                    rtl = swiper.rtlTranslate,
                    swiperSize = swiper.size;
                var params = swiper.params.cubeEffect;
                var isHorizontal = swiper.isHorizontal();
                var isVirtual = swiper.virtual && swiper.params.virtual.enabled;
                var wrapperRotate = 0;
                var $cubeShadowEl;

                if (params.shadow) {
                  if (isHorizontal) {
                    $cubeShadowEl = $wrapperEl.find('.swiper-cube-shadow');

                    if ($cubeShadowEl.length === 0) {
                      $cubeShadowEl = swiper.$('<div class="swiper-cube-shadow"></div>');
                      $wrapperEl.append($cubeShadowEl);
                    }

                    $cubeShadowEl.css({
                      height: "".concat(swiperWidth, "px")
                    });
                  } else {
                    $cubeShadowEl = $el.find('.swiper-cube-shadow');

                    if ($cubeShadowEl.length === 0) {
                      $cubeShadowEl = swiper.$('<div class="swiper-cube-shadow"></div>');
                      $el.append($cubeShadowEl);
                    }
                  }
                }

                for (var i = 0; i < slides.length; i += 1) {
                  var $slideEl = slides.eq(i);
                  var slideIndex = i;

                  if (isVirtual) {
                    slideIndex = parseInt($slideEl.attr('data-swiper-slide-index'), 10);
                  }

                  var slideAngle = slideIndex * 90;
                  var round = Math.floor(slideAngle / 360);

                  if (rtl) {
                    slideAngle = -slideAngle;
                    round = Math.floor(-slideAngle / 360);
                  }

                  var progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
                  var tx = 0;
                  var ty = 0;
                  var tz = 0;

                  if (slideIndex % 4 === 0) {
                    tx = -round * 4 * swiperSize;
                    tz = 0;
                  } else if ((slideIndex - 1) % 4 === 0) {
                    tx = 0;
                    tz = -round * 4 * swiperSize;
                  } else if ((slideIndex - 2) % 4 === 0) {
                    tx = swiperSize + round * 4 * swiperSize;
                    tz = swiperSize;
                  } else if ((slideIndex - 3) % 4 === 0) {
                    tx = -swiperSize;
                    tz = 3 * swiperSize + swiperSize * 4 * round;
                  }

                  if (rtl) {
                    tx = -tx;
                  }

                  if (!isHorizontal) {
                    ty = tx;
                    tx = 0;
                  }

                  var transform$$1 = "rotateX(".concat(isHorizontal ? 0 : -slideAngle, "deg) rotateY(").concat(isHorizontal ? slideAngle : 0, "deg) translate3d(").concat(tx, "px, ").concat(ty, "px, ").concat(tz, "px)");

                  if (progress <= 1 && progress > -1) {
                    wrapperRotate = slideIndex * 90 + progress * 90;
                    if (rtl) wrapperRotate = -slideIndex * 90 - progress * 90;
                  }

                  $slideEl.transform(transform$$1);

                  if (params.slideShadows) {
                    // Set shadows
                    var shadowBefore = isHorizontal ? $slideEl.find('.swiper-slide-shadow-left') : $slideEl.find('.swiper-slide-shadow-top');
                    var shadowAfter = isHorizontal ? $slideEl.find('.swiper-slide-shadow-right') : $slideEl.find('.swiper-slide-shadow-bottom');

                    if (shadowBefore.length === 0) {
                      shadowBefore = swiper.$("<div class=\"swiper-slide-shadow-".concat(isHorizontal ? 'left' : 'top', "\"></div>"));
                      $slideEl.append(shadowBefore);
                    }

                    if (shadowAfter.length === 0) {
                      shadowAfter = swiper.$("<div class=\"swiper-slide-shadow-".concat(isHorizontal ? 'right' : 'bottom', "\"></div>"));
                      $slideEl.append(shadowAfter);
                    }

                    if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
                    if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
                  }
                }

                $wrapperEl.css({
                  '-webkit-transform-origin': "50% 50% -".concat(swiperSize / 2, "px"),
                  '-moz-transform-origin': "50% 50% -".concat(swiperSize / 2, "px"),
                  '-ms-transform-origin': "50% 50% -".concat(swiperSize / 2, "px"),
                  'transform-origin': "50% 50% -".concat(swiperSize / 2, "px")
                });

                if (params.shadow) {
                  if (isHorizontal) {
                    $cubeShadowEl.transform("translate3d(0px, ".concat(swiperWidth / 2 + params.shadowOffset, "px, ").concat(-swiperWidth / 2, "px) rotateX(90deg) rotateZ(0deg) scale(").concat(params.shadowScale, ")"));
                  } else {
                    var shadowAngle = Math.abs(wrapperRotate) - Math.floor(Math.abs(wrapperRotate) / 90) * 90;
                    var multiplier = 1.5 - (Math.sin(shadowAngle * 2 * Math.PI / 360) / 2 + Math.cos(shadowAngle * 2 * Math.PI / 360) / 2);
                    var scale1 = params.shadowScale;
                    var scale2 = params.shadowScale / multiplier;
                    var offset$$1 = params.shadowOffset;
                    $cubeShadowEl.transform("scale3d(".concat(scale1, ", 1, ").concat(scale2, ") translate3d(0px, ").concat(swiperHeight / 2 + offset$$1, "px, ").concat(-swiperHeight / 2 / scale2, "px) rotateX(-90deg)"));
                  }
                }

                var zFactor = swiper.browser.isSafari || swiper.browser.isUiWebView ? -swiperSize / 2 : 0;
                $wrapperEl.transform("translate3d(0px,0,".concat(zFactor, "px) rotateX(").concat(swiper.isHorizontal() ? 0 : wrapperRotate, "deg) rotateY(").concat(swiper.isHorizontal() ? -wrapperRotate : 0, "deg)"));
              },
              setTransition: function setTransition(duration) {
                var swiper = this;
                var $el = swiper.$el,
                    slides = swiper.slides;
                slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);

                if (swiper.params.cubeEffect.shadow && !swiper.isHorizontal()) {
                  $el.find('.swiper-cube-shadow').transition(duration);
                }
              }
            }
          };
        }
      }, {
        key: "applyCoverFlowEffect",
        value: function applyCoverFlowEffect() {
          this.slideOpts = {
            autoplay: {
              delay: 2500,
              disableOnInteraction: false
            },
            slidesPerView: 1,
            coverflowEffect: {
              rotate: 50,
              stretch: 0,
              depth: 100,
              modifier: 1,
              slideShadows: true
            },
            on: {
              beforeInit: function beforeInit() {
                var swiper = this;
                swiper.classNames.push("".concat(swiper.params.containerModifierClass, "coverflow"));
                swiper.classNames.push("".concat(swiper.params.containerModifierClass, "3d"));
                swiper.params.watchSlidesProgress = true;
                swiper.originalParams.watchSlidesProgress = true;
              },
              setTranslate: function setTranslate() {
                var swiper = this;
                var swiperWidth = swiper.width,
                    swiperHeight = swiper.height,
                    slides = swiper.slides,
                    $wrapperEl = swiper.$wrapperEl,
                    slidesSizesGrid = swiper.slidesSizesGrid,
                    $ = swiper.$;
                var params = swiper.params.coverflowEffect;
                var isHorizontal = swiper.isHorizontal();
                var transform$$1 = swiper.translate;
                var center = isHorizontal ? -transform$$1 + swiperWidth / 2 : -transform$$1 + swiperHeight / 2;
                var rotate = isHorizontal ? params.rotate : -params.rotate;
                var translate = params.depth; // Each slide offset from center

                for (var i = 0, length = slides.length; i < length; i += 1) {
                  var $slideEl = slides.eq(i);
                  var slideSize = slidesSizesGrid[i];
                  var slideOffset = $slideEl[0].swiperSlideOffset;
                  var offsetMultiplier = (center - slideOffset - slideSize / 2) / slideSize * params.modifier;
                  var rotateY = isHorizontal ? rotate * offsetMultiplier : 0;
                  var rotateX = isHorizontal ? 0 : rotate * offsetMultiplier; // var rotateZ = 0

                  var translateZ = -translate * Math.abs(offsetMultiplier);
                  var translateY = isHorizontal ? 0 : params.stretch * offsetMultiplier;
                  var translateX = isHorizontal ? params.stretch * offsetMultiplier : 0; // Fix for ultra small values

                  if (Math.abs(translateX) < 0.001) translateX = 0;
                  if (Math.abs(translateY) < 0.001) translateY = 0;
                  if (Math.abs(translateZ) < 0.001) translateZ = 0;
                  if (Math.abs(rotateY) < 0.001) rotateY = 0;
                  if (Math.abs(rotateX) < 0.001) rotateX = 0;
                  var slideTransform = "translate3d(".concat(translateX, "px,").concat(translateY, "px,").concat(translateZ, "px)  rotateX(").concat(rotateX, "deg) rotateY(").concat(rotateY, "deg)");
                  $slideEl.transform(slideTransform);
                  $slideEl[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;

                  if (params.slideShadows) {
                    // Set shadows
                    var $shadowBeforeEl = isHorizontal ? $slideEl.find('.swiper-slide-shadow-left') : $slideEl.find('.swiper-slide-shadow-top');
                    var $shadowAfterEl = isHorizontal ? $slideEl.find('.swiper-slide-shadow-right') : $slideEl.find('.swiper-slide-shadow-bottom');

                    if ($shadowBeforeEl.length === 0) {
                      $shadowBeforeEl = swiper.$("<div class=\"swiper-slide-shadow-".concat(isHorizontal ? 'left' : 'top', "\"></div>"));
                      $slideEl.append($shadowBeforeEl);
                    }

                    if ($shadowAfterEl.length === 0) {
                      $shadowAfterEl = swiper.$("<div class=\"swiper-slide-shadow-".concat(isHorizontal ? 'right' : 'bottom', "\"></div>"));
                      $slideEl.append($shadowAfterEl);
                    }

                    if ($shadowBeforeEl.length) $shadowBeforeEl[0].style.opacity = offsetMultiplier > 0 ? offsetMultiplier : 0;
                    if ($shadowAfterEl.length) $shadowAfterEl[0].style.opacity = -offsetMultiplier > 0 ? -offsetMultiplier : 0;
                  }
                } // Set correct perspective for IE10


                if (swiper.support.pointerEvents || swiper.support.prefixedPointerEvents) {
                  var ws = $wrapperEl[0].style;
                  ws.perspectiveOrigin = "".concat(center, "px 50%");
                }
              },
              setTransition: function setTransition(duration) {
                var swiper = this;
                swiper.slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);
              }
            }
          };
        }
      }, {
        key: "applyFadeEffect",
        value: function applyFadeEffect() {
          this.slideOpts = {
            autoplay: {
              delay: 2500,
              disableOnInteraction: false
            },
            on: {
              beforeInit: function beforeInit() {
                var swiper = this;
                swiper.classNames.push("".concat(swiper.params.containerModifierClass, "fade"));
                var overwriteParams = {
                  slidesPerView: 1,
                  slidesPerColumn: 1,
                  slidesPerGroup: 1,
                  watchSlidesProgress: true,
                  spaceBetween: 0,
                  virtualTranslate: true
                };
                swiper.params = Object.assign(swiper.params, overwriteParams);
                swiper.params = Object.assign(swiper.originalParams, overwriteParams);
              },
              setTranslate: function setTranslate() {
                var swiper = this;
                var slides = swiper.slides;

                for (var i = 0; i < slides.length; i += 1) {
                  var $slideEl = swiper.slides.eq(i);
                  var offset$$1 = $slideEl[0].swiperSlideOffset;
                  var tx = -offset$$1;
                  if (!swiper.params.virtualTranslate) tx -= swiper.translate;
                  var ty = 0;

                  if (!swiper.isHorizontal()) {
                    ty = tx;
                    tx = 0;
                  }

                  var slideOpacity = swiper.params.fadeEffect.crossFade ? Math.max(1 - Math.abs($slideEl[0].progress), 0) : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
                  $slideEl.css({
                    opacity: slideOpacity
                  }).transform("translate3d(".concat(tx, "px, ").concat(ty, "px, 0px)"));
                }
              },
              setTransition: function setTransition(duration) {
                var swiper = this;
                var slides = swiper.slides,
                    $wrapperEl = swiper.$wrapperEl;
                slides.transition(duration);

                if (swiper.params.virtualTranslate && duration !== 0) {
                  var eventTriggered = false;
                  slides.transitionEnd(function () {
                    if (eventTriggered) return;
                    if (!swiper || swiper.destroyed) return;
                    eventTriggered = true;
                    swiper.animating = false;
                    var triggerEvents = ['webkitTransitionEnd', 'transitionend'];

                    for (var i = 0; i < triggerEvents.length; i += 1) {
                      $wrapperEl.trigger(triggerEvents[i]);
                    }
                  });
                }
              }
            }
          };
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ngDoCheck",
        value: function ngDoCheck() {
          if (this.config.bannerAnimationEffect == 'fade') this.applyFadeEffect();else if (this.config.bannerAnimationEffect == 'cube') this.applyCubeEffect();else if (this.config.bannerAnimationEffect == 'flip') this.applyFlipEffect();else if (this.config.bannerAnimationEffect == 'coverFlow') this.applyCoverFlowEffect();

          if (this.config.bannerStyle == "bottomBulletsWhiteBackground") {
            this.slideOpts.pagination = {
              el: '.swiper-pagination',
              dynamicBullets: true
            };
          }

          if (this.config.bannerStyle == "progressBar") {
            this.slideOpts.pagination = {
              el: '.swiper-pagination',
              type: 'progressbar',
              progressbarOpposite: false
            };
          }

          if (this.config.bannerStyle == "numberBullets") {
            this.slideOpts.pagination = {
              el: '.swiper-pagination',
              renderBullet: function renderBullet(index, className) {
                return '<span class="' + className + '">' + (index + 1) + '</span>';
              }
            };
          }
        }
      }]);

      return BannerComponent;
    }();

    BannerComponent.ctorParameters = function () {
      return [{
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"]
      }, {
        type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_5__["HTTP"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]
      }];
    };

    BannerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-banner',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./banner.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/components/banner/banner.component.html"))["default"],
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./banner.component.scss */
      "./src/components/banner/banner.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"], _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_5__["HTTP"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]])], BannerComponent);
    /***/
  },

  /***/
  "./src/components/categories/categories.component.scss":
  /*!*************************************************************!*\
    !*** ./src/components/categories/categories.component.scss ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcComponentsCategoriesCategoriesComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".name .swiper-slide {\n  width: 40%;\n}\n.name ion-card {\n  margin-left: 10px;\n  margin-right: 0px;\n  background: white;\n  border-radius: 0;\n  box-shadow: #eee;\n  box-shadow: none;\n}\n.name ion-card ion-img {\n  height: 150px;\n  width: 100%;\n}\n.name ion-card img {\n  width: 100%;\n}\n.name ion-card h6 {\n  font-size: var(--heading-font-size) !important;\n  font-weight: bold;\n  text-align: center;\n  margin-bottom: 0;\n  margin-top: 5px;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap;\n  color: #000;\n}\n.name ion-card p {\n  font-size: var(--sub-heading-font-size);\n  text-align: center;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap;\n  margin-top: 0;\n  color: #747474;\n}\n.name .skeleton-name {\n  width: 100%;\n  margin-top: 15px;\n  border-radius: 0px;\n  box-shadow: 0.1px 0.1px 0.1px 0.1px #eee;\n}\n.name .skeleton-name ion-skeleton-text {\n  height: 90px;\n}\n.name .skeleton-name p {\n  height: 20px;\n  padding-left: 10px;\n  padding-right: 10px;\n}\n.name .skeleton-name ion-skeleton-text {\n  width: 100%;\n  height: 110px;\n  margin-top: 0;\n}\n.round .swiper-slide {\n  width: 40%;\n}\n.round ion-slides ion-slide ion-card {\n  background: white;\n  margin-top: 15px;\n  border-radius: 0px;\n  box-shadow: 0.1px 0.1px 0.1px 0.1px #eee;\n  border-radius: 0;\n  box-shadow: #eee;\n  box-shadow: none;\n}\n.round ion-slides ion-slide ion-card ion-avatar {\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 10px;\n}\n.round ion-slides ion-slide ion-card ion-skeleton-text {\n  height: 90px;\n}\n.round ion-slides ion-slide ion-card p {\n  height: 20px;\n  padding-left: 10px;\n  padding-right: 10px;\n}\n.round ion-slides ion-slide ion-card ion-skeleton-text {\n  width: 100%;\n  height: 110px;\n  margin-top: 0;\n}\n.round ion-slides ion-slide ion-card ion-img {\n  height: 150px;\n  width: 100%;\n}\n.round ion-slides ion-slide ion-card img {\n  width: 100%;\n}\n.round ion-slides ion-slide ion-card h6 {\n  font-size: var(--heading-font-size) !important;\n  font-weight: bold;\n  text-align: center;\n  margin-bottom: 0;\n  color: var(--ion-text-color);\n}\n.round ion-slides ion-slide ion-card p {\n  font-size: var(--sub-heading-font-size);\n  text-align: center;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap;\n  margin-top: 0;\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\n.name-count .swiper-slide {\n  width: 40%;\n}\n.name-count ion-slides ion-slide ion-card {\n  background: white;\n  border-radius: 0;\n  box-shadow: #eee;\n  box-shadow: none;\n}\n.name-count ion-slides ion-slide ion-card:nth-child(2) {\n  background: none;\n}\n.name-count ion-slides ion-slide ion-card ion-avatar {\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 10px;\n}\n.name-count ion-slides ion-slide ion-card:last-child {\n  width: 100%;\n  margin-top: 15px;\n  border-radius: 0px;\n  box-shadow: 0.1px 0.1px 0.1px 0.1px #eee;\n}\n.name-count ion-slides ion-slide ion-card:last-child ion-skeleton-text {\n  height: 90px;\n}\n.name-count ion-slides ion-slide ion-card:last-child p {\n  height: 20px;\n  padding-left: 10px;\n  padding-right: 10px;\n}\n.name-count ion-slides ion-slide ion-card:last-child ion-skeleton-text {\n  width: 100%;\n  height: 110px;\n  margin-top: 0;\n}\n.name-count ion-slides ion-slide ion-card ion-img {\n  height: 150px;\n  width: 100%;\n}\n.name-count ion-slides ion-slide ion-card img {\n  width: 100%;\n}\n.name-count ion-slides ion-slide ion-card h6 {\n  font-size: var(--heading-font-size) !important;\n  font-weight: bold;\n  text-align: center;\n  margin-bottom: 0;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap;\n}\n.name-count ion-slides ion-slide ion-card p {\n  font-size: var(--sub-heading-font-size);\n  text-align: center;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap;\n  margin-top: 0;\n}\n.grid ion-card {\n  min-height: 144px;\n  border-radius: 0px;\n  box-shadow: 0.1px 0.1px 0.1px 0.1px #eee;\n  border-radius: 0;\n  box-shadow: #eee;\n  box-shadow: none;\n}\n.grid ion-card ion-avatar {\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 10px;\n}\n.grid ion-card ion-img {\n  height: 150px;\n  width: 100%;\n}\n.grid ion-card img {\n  width: 100%;\n}\n.grid ion-card h6 {\n  font-size: var(--heading-font-size) !important;\n  font-weight: bold;\n  text-align: center;\n  margin-bottom: 0;\n  color: #000;\n}\n.grid ion-card p {\n  font-size: var(--sub-heading-font-size);\n  text-align: center;\n  margin-top: 0;\n  color: #747474;\n}\n.card-h7 {\n  margin-right: 0 !important;\n}\n.card-h8 {\n  background: none !important;\n  box-shadow: none !important;\n  margin-top: 0 !important;\n  margin-bottom: 0 !important;\n}\n.card-skeleton-h8 {\n  box-shadow: none !important;\n  margin-top: 0 !important;\n  margin-bottom: 0 !important;\n}\n.card-skeleton-h8 .skeleton-h8 {\n  height: 90% !important;\n}\n.card-h9 {\n  margin: 0;\n  border-radius: 0;\n  background: white;\n  box-shadow: none;\n}\n.card-h9 .avatar-h9 {\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 10px;\n}\n.card-h9 h6 {\n  font-weight: bold;\n  text-align: center;\n  margin-bottom: 0;\n}\n.card-h9 p {\n  white-space: nowrap;\n  margin-top: 0;\n}\n.name {\n  font-size: var(--heading-font-size) !important;\n}\n.count {\n  font-size: var(--sub-heading-font-size) !important;\n  margin-bottom: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2NhdGVnb3JpZXMvRDpcXERvY3VtZW50b3NcXFByb2dyYW1hY2lvblxcSmF2YXNjcmlwdFxcSW9uaWNcXGRlbGl2ZXJ5Y3VzdG9tZXIvc3JjXFxjb21wb25lbnRzXFxjYXRlZ29yaWVzXFxjYXRlZ29yaWVzLmNvbXBvbmVudC5zY3NzIiwic3JjL2NvbXBvbmVudHMvY2F0ZWdvcmllcy9jYXRlZ29yaWVzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNBSjtBRENJO0VBQ0UsYUFBQTtFQUNBLFdBQUE7QUNDTjtBRENJO0VBQ0UsV0FBQTtBQ0NOO0FERUk7RUFDRSw4Q0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FDQU47QURFSTtFQUNFLHVDQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ0FOO0FER0U7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLHdDQUFBO0FDREo7QURFSTtFQUNFLFlBQUE7QUNBTjtBREVJO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNBTjtBREVJO0VBQ0UsV0FBQTtFQUNBLGFBQUE7RUFDQSxhQUFBO0FDQU47QURNRTtFQUNFLFVBQUE7QUNISjtBRE9NO0VBQ0UsaUJBQUE7RUFPQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esd0NBQUE7RUFlQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUN6QlI7QURBUTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0VWO0FESVE7RUFDRSxZQUFBO0FDRlY7QURJUTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDRlY7QURJUTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtBQ0ZWO0FEUVE7RUFDRSxhQUFBO0VBQ0EsV0FBQTtBQ05WO0FEUVE7RUFDRSxXQUFBO0FDTlY7QURTUTtFQUNFLDhDQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsNEJBQUE7QUNQVjtBRFNRO0VBQ0UsdUNBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSwyQ0FBQTtBQ1BWO0FEY0U7RUFDRSxVQUFBO0FDWEo7QURlTTtFQUNFLGlCQUFBO0VBNEJBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ3hDUjtBRFdRO0VBQ0UsZ0JBQUE7QUNUVjtBRFdRO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDVFY7QURXUTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esd0NBQUE7QUNUVjtBRFVVO0VBQ0UsWUFBQTtBQ1JaO0FEVVU7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ1JaO0FEVVU7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGFBQUE7QUNSWjtBRGNRO0VBQ0UsYUFBQTtFQUNBLFdBQUE7QUNaVjtBRGNRO0VBQ0UsV0FBQTtBQ1pWO0FEZVE7RUFDRSw4Q0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ2JWO0FEZVE7RUFDRSx1Q0FBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQ2JWO0FEb0JFO0VBQ0UsaUJBQUE7RUFNQSxrQkFBQTtFQUNBLHdDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDdEJKO0FEYUk7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNYTjtBRGtCSTtFQUNFLGFBQUE7RUFDQSxXQUFBO0FDaEJOO0FEa0JJO0VBQ0UsV0FBQTtBQ2hCTjtBRG1CSTtFQUNFLDhDQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQ2pCTjtBRG1CSTtFQUNFLHVDQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ2pCTjtBRHNCQTtFQUNFLDBCQUFBO0FDbkJGO0FEcUJBO0VBQ0UsMkJBQUE7RUFDQSwyQkFBQTtFQUNBLHdCQUFBO0VBQ0EsMkJBQUE7QUNsQkY7QURvQkE7RUFDRSwyQkFBQTtFQUNBLHdCQUFBO0VBQ0EsMkJBQUE7QUNqQkY7QURrQkU7RUFDRSxzQkFBQTtBQ2hCSjtBRG1CQTtFQUNFLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNoQkY7QURpQkU7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNmSjtBRGlCRTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ2ZKO0FEaUJFO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0FDZko7QURtQkE7RUFDRSw4Q0FBQTtBQ2hCRjtBRGtCQTtFQUNFLGtEQUFBO0VBQ0EsZ0JBQUE7QUNmRiIsImZpbGUiOiJzcmMvY29tcG9uZW50cy9jYXRlZ29yaWVzL2NhdGVnb3JpZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmFtZSB7XHJcbiAgLnN3aXBlci1zbGlkZSB7XHJcbiAgICB3aWR0aDogNDAlO1xyXG4gIH1cclxuICBpb24tY2FyZCB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgYm94LXNoYWRvdzogI2VlZTtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICBpb24taW1nIHtcclxuICAgICAgaGVpZ2h0OiAxNTBweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICBoNiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpICFpbXBvcnRhbnQ7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgfVxyXG4gICAgcCB7XHJcbiAgICAgIGZvbnQtc2l6ZTogdmFyKC0tc3ViLWhlYWRpbmctZm9udC1zaXplKTtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgbWFyZ2luLXRvcDogMDtcclxuICAgICAgY29sb3I6ICM3NDc0NzQ7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5za2VsZXRvbi1uYW1lIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgIGJveC1zaGFkb3c6IDAuMXB4IDAuMXB4IDAuMXB4IDAuMXB4ICNlZWU7XHJcbiAgICBpb24tc2tlbGV0b24tdGV4dCB7XHJcbiAgICAgIGhlaWdodDogOTBweDtcclxuICAgIH1cclxuICAgIHAge1xyXG4gICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIH1cclxuICAgIGlvbi1za2VsZXRvbi10ZXh0IHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGhlaWdodDogMTEwcHg7XHJcbiAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4ucm91bmQge1xyXG4gIC5zd2lwZXItc2xpZGUge1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICB9XHJcbiAgaW9uLXNsaWRlcyB7XHJcbiAgICBpb24tc2xpZGUge1xyXG4gICAgICBpb24tY2FyZCB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgaW9uLWF2YXRhciB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgICAgICBib3gtc2hhZG93OiAwLjFweCAwLjFweCAwLjFweCAwLjFweCAjZWVlO1xyXG4gICAgICAgIGlvbi1za2VsZXRvbi10ZXh0IHtcclxuICAgICAgICAgIGhlaWdodDogOTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcCB7XHJcbiAgICAgICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpb24tc2tlbGV0b24tdGV4dCB7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIGhlaWdodDogMTEwcHg7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgICBib3gtc2hhZG93OiAjZWVlO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAgICAgaW9uLWltZyB7XHJcbiAgICAgICAgICBoZWlnaHQ6IDE1MHB4O1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGg2IHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogdmFyKC0tc3ViLWhlYWRpbmctZm9udC1zaXplKTtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICAgICAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuLm5hbWUtY291bnQge1xyXG4gIC5zd2lwZXItc2xpZGUge1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICB9XHJcbiAgaW9uLXNsaWRlcyB7XHJcbiAgICBpb24tc2xpZGUge1xyXG4gICAgICBpb24tY2FyZCB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgJjpudGgtY2hpbGQoMikge1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaW9uLWF2YXRhciB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICAgICAgICBib3gtc2hhZG93OiAwLjFweCAwLjFweCAwLjFweCAwLjFweCAjZWVlO1xyXG4gICAgICAgICAgaW9uLXNrZWxldG9uLXRleHQge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDkwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBwIHtcclxuICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpb24tc2tlbGV0b24tdGV4dCB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDExMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgIGJveC1zaGFkb3c6ICNlZWU7XHJcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICBpb24taW1nIHtcclxuICAgICAgICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaDYge1xyXG4gICAgICAgICAgZm9udC1zaXplOiB2YXIoLS1oZWFkaW5nLWZvbnQtc2l6ZSkgIWltcG9ydGFudDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHAge1xyXG4gICAgICAgICAgZm9udC1zaXplOiB2YXIoLS1zdWItaGVhZGluZy1mb250LXNpemUpO1xyXG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5ncmlkIHtcclxuICBpb24tY2FyZCB7XHJcbiAgICBtaW4taGVpZ2h0OiAxNDRweDtcclxuICAgIGlvbi1hdmF0YXIge1xyXG4gICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgfVxyXG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICAgYm94LXNoYWRvdzogMC4xcHggMC4xcHggMC4xcHggMC4xcHggI2VlZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICBib3gtc2hhZG93OiAjZWVlO1xyXG4gICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgIGlvbi1pbWcge1xyXG4gICAgICBoZWlnaHQ6IDE1MHB4O1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG5cclxuICAgIGg2IHtcclxuICAgICAgZm9udC1zaXplOiB2YXIoLS1oZWFkaW5nLWZvbnQtc2l6ZSkgIWltcG9ydGFudDtcclxuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgY29sb3I6ICMwMDA7XHJcbiAgICB9XHJcbiAgICBwIHtcclxuICAgICAgZm9udC1zaXplOiB2YXIoLS1zdWItaGVhZGluZy1mb250LXNpemUpO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgIGNvbG9yOiAjNzQ3NDc0O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmNhcmQtaDcge1xyXG4gIG1hcmdpbi1yaWdodDogMCAhaW1wb3J0YW50O1xyXG59XHJcbi5jYXJkLWg4IHtcclxuICBiYWNrZ3JvdW5kOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcclxuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuLmNhcmQtc2tlbGV0b24taDgge1xyXG4gIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xyXG4gIC5za2VsZXRvbi1oOCB7XHJcbiAgICBoZWlnaHQ6IDkwJSAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG4uY2FyZC1oOSB7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICAuYXZhdGFyLWg5IHtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICB9XHJcbiAgaDYge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gIH1cclxuICBwIHtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBtYXJnaW4tdG9wOiAwO1xyXG4gIH1cclxufVxyXG5cclxuLm5hbWUge1xyXG4gIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpICFpbXBvcnRhbnQ7XHJcbn1cclxuLmNvdW50IHtcclxuICBmb250LXNpemU6IHZhcigtLXN1Yi1oZWFkaW5nLWZvbnQtc2l6ZSkgIWltcG9ydGFudDtcclxuICBtYXJnaW4tYm90dG9tOiAwO1xyXG59XHJcbiIsIi5uYW1lIC5zd2lwZXItc2xpZGUge1xuICB3aWR0aDogNDAlO1xufVxuLm5hbWUgaW9uLWNhcmQge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBib3gtc2hhZG93OiAjZWVlO1xuICBib3gtc2hhZG93OiBub25lO1xufVxuLm5hbWUgaW9uLWNhcmQgaW9uLWltZyB7XG4gIGhlaWdodDogMTUwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm5hbWUgaW9uLWNhcmQgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubmFtZSBpb24tY2FyZCBoNiB7XG4gIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIGNvbG9yOiAjMDAwO1xufVxuLm5hbWUgaW9uLWNhcmQgcCB7XG4gIGZvbnQtc2l6ZTogdmFyKC0tc3ViLWhlYWRpbmctZm9udC1zaXplKTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgbWFyZ2luLXRvcDogMDtcbiAgY29sb3I6ICM3NDc0NzQ7XG59XG4ubmFtZSAuc2tlbGV0b24tbmFtZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG4gIGJveC1zaGFkb3c6IDAuMXB4IDAuMXB4IDAuMXB4IDAuMXB4ICNlZWU7XG59XG4ubmFtZSAuc2tlbGV0b24tbmFtZSBpb24tc2tlbGV0b24tdGV4dCB7XG4gIGhlaWdodDogOTBweDtcbn1cbi5uYW1lIC5za2VsZXRvbi1uYW1lIHAge1xuICBoZWlnaHQ6IDIwcHg7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbn1cbi5uYW1lIC5za2VsZXRvbi1uYW1lIGlvbi1za2VsZXRvbi10ZXh0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTEwcHg7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5yb3VuZCAuc3dpcGVyLXNsaWRlIHtcbiAgd2lkdGg6IDQwJTtcbn1cbi5yb3VuZCBpb24tc2xpZGVzIGlvbi1zbGlkZSBpb24tY2FyZCB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG4gIGJveC1zaGFkb3c6IDAuMXB4IDAuMXB4IDAuMXB4IDAuMXB4ICNlZWU7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJveC1zaGFkb3c6ICNlZWU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG59XG4ucm91bmQgaW9uLXNsaWRlcyBpb24tc2xpZGUgaW9uLWNhcmQgaW9uLWF2YXRhciB7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4ucm91bmQgaW9uLXNsaWRlcyBpb24tc2xpZGUgaW9uLWNhcmQgaW9uLXNrZWxldG9uLXRleHQge1xuICBoZWlnaHQ6IDkwcHg7XG59XG4ucm91bmQgaW9uLXNsaWRlcyBpb24tc2xpZGUgaW9uLWNhcmQgcCB7XG4gIGhlaWdodDogMjBweDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuLnJvdW5kIGlvbi1zbGlkZXMgaW9uLXNsaWRlIGlvbi1jYXJkIGlvbi1za2VsZXRvbi10ZXh0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTEwcHg7XG4gIG1hcmdpbi10b3A6IDA7XG59XG4ucm91bmQgaW9uLXNsaWRlcyBpb24tc2xpZGUgaW9uLWNhcmQgaW9uLWltZyB7XG4gIGhlaWdodDogMTUwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnJvdW5kIGlvbi1zbGlkZXMgaW9uLXNsaWRlIGlvbi1jYXJkIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnJvdW5kIGlvbi1zbGlkZXMgaW9uLXNsaWRlIGlvbi1jYXJkIGg2IHtcbiAgZm9udC1zaXplOiB2YXIoLS1oZWFkaW5nLWZvbnQtc2l6ZSkgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgY29sb3I6IHZhcigtLWlvbi10ZXh0LWNvbG9yKTtcbn1cbi5yb3VuZCBpb24tc2xpZGVzIGlvbi1zbGlkZSBpb24tY2FyZCBwIHtcbiAgZm9udC1zaXplOiB2YXIoLS1zdWItaGVhZGluZy1mb250LXNpemUpO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBtYXJnaW4tdG9wOiAwO1xuICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xufVxuXG4ubmFtZS1jb3VudCAuc3dpcGVyLXNsaWRlIHtcbiAgd2lkdGg6IDQwJTtcbn1cbi5uYW1lLWNvdW50IGlvbi1zbGlkZXMgaW9uLXNsaWRlIGlvbi1jYXJkIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJveC1zaGFkb3c6ICNlZWU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG59XG4ubmFtZS1jb3VudCBpb24tc2xpZGVzIGlvbi1zbGlkZSBpb24tY2FyZDpudGgtY2hpbGQoMikge1xuICBiYWNrZ3JvdW5kOiBub25lO1xufVxuLm5hbWUtY291bnQgaW9uLXNsaWRlcyBpb24tc2xpZGUgaW9uLWNhcmQgaW9uLWF2YXRhciB7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4ubmFtZS1jb3VudCBpb24tc2xpZGVzIGlvbi1zbGlkZSBpb24tY2FyZDpsYXN0LWNoaWxkIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgYm94LXNoYWRvdzogMC4xcHggMC4xcHggMC4xcHggMC4xcHggI2VlZTtcbn1cbi5uYW1lLWNvdW50IGlvbi1zbGlkZXMgaW9uLXNsaWRlIGlvbi1jYXJkOmxhc3QtY2hpbGQgaW9uLXNrZWxldG9uLXRleHQge1xuICBoZWlnaHQ6IDkwcHg7XG59XG4ubmFtZS1jb3VudCBpb24tc2xpZGVzIGlvbi1zbGlkZSBpb24tY2FyZDpsYXN0LWNoaWxkIHAge1xuICBoZWlnaHQ6IDIwcHg7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbn1cbi5uYW1lLWNvdW50IGlvbi1zbGlkZXMgaW9uLXNsaWRlIGlvbi1jYXJkOmxhc3QtY2hpbGQgaW9uLXNrZWxldG9uLXRleHQge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMTBweDtcbiAgbWFyZ2luLXRvcDogMDtcbn1cbi5uYW1lLWNvdW50IGlvbi1zbGlkZXMgaW9uLXNsaWRlIGlvbi1jYXJkIGlvbi1pbWcge1xuICBoZWlnaHQ6IDE1MHB4O1xuICB3aWR0aDogMTAwJTtcbn1cbi5uYW1lLWNvdW50IGlvbi1zbGlkZXMgaW9uLXNsaWRlIGlvbi1jYXJkIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm5hbWUtY291bnQgaW9uLXNsaWRlcyBpb24tc2xpZGUgaW9uLWNhcmQgaDYge1xuICBmb250LXNpemU6IHZhcigtLWhlYWRpbmctZm9udC1zaXplKSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbn1cbi5uYW1lLWNvdW50IGlvbi1zbGlkZXMgaW9uLXNsaWRlIGlvbi1jYXJkIHAge1xuICBmb250LXNpemU6IHZhcigtLXN1Yi1oZWFkaW5nLWZvbnQtc2l6ZSk7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5ncmlkIGlvbi1jYXJkIHtcbiAgbWluLWhlaWdodDogMTQ0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgYm94LXNoYWRvdzogMC4xcHggMC4xcHggMC4xcHggMC4xcHggI2VlZTtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgYm94LXNoYWRvdzogI2VlZTtcbiAgYm94LXNoYWRvdzogbm9uZTtcbn1cbi5ncmlkIGlvbi1jYXJkIGlvbi1hdmF0YXIge1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmdyaWQgaW9uLWNhcmQgaW9uLWltZyB7XG4gIGhlaWdodDogMTUwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmdyaWQgaW9uLWNhcmQgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4uZ3JpZCBpb24tY2FyZCBoNiB7XG4gIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIGNvbG9yOiAjMDAwO1xufVxuLmdyaWQgaW9uLWNhcmQgcCB7XG4gIGZvbnQtc2l6ZTogdmFyKC0tc3ViLWhlYWRpbmctZm9udC1zaXplKTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAwO1xuICBjb2xvcjogIzc0NzQ3NDtcbn1cblxuLmNhcmQtaDcge1xuICBtYXJnaW4tcmlnaHQ6IDAgIWltcG9ydGFudDtcbn1cblxuLmNhcmQtaDgge1xuICBiYWNrZ3JvdW5kOiBub25lICFpbXBvcnRhbnQ7XG4gIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMCAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG59XG5cbi5jYXJkLXNrZWxldG9uLWg4IHtcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcbn1cbi5jYXJkLXNrZWxldG9uLWg4IC5za2VsZXRvbi1oOCB7XG4gIGhlaWdodDogOTAlICFpbXBvcnRhbnQ7XG59XG5cbi5jYXJkLWg5IHtcbiAgbWFyZ2luOiAwO1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm94LXNoYWRvdzogbm9uZTtcbn1cbi5jYXJkLWg5IC5hdmF0YXItaDkge1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtaDkgaDYge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLmNhcmQtaDkgcCB7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5uYW1lIHtcbiAgZm9udC1zaXplOiB2YXIoLS1oZWFkaW5nLWZvbnQtc2l6ZSkgIWltcG9ydGFudDtcbn1cblxuLmNvdW50IHtcbiAgZm9udC1zaXplOiB2YXIoLS1zdWItaGVhZGluZy1mb250LXNpemUpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59Il19 */";
    /***/
  },

  /***/
  "./src/components/categories/categories.component.ts":
  /*!***********************************************************!*\
    !*** ./src/components/categories/categories.component.ts ***!
    \***********************************************************/

  /*! exports provided: CategoriesComponent */

  /***/
  function srcComponentsCategoriesCategoriesComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoriesComponent", function () {
      return CategoriesComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var CategoriesComponent = /*#__PURE__*/function () {
      function CategoriesComponent(config, shared, nav, appEventsService) {
        _classCallCheck(this, CategoriesComponent);

        this.config = config;
        this.shared = shared;
        this.nav = nav;
        this.appEventsService = appEventsService; //for product slider after banner

        this.sliderConfig = {
          slidesPerView: 2.2,
          spaceBetween: 0
        };
        this.sliderConfig2 = {
          slidesPerView: 3.5,
          spaceBetween: 0
        };
      }

      _createClass(CategoriesComponent, [{
        key: "openSubCategories",
        value: function openSubCategories(parent) {
          var count = 0;

          var _iterator = _createForOfIteratorHelper(this.shared.subCategories),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var val = _step.value;
              if (val.parent == parent.id) count++;
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          if (count == 0) this.nav.navigateForward(this.config.currentRoute + "/products/" + parent.id + "/" + parent.name + "/newest");else {
            this.appEventsService.publish("openSubcategoryPage", parent);
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return CategoriesComponent;
    }();

    CategoriesComponent.ctorParameters = function () {
      return [{
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('type'), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], CategoriesComponent.prototype, "type", void 0);
    CategoriesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-categories',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./categories.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/components/categories/categories.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./categories.component.scss */
      "./src/components/categories/categories.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"]])], CategoriesComponent);
    /***/
  },

  /***/
  "./src/components/google-map/google-map.component.scss":
  /*!*************************************************************!*\
    !*** ./src/components/google-map/google-map.component.scss ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcComponentsGoogleMapGoogleMapComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".map-component div {\n  width: 100%;\n  height: 400px;\n}\n.map-component ion-text {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2dvb2dsZS1tYXAvRDpcXERvY3VtZW50b3NcXFByb2dyYW1hY2lvblxcSmF2YXNjcmlwdFxcSW9uaWNcXGRlbGl2ZXJ5Y3VzdG9tZXIvc3JjXFxjb21wb25lbnRzXFxnb29nbGUtbWFwXFxnb29nbGUtbWFwLmNvbXBvbmVudC5zY3NzIiwic3JjL2NvbXBvbmVudHMvZ29vZ2xlLW1hcC9nb29nbGUtbWFwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksV0FBQTtFQUNBLGFBQUE7QUNBUjtBREVJO0VBQ0ksV0FBQTtBQ0FSIiwiZmlsZSI6InNyYy9jb21wb25lbnRzL2dvb2dsZS1tYXAvZ29vZ2xlLW1hcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXAtY29tcG9uZW50IHtcclxuICAgIGRpdiB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiA0MDBweDtcclxuICAgIH1cclxuICAgIGlvbi10ZXh0IHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxufVxyXG4iLCIubWFwLWNvbXBvbmVudCBkaXYge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA0MDBweDtcbn1cbi5tYXAtY29tcG9uZW50IGlvbi10ZXh0IHtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/components/google-map/google-map.component.ts":
  /*!***********************************************************!*\
    !*** ./src/components/google-map/google-map.component.ts ***!
    \***********************************************************/

  /*! exports provided: GoogleMapComponent */

  /***/
  function srcComponentsGoogleMapGoogleMapComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GoogleMapComponent", function () {
      return GoogleMapComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_user_address_user_address_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/user-address/user-address.service */
    "./src/providers/user-address/user-address.service.ts");
    /* harmony import */


    var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/google-maps */
    "./node_modules/@ionic-native/google-maps/index.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");

    var GoogleMapComponent = /*#__PURE__*/function () {
      function GoogleMapComponent(platform, shared, loading, userAddress, geolocation) {
        var _this3 = this;

        _classCallCheck(this, GoogleMapComponent);

        this.platform = platform;
        this.shared = shared;
        this.loading = loading;
        this.userAddress = userAddress;
        this.geolocation = geolocation;
        this.locationUpdated = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.myLocation = {}; //product data
        //this.data = this.shared.mapPageData;

        this.loading.show();
        var locationEnable = false;
        this.userAddress.getCordinates().then(function (res) {
          locationEnable = true;

          _this3.loading.hide();

          _this3.myLocation = res;

          _this3.loadMap();
        });
        setTimeout(function () {
          if (locationEnable == false) {
            _this3.shared.showAlert("Please Turn On Device Location");
          }
        }, 10000);
      }

      _createClass(GoogleMapComponent, [{
        key: "loadMap",
        value: function loadMap() {
          var _this4 = this;

          if (this.page == "shipping") {
            if (this.shared.orderDetails.delivery_long != "") {
              this.myLocation.lat = this.shared.orderDetails.delivery_lat;
              this.myLocation["long"] = this.shared.orderDetails.delivery_long;
            }
          }

          if (this.page == "billing") {
            if (this.shared.orderDetails.billing_long != "") {
              this.myLocation.lat = this.shared.orderDetails.billing_lat;
              this.myLocation["long"] = this.shared.orderDetails.billing_long;
            }
          }
          /* The create() function will take the ID of your map element */


          var map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_6__["GoogleMaps"].create('mapcomponent');

          map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_6__["GoogleMapsEvent"].MAP_READY).then(function (data) {
            var coordinates = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_6__["LatLng"](_this4.myLocation.lat, _this4.myLocation["long"]);
            map.setCameraTarget(coordinates);
            map.setCameraZoom(16);
          });
          var marker = map.addMarkerSync({
            position: {
              lat: this.myLocation.lat,
              lng: this.myLocation["long"]
            },
            title: "My Location",
            draggable: true
          });

          var _this = this;

          marker.on(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_6__["GoogleMapsEvent"].MARKER_DRAG_END).subscribe(function (data) {
            if (_this.page == "shipping") {
              _this.shared.orderDetails.delivery_lat = data[0].lat;
              _this.shared.orderDetails.delivery_long = data[0].lng;
              _this.shared.orderDetails.delivery_location = data[0].lat + ", " + data[0].lng;
              console.log(_this.shared.orderDetails.delivery_location);
            }

            if (_this.page == "billing") {
              _this.shared.orderDetails.billing_lat = data[0].lat;
              _this.shared.orderDetails.billing_long = data[0].lng;
              _this.shared.orderDetails.billing_location = data[0].lat + ", " + data[0].lng;
            }

            console.log(data);
          });
        }
      }, {
        key: "setThisAddress",
        value: function setThisAddress() {
          this.locationUpdated.emit();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return GoogleMapComponent;
    }();

    GoogleMapComponent.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: src_providers_user_address_user_address_service__WEBPACK_IMPORTED_MODULE_5__["UserAddressService"]
      }, {
        type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__["Geolocation"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('page'), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], GoogleMapComponent.prototype, "page", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], GoogleMapComponent.prototype, "locationUpdated", void 0);
    GoogleMapComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-google-map',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./google-map.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/components/google-map/google-map.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./google-map.component.scss */
      "./src/components/google-map/google-map.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], src_providers_user_address_user_address_service__WEBPACK_IMPORTED_MODULE_5__["UserAddressService"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__["Geolocation"]])], GoogleMapComponent);
    /***/
  },

  /***/
  "./src/components/product/product.component.scss":
  /*!*******************************************************!*\
    !*** ./src/components/product/product.component.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcComponentsProductProductComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "@charset \"UTF-8\";\n.product-component {\n  padding-bottom: 2px;\n}\n.product-component .cart-button {\n  height: 100%;\n  margin: 0px;\n  --border-radius: 0px;\n  font-size: 1vw;\n}\n.product-component .timer-button {\n  width: 100%;\n  height: 25px;\n  margin: 0px;\n  --border-radius: 0px;\n}\n.product-component .timer-button app-timer {\n  letter-spacing: 2px;\n  text-transform: lowercase;\n  font-size: 12px;\n}\n.product-component .default {\n  margin-right: 0px;\n  margin-top: 10px;\n  margin-bottom: 0px;\n  height: auto;\n  border-radius: 0px;\n}\n.product-component .default app-timer {\n  letter-spacing: 2px;\n  text-transform: lowercase;\n  font-size: 12px;\n}\n.product-component .default div div {\n  text-transform: uppercase;\n  position: absolute;\n  z-index: 1;\n  font-size: 0.6875rem;\n  background: var(--ion-color-secondary);\n  color: var(--ion-color-secondary-contrast);\n  right: 0px;\n  width: auto;\n  top: 0px;\n  border-radius: 2px;\n  padding: 2px 4px;\n}\n.product-component .default div div:nth-child(2) {\n  top: 20px;\n}\n.product-component .default #newimage {\n  height: 52px;\n  position: absolute;\n  z-index: 1;\n  left: 0px !important;\n  width: 30%;\n  top: -4px;\n  margin-left: -4.2px;\n}\n.product-component .default #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: #fff;\n  width: auto;\n  height: auto;\n}\n.product-component .default .name {\n  padding-left: 5px;\n  padding-right: 5px;\n  margin-top: 5px;\n  margin-bottom: 0px;\n}\n.product-component .default .price {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  padding-left: 5px;\n  padding-right: 5px;\n}\n.product-component .default ion-icon {\n  font-size: 22px;\n  color: var(--ion-color-secondary);\n}\n.product-component .default ion-button {\n  width: 100%;\n  height: 25px;\n  margin: 0px;\n  --border-radius: 0px;\n}\n.product-component ion-item {\n  border-bottom: solid #eee;\n  border-top: solid #eee;\n  --padding-start: 10px;\n  --padding-end: 10px;\n}\n.product-component ion-item ion-thumbnail {\n  margin: 0;\n  height: 100px;\n  width: 100px;\n}\n.product-component ion-item ion-thumbnail .badge-img {\n  position: absolute;\n  z-index: 1;\n  height: 45px;\n  width: 45px;\n  left: 9px;\n  top: 9px;\n}\n.product-component ion-item ion-thumbnail img {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n}\n.product-component ion-item #list-heart-icon {\n  margin-bottom: 0;\n  position: absolute;\n  top: 60px;\n  right: 10px;\n  font-size: 25px;\n  color: var(--ion-color-secondary);\n}\n.product-component ion-item ion-label {\n  margin-bottom: auto;\n  margin-left: 10px;\n}\n.product-component ion-item ion-label p {\n  margin-top: 10px;\n}\n.product-component ion-item .sale {\n  background: var(--ion-color-secondary);\n  padding: 2px;\n  color: var(--ion-color-secondary-contrast);\n  position: absolute;\n  z-index: 1;\n  right: 0;\n}\n.product-component ion-item .featured {\n  background: var(--ion-color-secondary);\n  padding: 2px;\n  color: var(--ion-color-secondary-contrast);\n  position: absolute;\n  z-index: 1;\n  right: 0;\n}\n.product-component ion-item .img-div {\n  font-size: 12px;\n}\n.product-component ion-item .img-div :nth-child(1) {\n  top: 0;\n}\n.product-component ion-item .img-div :nth-child(2) {\n  top: 25px;\n}\n.product-component .style21, .product-component .style12, .product-component .style5, .product-component .style11 {\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 0px;\n}\n.product-component .style21 .name, .product-component .style12 .name, .product-component .style5 .name, .product-component .style11 .name {\n  margin-top: 0px;\n}\n.product-component .style21 .price, .product-component .style12 .price, .product-component .style5 .price, .product-component .style11 .price {\n  justify-content: center;\n}\n.product-component .style21 .main-img, .product-component .style12 .main-img, .product-component .style5 .main-img, .product-component .style11 .main-img {\n  padding: 3px;\n}\n.product-component .style21 #image, .product-component .style12 #image, .product-component .style5 #image, .product-component .style11 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  height: auto;\n}\n.product-component .style21 ion-grid ion-row ion-icon, .product-component .style12 ion-grid ion-row ion-icon, .product-component .style5 ion-grid ion-row ion-icon, .product-component .style11 ion-grid ion-row ion-icon {\n  font-size: 22px;\n  color: var(--ion-color-secondary);\n}\n.product-component .style11 .price .card-price-normal {\n  color: var(--ion-color-danger) !important;\n}\n.product-component .style3 {\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 10px 10px 0 0;\n}\n.product-component .style3 .main-image {\n  position: relative;\n}\n.product-component .style3 .main-image ion-badge {\n  position: absolute;\n  top: 0;\n  left: 0px;\n  z-index: 10;\n  font-size: 10px;\n  padding: 5px 8px;\n  border-radius: 0;\n  display: inline-grid;\n}\n.product-component .style3 .main-image span {\n  margin-top: 5px;\n}\n.product-component .style3 .name {\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n.product-component .style3 .main-img {\n  padding: 3px;\n}\n.product-component .style3 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  height: auto;\n}\n.product-component .style3 ion-grid ion-row ion-icon {\n  color: var(--ion-text-color);\n  margin-top: 5px;\n  margin-right: 3px;\n  font-size: 16px !important;\n}\n.product-component .style6 {\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 0;\n}\n.product-component .style6 .main-image {\n  position: relative;\n}\n.product-component .style6 .main-image ion-fab-button {\n  position: absolute;\n  bottom: -10px;\n  right: 0px;\n  z-index: 10;\n  width: 20px;\n  height: 20px;\n  margin: 3px;\n}\n.product-component .style6 .main-image ion-fab-button ion-icon {\n  font-size: inherit;\n}\n.product-component .style6 .main-image span {\n  margin-top: 5px;\n}\n.product-component .style6 .name {\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.product-component .style6 .colum-2 {\n  margin-top: 2px;\n  margin-bottom: 2px;\n}\n.product-component .style6 .price {\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.product-component .style6 .main-img {\n  padding: 3px;\n}\n.product-component .style6 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  height: auto;\n}\n.product-component .style7 {\n  border-radius: 10px;\n  position: relative;\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n}\n.product-component .style7 .floating-tags {\n  position: absolute;\n  top: 0px;\n  left: 0px;\n  padding: 0;\n  z-index: 1;\n  display: inline-grid;\n}\n.product-component .style7 .floating-tags ion-badge {\n  font-size: 9px;\n  margin-bottom: 2px;\n  z-index: 4;\n}\n.product-component .style7 .floating-tags .first {\n  border-radius: 10px 0px 3px 0;\n  padding: 5px 5px;\n}\n.product-component .style7 .floating-tags .second {\n  border-radius: 0px 3px 3px 0px;\n  padding: 5px 5px;\n}\n.product-component .style7 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  height: auto;\n}\n.product-component .style7 ion-icon {\n  font-size: 20px;\n  color: var(--ion-color-secondary);\n}\n.product-component .style7 ion-button {\n  width: 100%;\n  height: 25px;\n  margin: 0px;\n  --border-radius: 0px;\n}\n.product-component .style8 {\n  box-shadow: none;\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 0;\n}\n.product-component .style8 .product-ratings {\n  display: flex;\n}\n.product-component .style8 .product-ratings .rating-value {\n  font-size: 10px;\n  padding-top: 2px;\n}\n.product-component .style8 .name {\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.product-component .style8 .heart-top-right {\n  top: 5px;\n  right: 5px;\n}\n.product-component .style8 .price {\n  margin-top: 3px;\n  margin-bottom: 2px;\n}\n.product-component .style8 .main-img {\n  padding: 3px;\n}\n.product-component .style8 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: #fff;\n  width: auto;\n  height: auto;\n}\n.product-component .style9 {\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 10px;\n}\n.product-component .style9 .main-image {\n  position: relative;\n}\n.product-component .style9 .main-image ion-fab-button {\n  position: absolute;\n  bottom: -10px;\n  right: 2px;\n  z-index: 10;\n  width: 25px;\n  height: 25px;\n  margin: 3px;\n}\n.product-component .style9 .main-image ion-fab-button ion-icon {\n  font-size: 17px;\n}\n.product-component .style9 .main-image span {\n  margin-top: 5px;\n}\n.product-component .style9 .name {\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.product-component .style9 .price {\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.product-component .style9 .main-img {\n  padding: 3px;\n}\n.product-component .style9 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: #fff;\n  width: auto;\n  height: auto;\n}\n.product-component .style10 {\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 10px;\n}\n.product-component .style10 .main-image {\n  position: relative;\n}\n.product-component .style10 .main-image ion-badge {\n  position: absolute;\n  top: 0;\n  left: 0px;\n  z-index: 10;\n  font-size: 10px;\n  padding: 8px 10px;\n  border-radius: 10px 10px 10px 0;\n  margin: 0;\n}\n.product-component .style10 .name {\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n.product-component .style10 .category {\n  margin-bottom: 5px;\n}\n.product-component .style10 .main-img {\n  padding: 3px;\n}\n.product-component .style10 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  height: auto;\n}\n.product-component .style10 ion-grid ion-row ion-icon {\n  color: var(--ion-color-secondary);\n  margin-top: 5px;\n  font-size: 16px !important;\n}\n.product-component .style14 {\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 10px 10px 0 0;\n  box-shadow: none;\n  --background: var(--ion-background-color);\n}\n.product-component .style14 .heart-top-right {\n  position: absolute;\n  top: 5px;\n  right: 5px;\n  font-size: 20px;\n}\n.product-component .style14 .price {\n  justify-content: center;\n}\n.product-component .style14 .card-price-normal * {\n  color: var(--ion-color-danger) !important;\n}\n.product-component .style14 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  height: auto;\n  border-radius: 10px;\n}\n.product-component .style14 ion-grid ion-row ion-icon {\n  font-size: 22px;\n  color: var(--ion-color-secondary);\n}\n.product-component .style14 ion-grid ion-row ion-button {\n  width: 100%;\n  height: 25px;\n  margin: 0px;\n  --border-radius: 0px;\n}\n.product-component .style4 {\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  box-shadow: none;\n  border-radius: 0;\n  --background: var(--ion-background-color);\n}\n.product-component .style4 .heart-top-right {\n  position: absolute;\n  top: 5px;\n  right: 5px;\n  font-size: 20px;\n}\n.product-component .style4 ins * {\n  color: var(--ion-color-danger) !important;\n}\n.product-component .style4 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  height: auto;\n  border-radius: 10px 10px 0 0;\n}\n.product-component .style4 .name {\n  margin-bottom: 5px;\n}\n.product-component .style4 ion-badge {\n  width: 100%;\n  height: 100%;\n  border-radius: 0;\n}\n.product-component .style4 ion-badge span {\n  font-size: 9px;\n  vertical-align: sub;\n}\n.product-component .style4 .price {\n  width: 100%;\n}\n.product-component .style4 ion-button {\n  margin: 0px;\n  --border-radius: 0px;\n}\n.product-component .style13 {\n  border-radius: 0px;\n  margin-top: 0px !important;\n  margin-right: 0px;\n  height: auto;\n}\n.product-component .style13 .heart-top-right {\n  right: 5px;\n  top: 5px;\n  font-size: 16px !important;\n}\n.product-component .style13 .floating-tags {\n  position: absolute;\n  top: 5px;\n  left: 0px;\n  padding: 0;\n}\n.product-component .style13 .floating-tags ion-badge {\n  border-radius: 0 3px 3px 0;\n  font-size: 9px;\n  margin-bottom: 2px;\n  padding: 3px 5px;\n}\n.product-component .style13 .name {\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.product-component .style13 .card-price-normal * {\n  color: var(--ion-color-danger) !important;\n}\n.product-component .style13 .innerprice {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.product-component .style13 .price {\n  max-width: 100%;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  border-radius: 100%;\n  width: 45px;\n  height: 45px;\n}\n.product-component .style13 .price * {\n  font-size: 10px;\n}\n.product-component .style13 .main-img {\n  padding: 3px;\n}\n.product-component .style13 #image {\n  width: auto;\n  height: auto;\n}\n.product-component .style13 .card-price-normal {\n  margin-left: 0px;\n}\n.product-component .style13 .paddingb5 {\n  padding-bottom: 5px;\n}\n.product-component .style15 {\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 0;\n  padding-top: 5px;\n}\n.product-component .style15 .name {\n  margin-top: 5px;\n  margin-bottom: 0px;\n}\n.product-component .style15 .card-price-normal * {\n  color: var(--ion-color-danger) !important;\n}\n.product-component .style15 .price {\n  max-width: 100%;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  justify-content: center;\n}\n.product-component .style15 .rating-colum {\n  padding-bottom: 5px;\n}\n.product-component .style15 .main-img {\n  padding: 3px;\n}\n.product-component .style15 #image {\n  width: auto;\n  height: auto;\n}\n.product-component .style16 {\n  margin-right: 0px;\n  margin-top: 10px !important;\n  border-radius: 0px;\n  padding-bottom: 5px;\n  margin-bottom: 5px;\n  box-shadow: none;\n  --background: var(--ion-background-color);\n}\n.product-component .style16 ion-grid {\n  box-shadow: rgba(0, 0, 0, 0.2) 0px 3px 1px -2px, rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 1px 5px 0px;\n  width: 92%;\n  background-color: white;\n  border-radius: 10px;\n  border-radius: 6px;\n  padding-top: 5px;\n  z-index: 2;\n  margin-top: -25px;\n  position: relative;\n}\n.product-component .style16 .card-price-normal * {\n  color: var(--ion-color-danger) !important;\n}\n.product-component .style16 .product-ratings {\n  padding-bottom: 5px;\n}\n.product-component .style16 .name {\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.product-component .style16 .price {\n  max-width: 100%;\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.product-component .style16 .main-img {\n  padding: 3px;\n}\n.product-component .style16 ion-icon {\n  padding: 0 5px;\n}\n.product-component .style16 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  border-radius: 10px;\n  height: auto;\n  z-index: 1;\n  padding-bottom: 20px;\n}\n.product-component .style16 ion-button {\n  --border-radius: 0 0 6px 6px !important;\n}\n.product-component .style17 .card-price-normal * {\n  color: var(--ion-color-danger) !important;\n}\n.product-component .style17 .heart-top-right {\n  right: 5px;\n  top: 5px;\n}\n.product-component .style17 ion-icon {\n  font-size: inherit !important;\n  color: #f8ce0b !important;\n}\n.product-component .style19 {\n  box-shadow: none;\n  border-radius: 0px;\n}\n.product-component .style19 .heart-top-right {\n  right: 5px;\n  top: 5px;\n  font-size: 16px !important;\n}\n.product-component .style19 .floating-tags {\n  position: absolute;\n  top: 5px;\n  left: 5px;\n  padding: 0;\n}\n.product-component .style19 ion-badge {\n  border-radius: 0;\n  font-size: 9px;\n  margin-bottom: 2px;\n  padding: 2px 5px;\n  background: transparent;\n  border: 1px solid black;\n  color: black;\n}\n.product-component .style5 .name {\n  margin-top: 5px;\n}\n.product-component .style5 .price {\n  width: 100%;\n  padding: 0;\n}\n.product-component .style5 ion-icon {\n  margin-top: 5px;\n  font-size: 16px !important;\n}\n.product-component .style12 .name {\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n.product-component .style12 .price {\n  margin-top: 0px;\n}\n.product-component .style12 .price .card-price-normal {\n  color: var(--ion-color-danger) !important;\n}\n.product-component .style12 .cart-button {\n  height: 100%;\n  margin: 0px;\n  --border-radius: 0px;\n  font-size: 1vw;\n}\n.product-component .style18 {\n  box-shadow: none;\n  border-radius: 0;\n}\n.product-component .style18 .cart {\n  display: flex;\n  padding-right: 2px;\n  padding-left: 2px;\n}\n.product-component .style18 .cart ion-text {\n  margin: auto;\n  font-size: 18px;\n}\n.product-component .style18 .cart ion-text p {\n  margin: auto;\n  margin: auto 4px;\n}\n.product-component .style18 .cart .price {\n  width: 100%;\n}\n.product-component .style23 {\n  box-shadow: none;\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 10px 10px 0 0;\n  border-radius: 0;\n}\n.product-component .style23 .main-image {\n  position: relative;\n}\n.product-component .style23 .main-image ion-badge {\n  position: absolute;\n  top: 0;\n  left: 0px;\n  z-index: 10;\n  font-size: 10px;\n  padding: 5px 8px;\n  border-radius: 0;\n  display: inline-grid;\n}\n.product-component .style23 .main-image span {\n  margin-top: 5px;\n}\n.product-component .style23 .price {\n  justify-content: center;\n}\n.product-component .style23 .name {\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n.product-component .style23 .main-img {\n  padding: 3px;\n}\n.product-component .style23 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  height: auto;\n}\n.product-component .style23 .cart {\n  justify-content: center;\n  display: flex;\n  padding-right: 2px;\n  padding-left: 2px;\n}\n.product-component .style23 .cart ion-text {\n  margin: auto;\n  font-size: 18px;\n}\n.product-component .style23 .cart ion-text p {\n  margin: auto;\n  margin: auto 4px;\n}\n.product-component .style23 ion-grid ion-row ion-icon {\n  color: var(--ion-text-color);\n  font-size: 24px !important;\n  margin-top: 5px;\n  margin-right: 3px;\n}\n.product-component .style20 {\n  box-shadow: none;\n  margin-right: 0px;\n  margin-top: 10px !important;\n  height: auto;\n  border-radius: 0px;\n}\n.product-component .style20 .main-img {\n  position: relative;\n}\n.product-component .style20 .main-img .buttons-img {\n  position: absolute;\n  bottom: 8px;\n  left: 50%;\n  transform: translate(-50%, 0%);\n  width: 100%;\n}\n.product-component .style20 .main-img .buttons-img ion-button {\n  margin-right: 0px;\n  font-size: 2vw;\n  --border-radius: 0px;\n}\n.product-component .style20 .main-img .buttons-img ion-button ion-icon {\n  font-size: 3vw;\n}\n.product-component .style20 .name {\n  margin-top: 0px;\n}\n.product-component .style20 .price {\n  justify-content: center;\n}\n.product-component .style20 #image {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n  background: var(--ion-background-color);\n  width: auto;\n  height: auto;\n}\n.product-component .style20 ion-grid ion-row ion-icon {\n  font-size: 22px;\n  color: var(--ion-color-secondary);\n}\n.product-component .justify-content-center {\n  justify-content: center;\n}\n.product-component .product-ratings {\n  padding-right: 5px;\n  padding-left: 5px;\n}\n.product-component .product-ratings .stars-outer {\n  display: inline-block;\n  position: relative;\n  font-size: 12px;\n}\n.product-component .product-ratings .stars-outer::before {\n  content: \"☆☆☆☆☆\";\n  color: #ccc;\n}\n.product-component .product-ratings .stars-outer .stars-inner {\n  font-size: 12px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  white-space: nowrap;\n  overflow: hidden;\n}\n.product-component .product-ratings .stars-outer .stars-inner::before {\n  content: \"★★★★★\";\n  color: #f8ce0b;\n}\n.product-component .category {\n  padding-left: 5px;\n  padding-right: 5px;\n  margin: 0;\n  color: var(--ion-text-color);\n  font-size: 10px;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n}\n.product-component .heart-top-right {\n  position: absolute;\n  top: 10px;\n  right: 10px;\n  font-size: 22px;\n}\n.product-component .name {\n  padding-left: 5px;\n  padding-right: 5px;\n  margin-top: 5px;\n  margin-bottom: 0px;\n  color: var(--ion-text-color);\n  font-size: 12px !important;\n  font-weight: bold !important;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n}\n.product-component .price {\n  display: flex;\n  align-items: center;\n  margin-top: 5px;\n  margin-bottom: 5px;\n  padding-left: 5px;\n  padding-right: 5px;\n  font-weight: bold;\n}\n.product-component .price .card-price-normal-through {\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n  margin-right: 5px;\n  font-size: 12px;\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n  text-decoration: line-through;\n}\n.product-component .price .card-price-normal {\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n  color: var(--ion-color-danger);\n  font-size: 12px;\n}\n.product-component .bottom-big-button {\n  width: 100%;\n  height: 25px;\n  margin: 0px;\n  --border-radius: 0px;\n  z-index: 10;\n}\n.product-component .card-add-cart {\n  position: absolute;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 9;\n}\n.product-component .card-add-cart:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  top: 0;\n  width: 110%;\n  height: 102%;\n  background-color: var(--ion-background-color);\n  opacity: 0.8;\n}\n.product-component .card-add-cart ion-icon {\n  position: unset;\n  margin: auto;\n  font-size: 2.5rem !important;\n  color: var(--ion-color-secondary);\n  z-index: 10;\n}\n[dir=rtl] .product-component .default p {\n  text-align: right;\n}\n[dir=rtl] .product-component .card-price-normal-through {\n  margin-right: 0px !important;\n  margin-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL3Byb2R1Y3QvcHJvZHVjdC5jb21wb25lbnQuc2NzcyIsInNyYy9jb21wb25lbnRzL3Byb2R1Y3QvRDpcXERvY3VtZW50b3NcXFByb2dyYW1hY2lvblxcSmF2YXNjcmlwdFxcSW9uaWNcXGRlbGl2ZXJ5Y3VzdG9tZXIvc3JjXFxjb21wb25lbnRzXFxwcm9kdWN0XFxwcm9kdWN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0FoQjtFQUNFLG1CQUFBO0FERUY7QUNERTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxjQUFBO0FER0o7QUNERTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLG9CQUFBO0FER0o7QUNESTtFQUNFLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0FER047QUNBRTtFQU1FLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBREhKO0FDTkk7RUFDRSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBRFFOO0FDQU07RUFDRSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLG9CQUFBO0VBQ0Esc0NBQUE7RUFFQSwwQ0FBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QURDUjtBQ0FRO0VBQ0UsU0FBQTtBREVWO0FDRUk7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0Esb0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLG1CQUFBO0FEQU47QUNFSTtFQUNFLCtCQUFBO1VBQUEsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FEQU47QUNHSTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QURETjtBQ0dJO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBREROO0FDSUk7RUFDRSxlQUFBO0VBQ0EsaUNBQUE7QURGTjtBQ0lJO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7QURGTjtBQ09FO0VBQ0UseUJBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7QURMSjtBQ01JO0VBU0UsU0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FEWk47QUNFTTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7QURBUjtBQ0tNO0VBQ0UsK0JBQUE7VUFBQSx1QkFBQTtBREhSO0FDTUk7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsaUNBQUE7QURKTjtBQ01JO0VBQ0UsbUJBQUE7RUFDQSxpQkFBQTtBREpOO0FDS007RUFDRSxnQkFBQTtBREhSO0FDTUk7RUFDRSxzQ0FBQTtFQUNBLFlBQUE7RUFDQSwwQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QURKTjtBQ01JO0VBQ0Usc0NBQUE7RUFDQSxZQUFBO0VBQ0EsMENBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FESk47QUNNSTtFQUNFLGVBQUE7QURKTjtBQ0tNO0VBQ0UsTUFBQTtBREhSO0FDS007RUFDRSxTQUFBO0FESFI7QUNTRTtFQVdFLGlCQUFBO0VBQ0EsMkJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QURqQko7QUNJSTtFQUNFLGVBQUE7QURGTjtBQ0tJO0VBQ0UsdUJBQUE7QURITjtBQ0tJO0VBQ0UsWUFBQTtBREhOO0FDVUk7RUFDRSwrQkFBQTtVQUFBLHVCQUFBO0VBQ0EsdUNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBRFJOO0FDWVE7RUFDRSxlQUFBO0VBQ0EsaUNBQUE7QURWVjtBQ3FCTTtFQUNFLHlDQUFBO0FEbkJSO0FDeUJFO0VBQ0UsaUJBQUE7RUFDQSwyQkFBQTtFQUNBLFlBQUE7RUFDQSw0QkFBQTtBRHZCSjtBQ3lCSTtFQUNFLGtCQUFBO0FEdkJOO0FDd0JNO0VBQ0Usa0JBQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0FEdEJSO0FDd0JNO0VBQ0UsZUFBQTtBRHRCUjtBQzBCSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBRHhCTjtBQzBCSTtFQUNFLFlBQUE7QUR4Qk47QUMyQkk7RUFDRSwrQkFBQTtVQUFBLHVCQUFBO0VBQ0EsdUNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBRHpCTjtBQzZCUTtFQUNFLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsMEJBQUE7QUQzQlY7QUNrQ0U7RUFDRSxpQkFBQTtFQUNBLDJCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FEaENKO0FDa0NJO0VBQ0Usa0JBQUE7QURoQ047QUNrQ007RUFDRSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBRGhDUjtBQ2lDUTtFQUNFLGtCQUFBO0FEL0JWO0FDbUNNO0VBQ0UsZUFBQTtBRGpDUjtBQ3FDSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBRG5DTjtBQ3NDSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBRHBDTjtBQ3NDSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBRHBDTjtBQ3NDSTtFQUNFLFlBQUE7QURwQ047QUN1Q0k7RUFDRSwrQkFBQTtVQUFBLHVCQUFBO0VBQ0EsdUNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBRHJDTjtBQ3lDRTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLDJCQUFBO0VBQ0EsWUFBQTtBRHZDSjtBQ3lDSTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLG9CQUFBO0FEdkNOO0FDd0NNO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEsVUFBQTtBRHZDUjtBQ3lDTTtFQUNFLDZCQUFBO0VBQ0EsZ0JBQUE7QUR2Q1I7QUN5Q007RUFDRSw4QkFBQTtFQUNBLGdCQUFBO0FEdkNSO0FDMENJO0VBQ0UsK0JBQUE7VUFBQSx1QkFBQTtFQUNBLHVDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUR4Q047QUMyQ0k7RUFDRSxlQUFBO0VBQ0EsaUNBQUE7QUR6Q047QUMyQ0k7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtBRHpDTjtBQzZDRTtFQUNFLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSwyQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBRDNDSjtBQzRDSTtFQUNFLGFBQUE7QUQxQ047QUMyQ007RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7QUR6Q1I7QUM2Q0k7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUQzQ047QUM2Q0k7RUFDRSxRQUFBO0VBQ0EsVUFBQTtBRDNDTjtBQzZDSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBRDNDTjtBQzZDSTtFQUNFLFlBQUE7QUQzQ047QUM4Q0k7RUFDRSwrQkFBQTtVQUFBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBRDVDTjtBQ2lERTtFQUNFLGlCQUFBO0VBQ0EsMkJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUQvQ0o7QUNpREk7RUFDRSxrQkFBQTtBRC9DTjtBQ2lETTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FEL0NSO0FDZ0RRO0VBQ0UsZUFBQTtBRDlDVjtBQ2tETTtFQUNFLGVBQUE7QURoRFI7QUNvREk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QURsRE47QUNvREk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QURsRE47QUNvREk7RUFDRSxZQUFBO0FEbEROO0FDcURJO0VBQ0UsK0JBQUE7VUFBQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QURuRE47QUN1REU7RUFDRSxpQkFBQTtFQUNBLDJCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FEckRKO0FDdURJO0VBQ0Usa0JBQUE7QURyRE47QUNzRE07RUFDRSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLCtCQUFBO0VBQ0EsU0FBQTtBRHBEUjtBQ3dESTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBRHRETjtBQ3dESTtFQUNFLGtCQUFBO0FEdEROO0FDd0RJO0VBQ0UsWUFBQTtBRHRETjtBQ3lESTtFQUNFLCtCQUFBO1VBQUEsdUJBQUE7RUFDQSx1Q0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FEdkROO0FDMkRRO0VBQ0UsaUNBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7QUR6RFY7QUNpRUU7RUFDRSxpQkFBQTtFQUNBLDJCQUFBO0VBQ0EsWUFBQTtFQUNBLDRCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5Q0FBQTtBRC9ESjtBQ2lFSTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FEL0ROO0FDaUVJO0VBQ0UsdUJBQUE7QUQvRE47QUNrRUk7RUFDRSx5Q0FBQTtBRGhFTjtBQ2tFSTtFQUNFLCtCQUFBO1VBQUEsdUJBQUE7RUFDQSx1Q0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QURoRU47QUNxRVE7RUFDRSxlQUFBO0VBQ0EsaUNBQUE7QURuRVY7QUNzRVE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtBRHBFVjtBQzBFRTtFQUNFLGlCQUFBO0VBQ0EsMkJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLHlDQUFBO0FEeEVKO0FDMEVJO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUR4RU47QUMyRUk7RUFDRSx5Q0FBQTtBRHpFTjtBQzJFSTtFQUNFLCtCQUFBO1VBQUEsdUJBQUE7RUFDQSx1Q0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsNEJBQUE7QUR6RU47QUMyRUk7RUFDRSxrQkFBQTtBRHpFTjtBQzJFSTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUR6RU47QUMwRU07RUFDRSxjQUFBO0VBQ0EsbUJBQUE7QUR4RVI7QUMyRUk7RUFDRSxXQUFBO0FEekVOO0FDMkVJO0VBQ0UsV0FBQTtFQUNBLG9CQUFBO0FEekVOO0FDNkVFO0VBQ0Usa0JBQUE7RUFDQSwwQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBRDNFSjtBQzZFSTtFQUNFLFVBQUE7RUFDQSxRQUFBO0VBQ0EsMEJBQUE7QUQzRU47QUM4RUk7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBRDVFTjtBQzhFTTtFQUNFLDBCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUQ1RVI7QUNnRkk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUQ5RU47QUNpRkk7RUFDRSx5Q0FBQTtBRC9FTjtBQ2lGSTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtBRC9FTjtBQ2lGSTtFQUNFLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FEL0VOO0FDZ0ZNO0VBQ0UsZUFBQTtBRDlFUjtBQ2lGSTtFQUNFLFlBQUE7QUQvRU47QUNrRkk7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBRGhGTjtBQ2tGSTtFQUNFLGdCQUFBO0FEaEZOO0FDa0ZJO0VBQ0UsbUJBQUE7QURoRk47QUNvRkU7RUFDRSxpQkFBQTtFQUNBLDJCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QURsRko7QUNvRkk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QURsRk47QUNvRkk7RUFDRSx5Q0FBQTtBRGxGTjtBQ29GSTtFQUNFLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtBRGxGTjtBQ29GSTtFQUNFLG1CQUFBO0FEbEZOO0FDb0ZJO0VBQ0UsWUFBQTtBRGxGTjtBQ3FGSTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FEbkZOO0FDd0ZFO0VBQ0UsaUJBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUdBLGdCQUFBO0VBQ0EseUNBQUE7QUR4Rko7QUMwRkk7RUFDRSx5SEFBQTtFQUVBLFVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FEekZOO0FDMkZJO0VBQ0UseUNBQUE7QUR6Rk47QUMyRkk7RUFDRSxtQkFBQTtBRHpGTjtBQzJGSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBRHpGTjtBQzJGSTtFQUNFLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUR6Rk47QUMyRkk7RUFDRSxZQUFBO0FEekZOO0FDMkZJO0VBQ0UsY0FBQTtBRHpGTjtBQzRGSTtFQUNFLCtCQUFBO1VBQUEsdUJBQUE7RUFDQSx1Q0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esb0JBQUE7QUQxRk47QUM2Rkk7RUFDRSx1Q0FBQTtBRDNGTjtBQ2lHSTtFQUNFLHlDQUFBO0FEL0ZOO0FDaUdJO0VBQ0UsVUFBQTtFQUNBLFFBQUE7QUQvRk47QUNpR0k7RUFDRSw2QkFBQTtFQUNBLHlCQUFBO0FEL0ZOO0FDb0dFO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtBRGxHSjtBQ29HSTtFQUNFLFVBQUE7RUFDQSxRQUFBO0VBQ0EsMEJBQUE7QURsR047QUNxR0k7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBRG5HTjtBQ3NHSTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtBRHBHTjtBQzZHSTtFQUNFLGVBQUE7QUQzR047QUM2R0k7RUFDRSxXQUFBO0VBQ0EsVUFBQTtBRDNHTjtBQzZHSTtFQUNFLGVBQUE7RUFDQSwwQkFBQTtBRDNHTjtBQ21ISTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBRGpITjtBQ21ISTtFQUNFLGVBQUE7QURqSE47QUNrSE07RUFDRSx5Q0FBQTtBRGhIUjtBQ21ISTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxjQUFBO0FEakhOO0FDcUhFO0VBQ0UsZ0JBQUE7RUFDQSxnQkFBQTtBRG5ISjtBQ3FISTtFQUNFLGFBQUE7RUFhQSxrQkFBQTtFQUNBLGlCQUFBO0FEL0hOO0FDa0hNO0VBQ0UsWUFBQTtFQUNBLGVBQUE7QURoSFI7QUNrSFE7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7QURoSFY7QUNtSE07RUFDRSxXQUFBO0FEakhSO0FDd0hFO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDJCQUFBO0VBQ0EsWUFBQTtFQUNBLDRCQUFBO0VBb0NBLGdCQUFBO0FEekpKO0FDdUhJO0VBQ0Usa0JBQUE7QURySE47QUNzSE07RUFDRSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7QURwSFI7QUNzSE07RUFDRSxlQUFBO0FEcEhSO0FDdUhJO0VBQ0UsdUJBQUE7QURySE47QUN3SEk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUR0SE47QUN3SEk7RUFDRSxZQUFBO0FEdEhOO0FDeUhJO0VBQ0UsK0JBQUE7VUFBQSx1QkFBQTtFQUNBLHVDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUR2SE47QUMySEk7RUFDRSx1QkFBQTtFQUNBLGFBQUE7RUFXQSxrQkFBQTtFQUNBLGlCQUFBO0FEbklOO0FDd0hNO0VBQ0UsWUFBQTtFQUNBLGVBQUE7QUR0SFI7QUN3SFE7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7QUR0SFY7QUNnSVE7RUFDRSw0QkFBQTtFQUNBLDBCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FEOUhWO0FDcUlFO0VBQ0UsZ0JBQUE7RUE2QkEsaUJBQUE7RUFDQSwyQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBRC9KSjtBQ2dJSTtFQUNFLGtCQUFBO0FEOUhOO0FDZ0lNO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLDhCQUFBO0VBQ0EsV0FBQTtBRDlIUjtBQ2dJUTtFQUNFLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0FEOUhWO0FDK0hVO0VBQ0UsY0FBQTtBRDdIWjtBQ21JSTtFQUNFLGVBQUE7QURqSU47QUNvSUk7RUFDRSx1QkFBQTtBRGxJTjtBQ3lJSTtFQUNFLCtCQUFBO1VBQUEsdUJBQUE7RUFDQSx1Q0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FEdklOO0FDMklRO0VBQ0UsZUFBQTtFQUNBLGlDQUFBO0FEeklWO0FDK0lFO0VBQ0UsdUJBQUE7QUQ3SUo7QUMrSUU7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0FEN0lKO0FDOElJO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUQ1SU47QUM2SU07RUFDRSxnQkFBQTtFQUNBLFdBQUE7QUQzSVI7QUM2SU07RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUQzSVI7QUM2SVE7RUFDRSxnQkFBQTtFQUNBLGNBQUE7QUQzSVY7QUNnSkU7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBRDlJSjtBQ2dKRTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FEOUlKO0FDZ0pFO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLDRCQUFBO0VBQ0EsMEJBQUE7RUFDQSw0QkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBRDlJSjtBQ2lKRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBRC9JSjtBQ2dKSTtFQUNFLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLDJDQUFBO0VBQ0EsNkJBQUE7QUQ5SU47QUNnSkk7RUFDRSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7QUQ5SU47QUNrSkU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7QURoSko7QUNvSkU7RUFDRSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0FEbEpKO0FDb0pJO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDZDQUFBO0VBQ0EsWUFBQTtBRGxKTjtBQ29KSTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsNEJBQUE7RUFDQSxpQ0FBQTtFQUNBLFdBQUE7QURsSk47QUN5Skk7RUFDRSxpQkFBQTtBRHRKTjtBQ3dKSTtFQUNFLDRCQUFBO0VBQ0EsZ0JBQUE7QUR0Sk4iLCJmaWxlIjoic3JjL2NvbXBvbmVudHMvcHJvZHVjdC9wcm9kdWN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLnByb2R1Y3QtY29tcG9uZW50IHtcbiAgcGFkZGluZy1ib3R0b206IDJweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuY2FydC1idXR0b24ge1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1hcmdpbjogMHB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbiAgZm9udC1zaXplOiAxdnc7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnRpbWVyLWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbjogMHB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAudGltZXItYnV0dG9uIGFwcC10aW1lciB7XG4gIGxldHRlci1zcGFjaW5nOiAycHg7XG4gIHRleHQtdHJhbnNmb3JtOiBsb3dlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuZGVmYXVsdCB7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogYXV0bztcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5kZWZhdWx0IGFwcC10aW1lciB7XG4gIGxldHRlci1zcGFjaW5nOiAycHg7XG4gIHRleHQtdHJhbnNmb3JtOiBsb3dlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuZGVmYXVsdCBkaXYgZGl2IHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAxO1xuICBmb250LXNpemU6IDAuNjg3NXJlbTtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5LWNvbnRyYXN0KTtcbiAgcmlnaHQ6IDBweDtcbiAgd2lkdGg6IGF1dG87XG4gIHRvcDogMHB4O1xuICBib3JkZXItcmFkaXVzOiAycHg7XG4gIHBhZGRpbmc6IDJweCA0cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLmRlZmF1bHQgZGl2IGRpdjpudGgtY2hpbGQoMikge1xuICB0b3A6IDIwcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLmRlZmF1bHQgI25ld2ltYWdlIHtcbiAgaGVpZ2h0OiA1MnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDE7XG4gIGxlZnQ6IDBweCAhaW1wb3J0YW50O1xuICB3aWR0aDogMzAlO1xuICB0b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtNC4ycHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLmRlZmF1bHQgI2ltYWdlIHtcbiAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLmRlZmF1bHQgLm5hbWUge1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuZGVmYXVsdCAucHJpY2Uge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuZGVmYXVsdCBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5kZWZhdWx0IGlvbi1idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyNXB4O1xuICBtYXJnaW46IDBweDtcbiAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgaW9uLWl0ZW0ge1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAjZWVlO1xuICBib3JkZXItdG9wOiBzb2xpZCAjZWVlO1xuICAtLXBhZGRpbmctc3RhcnQ6IDEwcHg7XG4gIC0tcGFkZGluZy1lbmQ6IDEwcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgaW9uLWl0ZW0gaW9uLXRodW1ibmFpbCB7XG4gIG1hcmdpbjogMDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDEwMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IGlvbi1pdGVtIGlvbi10aHVtYm5haWwgLmJhZGdlLWltZyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogMTtcbiAgaGVpZ2h0OiA0NXB4O1xuICB3aWR0aDogNDVweDtcbiAgbGVmdDogOXB4O1xuICB0b3A6IDlweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCBpb24taXRlbSBpb24tdGh1bWJuYWlsIGltZyB7XG4gIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IGlvbi1pdGVtICNsaXN0LWhlYXJ0LWljb24ge1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNjBweDtcbiAgcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IGF1dG87XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IGlvbi1pdGVtIGlvbi1sYWJlbCBwIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCBpb24taXRlbSAuc2FsZSB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICBwYWRkaW5nOiAycHg7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5LWNvbnRyYXN0KTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAxO1xuICByaWdodDogMDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCBpb24taXRlbSAuZmVhdHVyZWQge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgcGFkZGluZzogMnB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeS1jb250cmFzdCk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogMTtcbiAgcmlnaHQ6IDA7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgaW9uLWl0ZW0gLmltZy1kaXYge1xuICBmb250LXNpemU6IDEycHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgaW9uLWl0ZW0gLmltZy1kaXYgOm50aC1jaGlsZCgxKSB7XG4gIHRvcDogMDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCBpb24taXRlbSAuaW1nLWRpdiA6bnRoLWNoaWxkKDIpIHtcbiAgdG9wOiAyNXB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIxLCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTIsIC5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU1LCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTEge1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUyMSAubmFtZSwgLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTEyIC5uYW1lLCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNSAubmFtZSwgLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTExIC5uYW1lIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIxIC5wcmljZSwgLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTEyIC5wcmljZSwgLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTUgLnByaWNlLCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTEgLnByaWNlIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMjEgLm1haW4taW1nLCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTIgLm1haW4taW1nLCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNSAubWFpbi1pbWcsIC5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMSAubWFpbi1pbWcge1xuICBwYWRkaW5nOiAzcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMjEgI2ltYWdlLCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTIgI2ltYWdlLCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNSAjaW1hZ2UsIC5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMSAjaW1hZ2Uge1xuICBmaWx0ZXI6IGJyaWdodG5lc3MoMC45KTtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xuICB3aWR0aDogYXV0bztcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIxIGlvbi1ncmlkIGlvbi1yb3cgaW9uLWljb24sIC5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMiBpb24tZ3JpZCBpb24tcm93IGlvbi1pY29uLCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNSBpb24tZ3JpZCBpb24tcm93IGlvbi1pY29uLCAucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTEgaW9uLWdyaWQgaW9uLXJvdyBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTExIC5wcmljZSAuY2FyZC1wcmljZS1ub3JtYWwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlcikgIWltcG9ydGFudDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUzIHtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMCAwO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTMgLm1haW4taW1hZ2Uge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMyAubWFpbi1pbWFnZSBpb24tYmFkZ2Uge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMHB4O1xuICB6LWluZGV4OiAxMDtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBwYWRkaW5nOiA1cHggOHB4O1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUzIC5tYWluLWltYWdlIHNwYW4ge1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMyAubmFtZSB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTMgLm1haW4taW1nIHtcbiAgcGFkZGluZzogM3B4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTMgI2ltYWdlIHtcbiAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogYXV0bztcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUzIGlvbi1ncmlkIGlvbi1yb3cgaW9uLWljb24ge1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1yaWdodDogM3B4O1xuICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU2IHtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTYgLm1haW4taW1hZ2Uge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNiAubWFpbi1pbWFnZSBpb24tZmFiLWJ1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAtMTBweDtcbiAgcmlnaHQ6IDBweDtcbiAgei1pbmRleDogMTA7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIG1hcmdpbjogM3B4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTYgLm1haW4taW1hZ2UgaW9uLWZhYi1idXR0b24gaW9uLWljb24ge1xuICBmb250LXNpemU6IGluaGVyaXQ7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNiAubWFpbi1pbWFnZSBzcGFuIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTYgLm5hbWUge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU2IC5jb2x1bS0yIHtcbiAgbWFyZ2luLXRvcDogMnB4O1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNiAucHJpY2Uge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU2IC5tYWluLWltZyB7XG4gIHBhZGRpbmc6IDNweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU2ICNpbWFnZSB7XG4gIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNyB7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTcgLmZsb2F0aW5nLXRhZ3Mge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMHB4O1xuICBsZWZ0OiAwcHg7XG4gIHBhZGRpbmc6IDA7XG4gIHotaW5kZXg6IDE7XG4gIGRpc3BsYXk6IGlubGluZS1ncmlkO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTcgLmZsb2F0aW5nLXRhZ3MgaW9uLWJhZGdlIHtcbiAgZm9udC1zaXplOiA5cHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgei1pbmRleDogNDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU3IC5mbG9hdGluZy10YWdzIC5maXJzdCB7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMHB4IDNweCAwO1xuICBwYWRkaW5nOiA1cHggNXB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTcgLmZsb2F0aW5nLXRhZ3MgLnNlY29uZCB7XG4gIGJvcmRlci1yYWRpdXM6IDBweCAzcHggM3B4IDBweDtcbiAgcGFkZGluZzogNXB4IDVweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU3ICNpbWFnZSB7XG4gIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNyBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTcgaW9uLWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbjogMHB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU4IHtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTggLnByb2R1Y3QtcmF0aW5ncyB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlOCAucHJvZHVjdC1yYXRpbmdzIC5yYXRpbmctdmFsdWUge1xuICBmb250LXNpemU6IDEwcHg7XG4gIHBhZGRpbmctdG9wOiAycHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlOCAubmFtZSB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTggLmhlYXJ0LXRvcC1yaWdodCB7XG4gIHRvcDogNXB4O1xuICByaWdodDogNXB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTggLnByaWNlIHtcbiAgbWFyZ2luLXRvcDogM3B4O1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlOCAubWFpbi1pbWcge1xuICBwYWRkaW5nOiAzcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlOCAjaW1hZ2Uge1xuICBmaWx0ZXI6IGJyaWdodG5lc3MoMC45KTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogYXV0bztcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU5IHtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTkgLm1haW4taW1hZ2Uge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlOSAubWFpbi1pbWFnZSBpb24tZmFiLWJ1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAtMTBweDtcbiAgcmlnaHQ6IDJweDtcbiAgei1pbmRleDogMTA7XG4gIHdpZHRoOiAyNXB4O1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbjogM3B4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTkgLm1haW4taW1hZ2UgaW9uLWZhYi1idXR0b24gaW9uLWljb24ge1xuICBmb250LXNpemU6IDE3cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlOSAubWFpbi1pbWFnZSBzcGFuIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTkgLm5hbWUge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU5IC5wcmljZSB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTkgLm1haW4taW1nIHtcbiAgcGFkZGluZzogM3B4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTkgI2ltYWdlIHtcbiAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTAge1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTAgLm1haW4taW1hZ2Uge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTAgLm1haW4taW1hZ2UgaW9uLWJhZGdlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDBweDtcbiAgei1pbmRleDogMTA7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgcGFkZGluZzogOHB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAxMHB4IDA7XG4gIG1hcmdpbjogMDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMCAubmFtZSB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTEwIC5jYXRlZ29yeSB7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMCAubWFpbi1pbWcge1xuICBwYWRkaW5nOiAzcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTAgI2ltYWdlIHtcbiAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogYXV0bztcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMCBpb24tZ3JpZCBpb24tcm93IGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE0IHtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMCAwO1xuICBib3gtc2hhZG93OiBub25lO1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxNCAuaGVhcnQtdG9wLXJpZ2h0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDVweDtcbiAgcmlnaHQ6IDVweDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE0IC5wcmljZSB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE0IC5jYXJkLXByaWNlLW5vcm1hbCAqIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpICFpbXBvcnRhbnQ7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTQgI2ltYWdlIHtcbiAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogYXV0bztcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxNCBpb24tZ3JpZCBpb24tcm93IGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTQgaW9uLWdyaWQgaW9uLXJvdyBpb24tYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjVweDtcbiAgbWFyZ2luOiAwcHg7XG4gIC0tYm9yZGVyLXJhZGl1czogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTQge1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IGF1dG87XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTQgLmhlYXJ0LXRvcC1yaWdodCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1cHg7XG4gIHJpZ2h0OiA1cHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU0IGlucyAqIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpICFpbXBvcnRhbnQ7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNCAjaW1hZ2Uge1xuICBmaWx0ZXI6IGJyaWdodG5lc3MoMC45KTtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xuICB3aWR0aDogYXV0bztcbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMCAwO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTQgLm5hbWUge1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNCBpb24tYmFkZ2Uge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTQgaW9uLWJhZGdlIHNwYW4ge1xuICBmb250LXNpemU6IDlweDtcbiAgdmVydGljYWwtYWxpZ246IHN1Yjtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU0IC5wcmljZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTQgaW9uLWJ1dHRvbiB7XG4gIG1hcmdpbjogMHB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMyB7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgbWFyZ2luLXRvcDogMHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBoZWlnaHQ6IGF1dG87XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTMgLmhlYXJ0LXRvcC1yaWdodCB7XG4gIHJpZ2h0OiA1cHg7XG4gIHRvcDogNXB4O1xuICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMyAuZmxvYXRpbmctdGFncyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1cHg7XG4gIGxlZnQ6IDBweDtcbiAgcGFkZGluZzogMDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMyAuZmxvYXRpbmctdGFncyBpb24tYmFkZ2Uge1xuICBib3JkZXItcmFkaXVzOiAwIDNweCAzcHggMDtcbiAgZm9udC1zaXplOiA5cHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgcGFkZGluZzogM3B4IDVweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMyAubmFtZSB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTEzIC5jYXJkLXByaWNlLW5vcm1hbCAqIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpICFpbXBvcnRhbnQ7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTMgLmlubmVycHJpY2Uge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTEzIC5wcmljZSB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIHdpZHRoOiA0NXB4O1xuICBoZWlnaHQ6IDQ1cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTMgLnByaWNlICoge1xuICBmb250LXNpemU6IDEwcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTMgLm1haW4taW1nIHtcbiAgcGFkZGluZzogM3B4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTEzICNpbWFnZSB7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTMgLmNhcmQtcHJpY2Utbm9ybWFsIHtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMyAucGFkZGluZ2I1IHtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxNSB7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBtYXJnaW4tdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogYXV0bztcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgcGFkZGluZy10b3A6IDVweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxNSAubmFtZSB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE1IC5jYXJkLXByaWNlLW5vcm1hbCAqIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpICFpbXBvcnRhbnQ7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTUgLnByaWNlIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTUgLnJhdGluZy1jb2x1bSB7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTUgLm1haW4taW1nIHtcbiAgcGFkZGluZzogM3B4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE1ICNpbWFnZSB7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTYge1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTYgaW9uLWdyaWQge1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMikgMHB4IDNweCAxcHggLTJweCwgcmdiYSgwLCAwLCAwLCAwLjE0KSAwcHggMnB4IDJweCAwcHgsIHJnYmEoMCwgMCwgMCwgMC4xMikgMHB4IDFweCA1cHggMHB4O1xuICB3aWR0aDogOTIlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICB6LWluZGV4OiAyO1xuICBtYXJnaW4tdG9wOiAtMjVweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE2IC5jYXJkLXByaWNlLW5vcm1hbCAqIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpICFpbXBvcnRhbnQ7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTYgLnByb2R1Y3QtcmF0aW5ncyB7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTYgLm5hbWUge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxNiAucHJpY2Uge1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE2IC5tYWluLWltZyB7XG4gIHBhZGRpbmc6IDNweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxNiBpb24taWNvbiB7XG4gIHBhZGRpbmc6IDAgNXB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE2ICNpbWFnZSB7XG4gIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG4gIHdpZHRoOiBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBoZWlnaHQ6IGF1dG87XG4gIHotaW5kZXg6IDE7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE2IGlvbi1idXR0b24ge1xuICAtLWJvcmRlci1yYWRpdXM6IDAgMCA2cHggNnB4ICFpbXBvcnRhbnQ7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTcgLmNhcmQtcHJpY2Utbm9ybWFsICoge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlcikgIWltcG9ydGFudDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxNyAuaGVhcnQtdG9wLXJpZ2h0IHtcbiAgcmlnaHQ6IDVweDtcbiAgdG9wOiA1cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTcgaW9uLWljb24ge1xuICBmb250LXNpemU6IGluaGVyaXQgIWltcG9ydGFudDtcbiAgY29sb3I6ICNmOGNlMGIgIWltcG9ydGFudDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxOSB7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxOSAuaGVhcnQtdG9wLXJpZ2h0IHtcbiAgcmlnaHQ6IDVweDtcbiAgdG9wOiA1cHg7XG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE5IC5mbG9hdGluZy10YWdzIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDVweDtcbiAgbGVmdDogNXB4O1xuICBwYWRkaW5nOiAwO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE5IGlvbi1iYWRnZSB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGZvbnQtc2l6ZTogOXB4O1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG4gIHBhZGRpbmc6IDJweCA1cHg7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgY29sb3I6IGJsYWNrO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTUgLm5hbWUge1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlNSAucHJpY2Uge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGU1IGlvbi1pY29uIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMiAubmFtZSB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTEyIC5wcmljZSB7XG4gIG1hcmdpbi10b3A6IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxMiAucHJpY2UgLmNhcmQtcHJpY2Utbm9ybWFsIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpICFpbXBvcnRhbnQ7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTIgLmNhcnQtYnV0dG9uIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXJnaW46IDBweDtcbiAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG4gIGZvbnQtc2l6ZTogMXZ3O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE4IHtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxOCAuY2FydCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBhZGRpbmctcmlnaHQ6IDJweDtcbiAgcGFkZGluZy1sZWZ0OiAycHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMTggLmNhcnQgaW9uLXRleHQge1xuICBtYXJnaW46IGF1dG87XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUxOCAuY2FydCBpb24tdGV4dCBwIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW46IGF1dG8gNHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTE4IC5jYXJ0IC5wcmljZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIzIHtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMCAwO1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIzIC5tYWluLWltYWdlIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIzIC5tYWluLWltYWdlIGlvbi1iYWRnZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwcHg7XG4gIHotaW5kZXg6IDEwO1xuICBmb250LXNpemU6IDEwcHg7XG4gIHBhZGRpbmc6IDVweCA4cHg7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGRpc3BsYXk6IGlubGluZS1ncmlkO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIzIC5tYWluLWltYWdlIHNwYW4ge1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMjMgLnByaWNlIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMjMgLm5hbWUge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUyMyAubWFpbi1pbWcge1xuICBwYWRkaW5nOiAzcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMjMgI2ltYWdlIHtcbiAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogYXV0bztcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUyMyAuY2FydCB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIzIC5jYXJ0IGlvbi10ZXh0IHtcbiAgbWFyZ2luOiBhdXRvO1xuICBmb250LXNpemU6IDE4cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMjMgLmNhcnQgaW9uLXRleHQgcCB7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luOiBhdXRvIDRweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUyMyBpb24tZ3JpZCBpb24tcm93IGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi10ZXh0LWNvbG9yKTtcbiAgZm9udC1zaXplOiAyNHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLXJpZ2h0OiAzcHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMjAge1xuICBib3gtc2hhZG93OiBub25lO1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUyMCAubWFpbi1pbWcge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMjAgLm1haW4taW1nIC5idXR0b25zLWltZyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiA4cHg7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgMCUpO1xuICB3aWR0aDogMTAwJTtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuc3R5bGUyMCAubWFpbi1pbWcgLmJ1dHRvbnMtaW1nIGlvbi1idXR0b24ge1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgZm9udC1zaXplOiAydnc7XG4gIC0tYm9yZGVyLXJhZGl1czogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIwIC5tYWluLWltZyAuYnV0dG9ucy1pbWcgaW9uLWJ1dHRvbiBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogM3Z3O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIwIC5uYW1lIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIwIC5wcmljZSB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5zdHlsZTIwICNpbWFnZSB7XG4gIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnN0eWxlMjAgaW9uLWdyaWQgaW9uLXJvdyBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5qdXN0aWZ5LWNvbnRlbnQtY2VudGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnByb2R1Y3QtcmF0aW5ncyB7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnByb2R1Y3QtcmF0aW5ncyAuc3RhcnMtb3V0ZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5wcm9kdWN0LXJhdGluZ3MgLnN0YXJzLW91dGVyOjpiZWZvcmUge1xuICBjb250ZW50OiBcIuKYhuKYhuKYhuKYhuKYhlwiO1xuICBjb2xvcjogI2NjYztcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAucHJvZHVjdC1yYXRpbmdzIC5zdGFycy1vdXRlciAuc3RhcnMtaW5uZXIge1xuICBmb250LXNpemU6IDEycHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5wcm9kdWN0LXJhdGluZ3MgLnN0YXJzLW91dGVyIC5zdGFycy1pbm5lcjo6YmVmb3JlIHtcbiAgY29udGVudDogXCLimIXimIXimIXimIXimIVcIjtcbiAgY29sb3I6ICNmOGNlMGI7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLmNhdGVnb3J5IHtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xuICBmb250LXNpemU6IDEwcHg7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5oZWFydC10b3AtcmlnaHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMjJweDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAubmFtZSB7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xuICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLnByaWNlIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5wcmljZSAuY2FyZC1wcmljZS1ub3JtYWwtdGhyb3VnaCB7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xuICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAucHJpY2UgLmNhcmQtcHJpY2Utbm9ybWFsIHtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKTtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLnByb2R1Y3QtY29tcG9uZW50IC5ib3R0b20tYmlnLWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbjogMHB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbiAgei1pbmRleDogMTA7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLmNhcmQtYWRkLWNhcnQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgei1pbmRleDogOTtcbn1cbi5wcm9kdWN0LWNvbXBvbmVudCAuY2FyZC1hZGQtY2FydDpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDExMCU7XG4gIGhlaWdodDogMTAyJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xuICBvcGFjaXR5OiAwLjg7XG59XG4ucHJvZHVjdC1jb21wb25lbnQgLmNhcmQtYWRkLWNhcnQgaW9uLWljb24ge1xuICBwb3NpdGlvbjogdW5zZXQ7XG4gIG1hcmdpbjogYXV0bztcbiAgZm9udC1zaXplOiAyLjVyZW0gIWltcG9ydGFudDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICB6LWluZGV4OiAxMDtcbn1cblxuW2Rpcj1ydGxdIC5wcm9kdWN0LWNvbXBvbmVudCAuZGVmYXVsdCBwIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5bZGlyPXJ0bF0gLnByb2R1Y3QtY29tcG9uZW50IC5jYXJkLXByaWNlLW5vcm1hbC10aHJvdWdoIHtcbiAgbWFyZ2luLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcbiAgbWFyZ2luLWxlZnQ6IDVweDtcbn0iLCIucHJvZHVjdC1jb21wb25lbnQge1xyXG4gIHBhZGRpbmctYm90dG9tOiAycHg7XHJcbiAgLmNhcnQtYnV0dG9uIHtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICBmb250LXNpemU6IDF2dztcclxuICB9XHJcbiAgLnRpbWVyLWJ1dHRvbiB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMjVweDtcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XHJcblxyXG4gICAgYXBwLXRpbWVyIHtcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDJweDtcclxuICAgICAgdGV4dC10cmFuc2Zvcm06IGxvd2VyY2FzZTtcclxuICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuZGVmYXVsdCB7XHJcbiAgICBhcHAtdGltZXIge1xyXG4gICAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xyXG4gICAgICB0ZXh0LXRyYW5zZm9ybTogbG93ZXJjYXNlO1xyXG4gICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB9XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICBkaXYge1xyXG4gICAgICBkaXYge1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgZm9udC1zaXplOiAwLjY4NzVyZW07XHJcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgICAgLy90ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3QpO1xyXG4gICAgICAgIHJpZ2h0OiAwcHg7XHJcbiAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgdG9wOiAwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDJweCA0cHg7XHJcbiAgICAgICAgJjpudGgtY2hpbGQoMikge1xyXG4gICAgICAgICAgdG9wOiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgI25ld2ltYWdlIHtcclxuICAgICAgaGVpZ2h0OiA1MnB4O1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgIGxlZnQ6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgICB3aWR0aDogMzAlO1xyXG4gICAgICB0b3A6IC00cHg7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAtNC4ycHg7XHJcbiAgICB9XHJcbiAgICAjaW1hZ2Uge1xyXG4gICAgICBmaWx0ZXI6IGJyaWdodG5lc3MoMC45KTtcclxuICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgIGhlaWdodDogYXV0bztcclxuICAgIH1cclxuXHJcbiAgICAubmFtZSB7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLnByaWNlIHtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcclxuICAgIH1cclxuICAgIGlvbi1idXR0b24ge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuICAgICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyBmb3IgcHJvZHVjdCBsaXN0XHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogc29saWQgI2VlZTtcclxuICAgIGJvcmRlci10b3A6IHNvbGlkICNlZWU7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDEwcHg7XHJcbiAgICAtLXBhZGRpbmctZW5kOiAxMHB4O1xyXG4gICAgaW9uLXRodW1ibmFpbCB7XHJcbiAgICAgIC5iYWRnZS1pbWcge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgIGhlaWdodDogNDVweDtcclxuICAgICAgICB3aWR0aDogNDVweDtcclxuICAgICAgICBsZWZ0OiA5cHg7XHJcbiAgICAgICAgdG9wOiA5cHg7XHJcbiAgICAgIH1cclxuICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgICNsaXN0LWhlYXJ0LWljb24ge1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHRvcDogNjBweDtcclxuICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG4gICAgfVxyXG4gICAgaW9uLWxhYmVsIHtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogYXV0bztcclxuICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgIHAge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5zYWxlIHtcclxuICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgIHBhZGRpbmc6IDJweDtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3QpO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgIHJpZ2h0OiAwO1xyXG4gICAgfVxyXG4gICAgLmZlYXR1cmVkIHtcclxuICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgIHBhZGRpbmc6IDJweDtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3QpO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgIHJpZ2h0OiAwO1xyXG4gICAgfVxyXG4gICAgLmltZy1kaXYge1xyXG4gICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgIDpudGgtY2hpbGQoMSkge1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgfVxyXG4gICAgICA6bnRoLWNoaWxkKDIpIHtcclxuICAgICAgICB0b3A6IDI1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAgc3R5bGUgMSAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAgLnN0eWxlMjEge1xyXG4gICAgLm5hbWUge1xyXG4gICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnByaWNlIHtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAubWFpbi1pbWcge1xyXG4gICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICB9XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuXHJcbiAgICAjaW1hZ2Uge1xyXG4gICAgICBmaWx0ZXI6IGJyaWdodG5lc3MoMC45KTtcclxuICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xyXG4gICAgICB3aWR0aDogYXV0bztcclxuICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gICAgaW9uLWdyaWQge1xyXG4gICAgICBpb24tcm93IHtcclxuICAgICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gIHN0eWxlIDIgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gIC5zdHlsZTExIHtcclxuICAgIEBleHRlbmQgLnN0eWxlMjE7XHJcblxyXG4gICAgLnByaWNlIHtcclxuICAgICAgLmNhcmQtcHJpY2Utbm9ybWFsIHtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlcikgIWltcG9ydGFudDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy89PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICBzdHlsZSAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAgLnN0eWxlMyB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwIDA7XHJcblxyXG4gICAgLm1haW4taW1hZ2Uge1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIGlvbi1iYWRnZSB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgICBsZWZ0OiAwcHg7XHJcbiAgICAgICAgei1pbmRleDogMTA7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDVweCA4cHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcclxuICAgICAgfVxyXG4gICAgICBzcGFuIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmFtZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgfVxyXG4gICAgLm1haW4taW1nIHtcclxuICAgICAgcGFkZGluZzogM3B4O1xyXG4gICAgfVxyXG5cclxuICAgICNpbWFnZSB7XHJcbiAgICAgIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xyXG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgICBpb24tZ3JpZCB7XHJcbiAgICAgIGlvbi1yb3cge1xyXG4gICAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDNweDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy89PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICBzdHlsZSAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAgLnN0eWxlNiB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcblxyXG4gICAgLm1haW4taW1hZ2Uge1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICAgICBpb24tZmFiLWJ1dHRvbiB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgcmlnaHQ6IDBweDtcclxuICAgICAgICB6LWluZGV4OiAxMDtcclxuICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAzcHg7XHJcbiAgICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgICAgZm9udC1zaXplOiBpbmhlcml0O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgc3BhbiB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLm5hbWUge1xyXG4gICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuXHJcbiAgICAuY29sdW0tMiB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMnB4O1xyXG4gICAgfVxyXG4gICAgLnByaWNlIHtcclxuICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWFpbi1pbWcge1xyXG4gICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICB9XHJcblxyXG4gICAgI2ltYWdlIHtcclxuICAgICAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XHJcbiAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgIGhlaWdodDogYXV0bztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5zdHlsZTcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG5cclxuICAgIC5mbG9hdGluZy10YWdzIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB0b3A6IDBweDtcclxuICAgICAgbGVmdDogMHB4O1xyXG4gICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICB6LWluZGV4OiAxO1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcclxuICAgICAgaW9uLWJhZGdlIHtcclxuICAgICAgICBmb250LXNpemU6IDlweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcblxyXG4gICAgICAgIHotaW5kZXg6IDQ7XHJcbiAgICAgIH1cclxuICAgICAgLmZpcnN0IHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4IDBweCAzcHggMDtcclxuICAgICAgICBwYWRkaW5nOiA1cHggNXB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5zZWNvbmQge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDBweCAzcHggM3B4IDBweDtcclxuICAgICAgICBwYWRkaW5nOiA1cHggNXB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAjaW1hZ2Uge1xyXG4gICAgICBmaWx0ZXI6IGJyaWdodG5lc3MoMC45KTtcclxuICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xyXG4gICAgICB3aWR0aDogYXV0bztcclxuICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICB9XHJcbiAgICBpb24tYnV0dG9uIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGhlaWdodDogMjVweDtcclxuICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gIHN0eWxlICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAuc3R5bGU4IHtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAucHJvZHVjdC1yYXRpbmdzIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgLnJhdGluZy12YWx1ZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAycHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmFtZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLmhlYXJ0LXRvcC1yaWdodCB7XHJcbiAgICAgIHRvcDogNXB4O1xyXG4gICAgICByaWdodDogNXB4O1xyXG4gICAgfVxyXG4gICAgLnByaWNlIHtcclxuICAgICAgbWFyZ2luLXRvcDogM3B4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbiAgICB9XHJcbiAgICAubWFpbi1pbWcge1xyXG4gICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICB9XHJcblxyXG4gICAgI2ltYWdlIHtcclxuICAgICAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gIHN0eWxlICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAuc3R5bGU5IHtcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuXHJcbiAgICAubWFpbi1pbWFnZSB7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgICAgIGlvbi1mYWItYnV0dG9uIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgYm90dG9tOiAtMTBweDtcclxuICAgICAgICByaWdodDogMnB4O1xyXG4gICAgICAgIHotaW5kZXg6IDEwO1xyXG4gICAgICAgIHdpZHRoOiAyNXB4O1xyXG4gICAgICAgIGhlaWdodDogMjVweDtcclxuICAgICAgICBtYXJnaW46IDNweDtcclxuICAgICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBzcGFuIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmFtZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLnByaWNlIHtcclxuICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWFpbi1pbWcge1xyXG4gICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICB9XHJcblxyXG4gICAgI2ltYWdlIHtcclxuICAgICAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAgc3R5bGUgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gIC5zdHlsZTEwIHtcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuXHJcbiAgICAubWFpbi1pbWFnZSB7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgaW9uLWJhZGdlIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIGxlZnQ6IDBweDtcclxuICAgICAgICB6LWluZGV4OiAxMDtcclxuICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgcGFkZGluZzogOHB4IDEwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDEwcHggMDtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmFtZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgfVxyXG4gICAgLmNhdGVnb3J5IHtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgfVxyXG4gICAgLm1haW4taW1nIHtcclxuICAgICAgcGFkZGluZzogM3B4O1xyXG4gICAgfVxyXG5cclxuICAgICNpbWFnZSB7XHJcbiAgICAgIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xyXG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgICBpb24tZ3JpZCB7XHJcbiAgICAgIGlvbi1yb3cge1xyXG4gICAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy89PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICBzdHlsZSAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcblxyXG4gIC5zdHlsZTE0IHtcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDAgMDtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuXHJcbiAgICAuaGVhcnQtdG9wLXJpZ2h0IHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB0b3A6IDVweDtcclxuICAgICAgcmlnaHQ6IDVweDtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgfVxyXG4gICAgLnByaWNlIHtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLmNhcmQtcHJpY2Utbm9ybWFsICoge1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlcikgIWltcG9ydGFudDtcclxuICAgIH1cclxuICAgICNpbWFnZSB7XHJcbiAgICAgIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xyXG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWdyaWQge1xyXG4gICAgICBpb24tcm93IHtcclxuICAgICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgICAgICAvL3BhZGRpbmctdG9wOiA0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlvbi1idXR0b24ge1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gIHN0eWxlICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAuc3R5bGU0IHtcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuXHJcbiAgICAuaGVhcnQtdG9wLXJpZ2h0IHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB0b3A6IDVweDtcclxuICAgICAgcmlnaHQ6IDVweDtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIGlucyAqIHtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgICAjaW1hZ2Uge1xyXG4gICAgICBmaWx0ZXI6IGJyaWdodG5lc3MoMC45KTtcclxuICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xyXG4gICAgICB3aWR0aDogYXV0bztcclxuICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMCAwO1xyXG4gICAgfVxyXG4gICAgLm5hbWUge1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcbiAgICBpb24tYmFkZ2Uge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICBzcGFuIHtcclxuICAgICAgICBmb250LXNpemU6IDlweDtcclxuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogc3ViO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAucHJpY2Uge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICAgIGlvbi1idXR0b24ge1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuICAgICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAgc3R5bGUgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gIC5zdHlsZTEzIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcblxyXG4gICAgLmhlYXJ0LXRvcC1yaWdodCB7XHJcbiAgICAgIHJpZ2h0OiA1cHg7XHJcbiAgICAgIHRvcDogNXB4O1xyXG4gICAgICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbiAgICAuZmxvYXRpbmctdGFncyB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgdG9wOiA1cHg7XHJcbiAgICAgIGxlZnQ6IDBweDtcclxuICAgICAgcGFkZGluZzogMDtcclxuXHJcbiAgICAgIGlvbi1iYWRnZSB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMCAzcHggM3B4IDA7XHJcbiAgICAgICAgZm9udC1zaXplOiA5cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMnB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDNweCA1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmFtZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5jYXJkLXByaWNlLW5vcm1hbCAqIHtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgICAuaW5uZXJwcmljZSB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgdG9wOiA1MCU7XHJcbiAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICB9XHJcbiAgICAucHJpY2Uge1xyXG4gICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICB3aWR0aDogNDVweDtcclxuICAgICAgaGVpZ2h0OiA0NXB4O1xyXG4gICAgICAqIHtcclxuICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5tYWluLWltZyB7XHJcbiAgICAgIHBhZGRpbmc6IDNweDtcclxuICAgIH1cclxuXHJcbiAgICAjaW1hZ2Uge1xyXG4gICAgICB3aWR0aDogYXV0bztcclxuICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gICAgLmNhcmQtcHJpY2Utbm9ybWFsIHtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgIH1cclxuICAgIC5wYWRkaW5nYjUge1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gIHN0eWxlICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAuc3R5bGUxNSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG5cclxuICAgIC5uYW1lIHtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAuY2FyZC1wcmljZS1ub3JtYWwgKiB7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gICAgLnByaWNlIHtcclxuICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAucmF0aW5nLWNvbHVtIHtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgIH1cclxuICAgIC5tYWluLWltZyB7XHJcbiAgICAgIHBhZGRpbmc6IDNweDtcclxuICAgIH1cclxuXHJcbiAgICAjaW1hZ2Uge1xyXG4gICAgICB3aWR0aDogYXV0bztcclxuICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy89PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICBzdHlsZSAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAgLnN0eWxlMTYge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG5cclxuICAgIC8vb3ZlcmZsb3c6IGF1dG87XHJcbiAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcblxyXG4gICAgaW9uLWdyaWQge1xyXG4gICAgICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMikgMHB4IDNweCAxcHggLTJweCwgcmdiYSgwLCAwLCAwLCAwLjE0KSAwcHggMnB4IDJweCAwcHgsXHJcbiAgICAgICAgcmdiYSgwLCAwLCAwLCAwLjEyKSAwcHggMXB4IDVweCAwcHg7XHJcbiAgICAgIHdpZHRoOiA5MiU7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgIHotaW5kZXg6IDI7XHJcbiAgICAgIG1hcmdpbi10b3A6IC0yNXB4O1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB9XHJcbiAgICAuY2FyZC1wcmljZS1ub3JtYWwgKiB7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gICAgLnByb2R1Y3QtcmF0aW5ncyB7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcbiAgICAubmFtZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLnByaWNlIHtcclxuICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5tYWluLWltZyB7XHJcbiAgICAgIHBhZGRpbmc6IDNweDtcclxuICAgIH1cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgcGFkZGluZzogMCA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgI2ltYWdlIHtcclxuICAgICAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XHJcbiAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgei1pbmRleDogMTtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWJ1dHRvbiB7XHJcbiAgICAgIC0tYm9yZGVyLXJhZGl1czogMCAwIDZweCA2cHggIWltcG9ydGFudDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAgc3R5bGUgICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAuc3R5bGUxNyB7XHJcbiAgICAuY2FyZC1wcmljZS1ub3JtYWwgKiB7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gICAgLmhlYXJ0LXRvcC1yaWdodCB7XHJcbiAgICAgIHJpZ2h0OiA1cHg7XHJcbiAgICAgIHRvcDogNXB4O1xyXG4gICAgfVxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBmb250LXNpemU6IGluaGVyaXQgIWltcG9ydGFudDtcclxuICAgICAgY29sb3I6ICNmOGNlMGIgIWltcG9ydGFudDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAgc3R5bGUgICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAuc3R5bGUxOSB7XHJcbiAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG5cclxuICAgIC5oZWFydC10b3AtcmlnaHQge1xyXG4gICAgICByaWdodDogNXB4O1xyXG4gICAgICB0b3A6IDVweDtcclxuICAgICAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLmZsb2F0aW5nLXRhZ3Mge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHRvcDogNXB4O1xyXG4gICAgICBsZWZ0OiA1cHg7XHJcbiAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWJhZGdlIHtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgZm9udC1zaXplOiA5cHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICAgICAgcGFkZGluZzogMnB4IDVweDtcclxuICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xyXG4gICAgICBjb2xvcjogYmxhY2s7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gIHN0eWxlIDUgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5cclxuICAuc3R5bGU1IHtcclxuICAgIEBleHRlbmQgLnN0eWxlMjE7XHJcblxyXG4gICAgLm5hbWUge1xyXG4gICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICB9XHJcbiAgICAucHJpY2Uge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgcGFkZGluZzogMDtcclxuICAgIH1cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAgc3R5bGUgNiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcblxyXG4gIC5zdHlsZTEyIHtcclxuICAgIEBleHRlbmQgLnN0eWxlMjE7XHJcbiAgICAubmFtZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgfVxyXG4gICAgLnByaWNlIHtcclxuICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICAuY2FyZC1wcmljZS1ub3JtYWwge1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKSAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FydC1idXR0b24ge1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAtLWJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgICAgZm9udC1zaXplOiAxdnc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuc3R5bGUxOCB7XHJcbiAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMDtcclxuXHJcbiAgICAuY2FydCB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGlvbi10ZXh0IHtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG5cclxuICAgICAgICBwIHtcclxuICAgICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICAgIG1hcmdpbjogYXV0byA0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5wcmljZSB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIH1cclxuICAgICAgcGFkZGluZy1yaWdodDogMnB4O1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDJweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5zdHlsZTIzIHtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwIDA7XHJcblxyXG4gICAgLm1haW4taW1hZ2Uge1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIGlvbi1iYWRnZSB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgICBsZWZ0OiAwcHg7XHJcbiAgICAgICAgei1pbmRleDogMTA7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDVweCA4cHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcclxuICAgICAgfVxyXG4gICAgICBzcGFuIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5wcmljZSB7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5uYW1lIHtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcbiAgICAubWFpbi1pbWcge1xyXG4gICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICB9XHJcblxyXG4gICAgI2ltYWdlIHtcclxuICAgICAgZmlsdGVyOiBicmlnaHRuZXNzKDAuOSk7XHJcbiAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgIGhlaWdodDogYXV0bztcclxuICAgIH1cclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcblxyXG4gICAgLmNhcnQge1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgaW9uLXRleHQge1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcblxyXG4gICAgICAgIHAge1xyXG4gICAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICAgICAgbWFyZ2luOiBhdXRvIDRweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDJweDtcclxuICAgICAgcGFkZGluZy1sZWZ0OiAycHg7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWdyaWQge1xyXG4gICAgICBpb24tcm93IHtcclxuICAgICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xyXG4gICAgICAgICAgZm9udC1zaXplOiAyNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDNweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAgc3R5bGUgNyAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAgLnN0eWxlMjAge1xyXG4gICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgIC5tYWluLWltZyB7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgICAgIC5idXR0b25zLWltZyB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGJvdHRvbTogOHB4O1xyXG4gICAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAwJSk7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgIGlvbi1idXR0b24ge1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgICBmb250LXNpemU6IDJ2dztcclxuICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICAgICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDN2dztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmFtZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgIH1cclxuXHJcbiAgICAucHJpY2Uge1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIH1cclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG5cclxuICAgICNpbWFnZSB7XHJcbiAgICAgIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xyXG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgICBpb24tZ3JpZCB7XHJcbiAgICAgIGlvbi1yb3cge1xyXG4gICAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gY29tbW9uIGNzcyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAuanVzdGlmeS1jb250ZW50LWNlbnRlcntcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIH1cclxuICAucHJvZHVjdC1yYXRpbmdzIHtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgLnN0YXJzLW91dGVyIHtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICBjb250ZW50OiBcIlxcMjYwNlxcMjYwNlxcMjYwNlxcMjYwNlxcMjYwNlwiO1xyXG4gICAgICAgIGNvbG9yOiAjY2NjO1xyXG4gICAgICB9XHJcbiAgICAgIC5zdGFycy1pbm5lciB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDA7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcblxyXG4gICAgICAgICY6OmJlZm9yZSB7XHJcbiAgICAgICAgICBjb250ZW50OiBcIlxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVwiO1xyXG4gICAgICAgICAgY29sb3I6ICNmOGNlMGI7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jYXRlZ29yeSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIH1cclxuICAuaGVhcnQtdG9wLXJpZ2h0IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMTBweDtcclxuICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gIH1cclxuICAubmFtZSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XHJcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIH1cclxuICAvL3ByaWNlXHJcbiAgLnByaWNlIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIC5jYXJkLXByaWNlLW5vcm1hbC10aHJvdWdoIHtcclxuICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XHJcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xyXG4gICAgfVxyXG4gICAgLmNhcmQtcHJpY2Utbm9ybWFsIHtcclxuICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKTtcclxuICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmJvdHRvbS1iaWctYnV0dG9uIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgIHotaW5kZXg6IDEwO1xyXG4gIH1cclxuICAvL2FkZCB0byBjYXJ0IGhpZ2hsaWdodGVyXHJcblxyXG4gIC5jYXJkLWFkZC1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0b3A6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHotaW5kZXg6IDk7XHJcblxyXG4gICAgJjpiZWZvcmUge1xyXG4gICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGxlZnQ6IDA7XHJcbiAgICAgIHRvcDogMDtcclxuICAgICAgd2lkdGg6IDExMCU7XHJcbiAgICAgIGhlaWdodDogMTAyJTtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xyXG4gICAgICBvcGFjaXR5OiAwLjg7XHJcbiAgICB9XHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIHBvc2l0aW9uOiB1bnNldDtcclxuICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICBmb250LXNpemU6IDIuNXJlbSAhaW1wb3J0YW50O1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgIHotaW5kZXg6IDEwO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuW2Rpcj1cInJ0bFwiXSB7XHJcbiAgLnByb2R1Y3QtY29tcG9uZW50IHtcclxuICAgIC5kZWZhdWx0IHAge1xyXG4gICAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIH1cclxuICAgIC5jYXJkLXByaWNlLW5vcm1hbC10aHJvdWdoIHtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19 */";
    /***/
  },

  /***/
  "./src/components/product/product.component.ts":
  /*!*****************************************************!*\
    !*** ./src/components/product/product.component.ts ***!
    \*****************************************************/

  /*! exports provided: ProductComponent */

  /***/
  function srcComponentsProductProductComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductComponent", function () {
      return ProductComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var src_app_modals_login_login_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/modals/login/login.page */
    "./src/app/modals/login/login.page.ts");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var src_pipes_curency_pipe__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! src/pipes/curency.pipe */
    "./src/pipes/curency.pipe.ts");

    var ProductComponent = /*#__PURE__*/function () {
      function ProductComponent(config, shared, navCtrl, modalCtrl, appEventsService, storage, loading, sanitizer, curencyPipe) {
        var _this5 = this;

        _classCallCheck(this, ProductComponent);

        // flash_expires_date
        // flash_start_date
        // server_time
        this.config = config;
        this.shared = shared;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.appEventsService = appEventsService;
        this.storage = storage;
        this.loading = loading;
        this.sanitizer = sanitizer;
        this.curencyPipe = curencyPipe; // @Output() someEvent = new EventEmitter();

        this.expired = false;
        this.is_upcomming = false;
        this.price_html = "";
        this.cartQuantity = 0; //============================================================================================  

        this.qunatityPlus = function (q) {
          q.customers_basket_quantity++;
          q.subtotal = q.final_price * q.customers_basket_quantity;
          q.total = q.subtotal;
          if (q.quantity == null) return 0;

          if (q.customers_basket_quantity > q.quantity) {
            q.customers_basket_quantity--;
            q.subtotal = q.final_price * q.customers_basket_quantity;
            q.total = q.subtotal;
            this.shared.toast('Product Quantity is Limited!', 'short', 'center');
          }
        }; //============================================================================================  
        //function decreasing the quantity


        this.qunatityMinus = function (q) {
          if (q.customers_basket_quantity == 1) {
            this.removeCartItem(q.cart_id);
            return 0;
          }

          q.customers_basket_quantity--;
          q.subtotal = q.final_price * q.customers_basket_quantity;
          q.total = q.subtotal;
        };

        var wishListUpdate = this.appEventsService.subscribe("wishListUpdate");
        wishListUpdate.subscriptions.add(wishListUpdate.event.subscribe(function (data) {
          if (_this5.p.products_id == data.id) _this5.p.isLiked = data.value;
        }));
        var productExpired = this.appEventsService.subscribe("productExpired");
        productExpired.subscriptions.add(productExpired.event.subscribe(function (data) {
          var id = data;
          if (_this5.p.products_id == id) _this5.productExpired();
        }));
      }

      _createClass(ProductComponent, [{
        key: "productExpired",
        value: function productExpired() {
          console.log("expired " + this.p.products_name);
          this.expired = true;
        }
      }, {
        key: "pDiscount",
        value: function pDiscount() {
          if (this.type != "flash") {
            var rtn = "";
            var p1 = parseInt(this.p.products_price);
            var p2 = parseInt(this.p.discount_price);

            if (p1 == 0 || p2 == null || p2 == undefined || p2 == 0) {
              rtn = "";
            }

            var result = Math.abs((p1 - p2) / p1 * 100);
            result = parseInt(result.toString());

            if (result == 0) {
              rtn = "";
            }

            rtn = result + '%';
            return rtn;
          } else if (this.type == "flash") {
            var rtn = "";
            var p1 = parseInt(this.p.products_price);
            var p2 = parseInt(this.p.flash_price);

            if (p1 == 0 || p2 == null || p2 == undefined || p2 == 0) {
              rtn = "";
            }

            var result = Math.abs((p1 - p2) / p1 * 100);
            result = parseInt(result.toString());

            if (result == 0) {
              rtn = "";
            }

            rtn = result + '%';
            return rtn;
          }
        }
      }, {
        key: "showProductDetail",
        value: function showProductDetail() {
          var _this6 = this;

          if (this.type == 'flash') {
            this.loading.show();
            var dat = {};
            if (this.shared.customerData != null) dat.customers_id = this.shared.customerData.customers_id;else dat.customers_id = null;
            dat.products_id = this.p.products_id;
            dat.language_id = this.config.langId;
            dat.currency_code = this.config.currecnyCode;
            dat.type = 'flashsale';
            this.config.postHttp('getallproducts', dat).then(function (data) {
              _this6.loading.hide();

              if (data.success == 1) {
                _this6.shared.singleProductPageData.push(data.product_data[0]);

                _this6.navCtrl.navigateForward(_this6.config.currentRoute + "/product-detail/" + _this6.p.products_id);
              }
            }, function (err) {
              console.log(err);
            });
          } else {
            this.shared.singleProductPageData.push(this.p);
            this.navCtrl.navigateForward(this.config.currentRoute + "/product-detail/" + this.p.products_id);
          }

          if (this.type != 'recent' && this.type != 'flash') this.shared.addToRecent(this.p);
        }
      }, {
        key: "checkProductNew",
        value: function checkProductNew() {
          var pDate = new Date(this.p.products_date_added);
          var date = pDate.getTime() + this.config.newProductDuration * 86400000;
          var todayDate = new Date().getTime();
          if (date > todayDate) return true;else return false;
        }
      }, {
        key: "addToCart",
        value: function addToCart() {
          this.shared.addToCart(this.p, []);
        }
      }, {
        key: "isInCart",
        value: function isInCart() {
          var found = false;

          var _iterator2 = _createForOfIteratorHelper(this.shared.cartProducts),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var value = _step2.value;

              if (value.products_id == this.p.products_id) {
                found = true;
              }
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }

          if (found == true) return true;else return false;
        }
      }, {
        key: "removeRecent",
        value: function removeRecent() {
          this.shared.removeRecent(this.p);
        }
      }, {
        key: "getProductImage",
        value: function getProductImage() {
          return this.config.imgUrl + this.p.products_image;
        }
      }, {
        key: "getButtonText",
        value: function getButtonText() {
          if (this.p.defaultStock > 0 && this.p.products_type == 0) return 'ADD TO CART';
          if (this.p.products_type != 0) return 'DETAILS';
          if (this.p.defaultStock <= 0 && this.p.products_type == 0) return 'OUT OF STOCK';
        }
      }, {
        key: "buttonClick",
        value: function buttonClick() {
          if (this.type == 'flash') this.showProductDetail();else if (this.getButtonText() == 'ADD TO CART') this.addToCart();else if (this.getButtonText() == 'DETAILS') this.showProductDetail();else if (this.getButtonText() == 'OUT OF STOCK') this.shared.toast("OUT OF STOCK");
        }
      }, {
        key: "getButtonColor",
        value: function getButtonColor() {
          if (this.getButtonText() == 'ADD TO CART') return 'secondary';else if (this.getButtonText() == 'DETAILS') return 'secondary';else if (this.getButtonText() == 'OUT OF STOCK') return 'danger';
        }
      }, {
        key: "getCategoryName",
        value: function getCategoryName() {
          if (this.p.categories.length != 0) return this.p.categories[0].categories_name;
        }
      }, {
        key: "removeProduct",
        value: function removeProduct(type) {
          if (type == "recent") {
            this.removeRecent();
          } else if (type == "wishList") {
            this.removeWishList();
          }
        }
      }, {
        key: "clickWishList",
        value: function clickWishList() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!(this.shared.customerData.customers_id == null || this.shared.customerData.customers_id == undefined)) {
                      _context.next = 8;
                      break;
                    }

                    _context.next = 3;
                    return this.modalCtrl.create({
                      component: src_app_modals_login_login_page__WEBPACK_IMPORTED_MODULE_7__["LoginPage"],
                      componentProps: {
                        'hideGuestLogin': true
                      }
                    });

                  case 3:
                    modal = _context.sent;
                    _context.next = 6;
                    return modal.present();

                  case 6:
                    _context.next = 9;
                    break;

                  case 8:
                    if (this.p.isLiked == '0') {
                      this.addWishList();
                    } else this.removeWishList();

                  case 9:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "addWishList",
        value: function addWishList() {
          this.shared.addWishList(this.p);
        }
      }, {
        key: "removeWishList",
        value: function removeWishList() {
          this.shared.removeWishList(this.p);
        }
      }, {
        key: "getHeartName",
        value: function getHeartName() {
          if (this.p.isLiked == '0') return "heart-outline";else return "heart";
        }
      }, {
        key: "getPriceHtml",
        value: function getPriceHtml() {
          var r = "";
          var p = this.curencyPipe.transform(this.p.products_price);
          var pD = this.curencyPipe.transform(this.p.discount_price);

          if (this.type != 'flash') {
            if (this.p.discount_price == null) r += "<span class='card-price-normal ion-float-start'>" + p + "</span>";
            if (this.p.discount_price != null) r += "<span class='card-price-normal-through ion-float-start' > " + p + " </span>";
            if (this.p.discount_price != null) r += "<span class='card-price-normal ion-float-start' > " + pD + " </span>";
          } else if (this.type == 'flash') {
            var pF = this.curencyPipe.transform(this.p.flash_price);
            r += "<span class='card-price-normal-through ion-float-start' > " + p + " </span>";
            r += "<span class='card-price-normal ion-float-start' > " + pF + " </span>";
          }

          return r;
        }
      }, {
        key: "getRating",
        value: function getRating() {
          return null;
        }
      }, {
        key: "getParsedRating",
        value: function getParsedRating() {
          return null;
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          if (this.type == 'flash') {
            if (this.p.server_time < this.p.flash_start_date) this.is_upcomming = true; //console.log("server time less than " + (this.p.server_time - this.p.flash_start_date));
          }

          this.price_html = this.getPriceHtml();
        }
      }, {
        key: "ngDoCheck",
        value: function ngDoCheck() {
          var _this7 = this;

          this.shared.cartProducts.forEach(function (element) {
            if (element.products_id == _this7.p.products_id) {
              _this7.cartQuantity = element.customers_basket_quantity;
            }
          });
        }
      }, {
        key: "addingToCart",
        value: function addingToCart() {
          var _this8 = this;

          if (this.getButtonText() == "ADD TO CART") {
            if (this.cartQuantity == 0) {
              this.addToCart();
            } else {
              this.shared.cartProducts.forEach(function (element) {
                if (element.products_id == _this8.p.products_id) {
                  _this8.qunatityPlus(element);
                }
              });
            }
          } else {
            this.buttonClick();
          }
        }
      }, {
        key: "removingToCart",
        value: function removingToCart() {
          var _this9 = this;

          this.shared.cartProducts.forEach(function (element) {
            if (element.products_id == _this9.p.products_id) {
              _this9.qunatityMinus(element);
            }
          });
        }
      }, {
        key: "removeCartItem",
        value: function removeCartItem(id) {
          this.shared.removeCart(id);
          this.cartQuantity = 0;
        }
      }, {
        key: "updateCart",
        value: function updateCart() {
          this.shared.removeCart(this.shared.cartProducts);
        }
      }, {
        key: "ratingPercentage",
        value: function ratingPercentage() {
          return this.shared.getProductRatingPercentage(this.p.rating);
        }
      }]);

      return ProductComponent;
    }();

    ProductComponent.ctorParameters = function () {
      return [{
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__["AppEventsService"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__["DomSanitizer"]
      }, {
        type: src_pipes_curency_pipe__WEBPACK_IMPORTED_MODULE_10__["CurencyPipe"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('data'), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], ProductComponent.prototype, "p", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('type'), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], ProductComponent.prototype, "type", void 0);
    ProductComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-product',
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./product.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/components/product/product.component.html"))["default"],
      providers: [src_pipes_curency_pipe__WEBPACK_IMPORTED_MODULE_10__["CurencyPipe"]],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./product.component.scss */
      "./src/components/product/product.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__["AppEventsService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__["DomSanitizer"], src_pipes_curency_pipe__WEBPACK_IMPORTED_MODULE_10__["CurencyPipe"]])], ProductComponent);
    /***/
  },

  /***/
  "./src/components/scrolling-featured-products/scrolling-featured-products.component.scss":
  /*!***********************************************************************************************!*\
    !*** ./src/components/scrolling-featured-products/scrolling-featured-products.component.scss ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcComponentsScrollingFeaturedProductsScrollingFeaturedProductsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-grid ion-row {\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL3Njcm9sbGluZy1mZWF0dXJlZC1wcm9kdWN0cy9EOlxcRG9jdW1lbnRvc1xcUHJvZ3JhbWFjaW9uXFxKYXZhc2NyaXB0XFxJb25pY1xcZGVsaXZlcnljdXN0b21lci9zcmNcXGNvbXBvbmVudHNcXHNjcm9sbGluZy1mZWF0dXJlZC1wcm9kdWN0c1xcc2Nyb2xsaW5nLWZlYXR1cmVkLXByb2R1Y3RzLmNvbXBvbmVudC5zY3NzIiwic3JjL2NvbXBvbmVudHMvc2Nyb2xsaW5nLWZlYXR1cmVkLXByb2R1Y3RzL3Njcm9sbGluZy1mZWF0dXJlZC1wcm9kdWN0cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGtCQUFBO0FDQVIiLCJmaWxlIjoic3JjL2NvbXBvbmVudHMvc2Nyb2xsaW5nLWZlYXR1cmVkLXByb2R1Y3RzL3Njcm9sbGluZy1mZWF0dXJlZC1wcm9kdWN0cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1ncmlke1xyXG4gICAgaW9uLXJvd3tcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICB9XHJcbn0iLCJpb24tZ3JpZCBpb24tcm93IHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/components/scrolling-featured-products/scrolling-featured-products.component.ts":
  /*!*********************************************************************************************!*\
    !*** ./src/components/scrolling-featured-products/scrolling-featured-products.component.ts ***!
    \*********************************************************************************************/

  /*! exports provided: ScrollingFeaturedProductsComponent */

  /***/
  function srcComponentsScrollingFeaturedProductsScrollingFeaturedProductsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ScrollingFeaturedProductsComponent", function () {
      return ScrollingFeaturedProductsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    var ScrollingFeaturedProductsComponent = /*#__PURE__*/function () {
      function ScrollingFeaturedProductsComponent(config, shared) {
        _classCallCheck(this, ScrollingFeaturedProductsComponent);

        this.config = config;
        this.shared = shared; // For products

        this.products = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.selected = '';
        this.page = 0;
        this.count = 0;
        this.loadingServerData = false;
      }

      _createClass(ScrollingFeaturedProductsComponent, [{
        key: "getProducts",
        value: function getProducts() {
          var _this10 = this;

          if (this.loadingServerData) return 0;

          if (this.page == 0) {
            this.count++;
            this.loadingServerData = false;
          }

          this.loadingServerData = true;
          var data = {};
          if (this.shared.customerData.customers_id != null) data.customers_id = this.shared.customerData.customers_id;
          data.page_number = this.page;
          data.language_id = this.config.langId;
          data.currency_code = this.config.currecnyCode;
          data.type = 'most liked';
          this.config.postHttp('getallproducts', data).then(function (data) {
            var dat = data.product_data;

            _this10.infinite.complete();

            if (_this10.page == 0) {
              _this10.products = new Array();
            }

            if (dat.length != 0) {
              _this10.page++;

              var _iterator3 = _createForOfIteratorHelper(dat),
                  _step3;

              try {
                for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                  var value = _step3.value;

                  _this10.products.push(value);
                }
              } catch (err) {
                _iterator3.e(err);
              } finally {
                _iterator3.f();
              }
            }

            if (dat.length == 0) {
              _this10.infinite.disabled = true;
            }

            _this10.loadingServerData = false;
          });
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.getProducts();
        }
      }]);

      return ScrollingFeaturedProductsComponent;
    }();

    ScrollingFeaturedProductsComponent.ctorParameters = function () {
      return [{
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonInfiniteScroll"], {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonInfiniteScroll"])], ScrollingFeaturedProductsComponent.prototype, "infinite", void 0);
    ScrollingFeaturedProductsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-scrolling-featured-products',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./scrolling-featured-products.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/components/scrolling-featured-products/scrolling-featured-products.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./scrolling-featured-products.component.scss */
      "./src/components/scrolling-featured-products/scrolling-featured-products.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]])], ScrollingFeaturedProductsComponent);
    /***/
  },

  /***/
  "./src/components/share/share.module.ts":
  /*!**********************************************!*\
    !*** ./src/components/share/share.module.ts ***!
    \**********************************************/

  /*! exports provided: ShareModule */

  /***/
  function srcComponentsShareShareModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShareModule", function () {
      return ShareModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _banner_banner_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../banner/banner.component */
    "./src/components/banner/banner.component.ts");
    /* harmony import */


    var _product_product_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../product/product.component */
    "./src/components/product/product.component.ts");
    /* harmony import */


    var _sliding_tabs_sliding_tabs_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../sliding-tabs/sliding-tabs.component */
    "./src/components/sliding-tabs/sliding-tabs.component.ts");
    /* harmony import */


    var _scrolling_featured_products_scrolling_featured_products_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../scrolling-featured-products/scrolling-featured-products.component */
    "./src/components/scrolling-featured-products/scrolling-featured-products.component.ts");
    /* harmony import */


    var _categories_categories_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../categories/categories.component */
    "./src/components/categories/categories.component.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");
    /* harmony import */


    var _timer_timer_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../timer/timer.component */
    "./src/components/timer/timer.component.ts");
    /* harmony import */


    var _google_map_google_map_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../google-map/google-map.component */
    "./src/components/google-map/google-map.component.ts"); //for home banner
    //for home footer segment
    // for product
    //for sliding tab
    //for featrued product scrolling
    //for categories


    var ShareModule = function ShareModule() {
      _classCallCheck(this, ShareModule);
    };

    ShareModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_banner_banner_component__WEBPACK_IMPORTED_MODULE_5__["BannerComponent"], _product_product_component__WEBPACK_IMPORTED_MODULE_6__["ProductComponent"], _sliding_tabs_sliding_tabs_component__WEBPACK_IMPORTED_MODULE_7__["SlidingTabsComponent"], _scrolling_featured_products_scrolling_featured_products_component__WEBPACK_IMPORTED_MODULE_8__["ScrollingFeaturedProductsComponent"], _categories_categories_component__WEBPACK_IMPORTED_MODULE_9__["CategoriesComponent"], _timer_timer_component__WEBPACK_IMPORTED_MODULE_11__["TimerComponent"], _google_map_google_map_component__WEBPACK_IMPORTED_MODULE_12__["GoogleMapComponent"]],
      exports: [_banner_banner_component__WEBPACK_IMPORTED_MODULE_5__["BannerComponent"], _product_product_component__WEBPACK_IMPORTED_MODULE_6__["ProductComponent"], _sliding_tabs_sliding_tabs_component__WEBPACK_IMPORTED_MODULE_7__["SlidingTabsComponent"], _scrolling_featured_products_scrolling_featured_products_component__WEBPACK_IMPORTED_MODULE_8__["ScrollingFeaturedProductsComponent"], _categories_categories_component__WEBPACK_IMPORTED_MODULE_9__["CategoriesComponent"], _timer_timer_component__WEBPACK_IMPORTED_MODULE_11__["TimerComponent"], _google_map_google_map_component__WEBPACK_IMPORTED_MODULE_12__["GoogleMapComponent"]],
      imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_10__["PipesModule"]]
    })], ShareModule);
    /***/
  },

  /***/
  "./src/components/sliding-tabs/sliding-tabs.component.scss":
  /*!*****************************************************************!*\
    !*** ./src/components/sliding-tabs/sliding-tabs.component.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcComponentsSlidingTabsSlidingTabsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".sliding-tabs .first-slide {\n  width: 80px !important;\n}\n.sliding-tabs ion-slides {\n  background-color: var(--ion-background-color);\n}\n.sliding-tabs ion-slides ion-slide {\n  height: auto;\n  display: inline-grid;\n  text-align: center;\n  width: auto;\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 12px;\n  border-top-width: 0;\n  border-left-width: 0;\n  border-right-width: 0;\n  border-bottom-style: solid;\n  border-bottom-width: 2px;\n  border-bottom-color: transparent;\n  color: var(--ion-text-color);\n  text-transform: uppercase;\n  min-height: 56px;\n}\n.sliding-tabs ion-slides ion-slide img {\n  width: 32px;\n  height: 36px;\n  padding: 6px;\n  margin: auto;\n}\n.sliding-tabs ion-slides .selected {\n  border-bottom-color: var(--ion-primary-color);\n  background-color: var(accent-color);\n}\n.sliding-tabs ion-grid ion-row {\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL3NsaWRpbmctdGFicy9EOlxcRG9jdW1lbnRvc1xcUHJvZ3JhbWFjaW9uXFxKYXZhc2NyaXB0XFxJb25pY1xcZGVsaXZlcnljdXN0b21lci9zcmNcXGNvbXBvbmVudHNcXHNsaWRpbmctdGFic1xcc2xpZGluZy10YWJzLmNvbXBvbmVudC5zY3NzIiwic3JjL2NvbXBvbmVudHMvc2xpZGluZy10YWJzL3NsaWRpbmctdGFicy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLHNCQUFBO0FDQVI7QURFSTtFQUNJLDZDQUFBO0FDQVI7QURDUTtFQU9JLFlBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLHFCQUFBO0VBQ0EsMEJBQUE7RUFDQSx3QkFBQTtFQUNBLGdDQUFBO0VBQ0EsNEJBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0FDTFo7QURoQlk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDa0JoQjtBRENRO0VBQ0ksNkNBQUE7RUFDQSxtQ0FBQTtBQ0NaO0FESVE7RUFDSSxrQkFBQTtBQ0ZaIiwiZmlsZSI6InNyYy9jb21wb25lbnRzL3NsaWRpbmctdGFicy9zbGlkaW5nLXRhYnMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2xpZGluZy10YWJzIHtcclxuICAgIC5maXJzdC1zbGlkZSB7XHJcbiAgICAgICAgd2lkdGg6IDgwcHggIWltcG9ydGFudDtcclxuICAgIH1cclxuICAgIGlvbi1zbGlkZXMge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuICAgICAgICBpb24tc2xpZGUge1xyXG4gICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDM2cHg7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA2cHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICB3aWR0aDogYXV0bztcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGJvcmRlci10b3Atd2lkdGg6IDA7XHJcbiAgICAgICAgICAgIGJvcmRlci1sZWZ0LXdpZHRoOiAwO1xyXG4gICAgICAgICAgICBib3JkZXItcmlnaHQtd2lkdGg6IDA7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b20tc3R5bGU6IHNvbGlkO1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tLXdpZHRoOiAycHg7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b20tY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiA1NnB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2VsZWN0ZWQge1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tLWNvbG9yOiB2YXIoLS1pb24tcHJpbWFyeS1jb2xvcik7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcihhY2NlbnQtY29sb3IpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpb24tZ3JpZCB7XHJcbiAgICAgICAgaW9uLXJvdyB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiLnNsaWRpbmctdGFicyAuZmlyc3Qtc2xpZGUge1xuICB3aWR0aDogODBweCAhaW1wb3J0YW50O1xufVxuLnNsaWRpbmctdGFicyBpb24tc2xpZGVzIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xufVxuLnNsaWRpbmctdGFicyBpb24tc2xpZGVzIGlvbi1zbGlkZSB7XG4gIGhlaWdodDogYXV0bztcbiAgZGlzcGxheTogaW5saW5lLWdyaWQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBib3JkZXItdG9wLXdpZHRoOiAwO1xuICBib3JkZXItbGVmdC13aWR0aDogMDtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiAwO1xuICBib3JkZXItYm90dG9tLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLWJvdHRvbS13aWR0aDogMnB4O1xuICBib3JkZXItYm90dG9tLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6IHZhcigtLWlvbi10ZXh0LWNvbG9yKTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWluLWhlaWdodDogNTZweDtcbn1cbi5zbGlkaW5nLXRhYnMgaW9uLXNsaWRlcyBpb24tc2xpZGUgaW1nIHtcbiAgd2lkdGg6IDMycHg7XG4gIGhlaWdodDogMzZweDtcbiAgcGFkZGluZzogNnB4O1xuICBtYXJnaW46IGF1dG87XG59XG4uc2xpZGluZy10YWJzIGlvbi1zbGlkZXMgLnNlbGVjdGVkIHtcbiAgYm9yZGVyLWJvdHRvbS1jb2xvcjogdmFyKC0taW9uLXByaW1hcnktY29sb3IpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoYWNjZW50LWNvbG9yKTtcbn1cbi5zbGlkaW5nLXRhYnMgaW9uLWdyaWQgaW9uLXJvdyB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/components/sliding-tabs/sliding-tabs.component.ts":
  /*!***************************************************************!*\
    !*** ./src/components/sliding-tabs/sliding-tabs.component.ts ***!
    \***************************************************************/

  /*! exports provided: SlidingTabsComponent */

  /***/
  function srcComponentsSlidingTabsSlidingTabsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SlidingTabsComponent", function () {
      return SlidingTabsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");

    var SlidingTabsComponent = /*#__PURE__*/function () {
      function SlidingTabsComponent(shared, config, loading) {
        _classCallCheck(this, SlidingTabsComponent);

        this.shared = shared;
        this.config = config;
        this.loading = loading;
        this.sliderConfig = {
          slidesPerView: "auto"
        };
        this.products = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.selected = 0;
        this.page = 0;
        this.httpRunning = true;
      }

      _createClass(SlidingTabsComponent, [{
        key: "getProducts",
        value: function getProducts(infiniteScroll) {
          var _this11 = this;

          this.httpRunning = true;

          if (this.page == 0) {
            this.products = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
          }

          var catId = this.selected;
          if (this.selected == 0) catId = '';
          var dat = {};
          dat.customers_id = null;
          dat.categories_id = this.selected;
          dat.page_number = this.page; // if (d.type != undefined)
          //   data.type = d.type;

          dat.language_id = this.config.langId;
          dat.currency_code = this.config.currecnyCode;
          this.config.postHttp('getallproducts', dat).then(function (data) {
            _this11.httpRunning = false;

            _this11.infinite.complete();

            if (_this11.page == 0) {
              _this11.products = new Array(); // this.loading.hide();
            }

            if (data.success == 1) {
              _this11.page++;
              var prod = data.product_data;

              var _iterator4 = _createForOfIteratorHelper(prod),
                  _step4;

              try {
                for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
                  var value = _step4.value;

                  _this11.products.push(value);
                }
              } catch (err) {
                _iterator4.e(err);
              } finally {
                _iterator4.f();
              }
            }

            if (data.success == 0) {
              _this11.infinite.disabled = true;
            }
          }); // console.log(this.products.length + "   " + this.page);
        } //changing tab

      }, {
        key: "changeTab",
        value: function changeTab(c) {
          this.infinite.disabled = false;
          this.page = 0;
          if (c == '0') this.selected = c;else this.selected = c.id;
          this.getProducts(null); //this.loading.autoHide(700);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.getProducts(null);
        }
      }]);

      return SlidingTabsComponent;
    }();

    SlidingTabsComponent.ctorParameters = function () {
      return [{
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonInfiniteScroll"], {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonInfiniteScroll"])], SlidingTabsComponent.prototype, "infinite", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('type'), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], SlidingTabsComponent.prototype, "type", void 0);
    SlidingTabsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sliding-tabs',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sliding-tabs.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/components/sliding-tabs/sliding-tabs.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sliding-tabs.component.scss */
      "./src/components/sliding-tabs/sliding-tabs.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]])], SlidingTabsComponent);
    /***/
  },

  /***/
  "./src/components/timer/timer.component.scss":
  /*!***************************************************!*\
    !*** ./src/components/timer/timer.component.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcComponentsTimerTimerComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvY29tcG9uZW50cy90aW1lci90aW1lci5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/components/timer/timer.component.ts":
  /*!*************************************************!*\
    !*** ./src/components/timer/timer.component.ts ***!
    \*************************************************/

  /*! exports provided: TimerComponent */

  /***/
  function srcComponentsTimerTimerComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TimerComponent", function () {
      return TimerComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var TimerComponent = /*#__PURE__*/function () {
      function TimerComponent(appEventsService) {
        _classCallCheck(this, TimerComponent);

        this.appEventsService = appEventsService;
      }

      _createClass(TimerComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          //console.log(this.timeInSeconds);
          var seconds = this.product.flash_expires_date;
          if (seconds == 0) this.productIsExpired();else {
            this.timeInSeconds = seconds - this.product.server_time;
          } //console.log(this.timeInSeconds);

          this.initTimer();
          this.startTimer();
        }
      }, {
        key: "productIsExpired",
        value: function productIsExpired() {
          this.appEventsService.publish('productExpired', this.product.products_id);
        }
      }, {
        key: "hasFinished",
        value: function hasFinished() {
          return this.timer.hasFinished;
        }
      }, {
        key: "initTimer",
        value: function initTimer() {
          if (!this.timeInSeconds) {
            this.timeInSeconds = 0;
          }

          this.timer = {
            seconds: this.timeInSeconds,
            runTimer: false,
            hasStarted: false,
            hasFinished: false,
            secondsRemaining: this.timeInSeconds
          };
          this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
        }
      }, {
        key: "startTimer",
        value: function startTimer() {
          this.timer.hasStarted = true;
          this.timer.runTimer = true;
          this.timerTick();
        }
      }, {
        key: "pauseTimer",
        value: function pauseTimer() {
          this.timer.runTimer = false;
        }
      }, {
        key: "resumeTimer",
        value: function resumeTimer() {
          this.startTimer();
        }
      }, {
        key: "timerTick",
        value: function timerTick() {
          var _this12 = this;

          setTimeout(function () {
            if (!_this12.timer.runTimer) {
              return;
            }

            _this12.timer.secondsRemaining--;
            _this12.timer.displayTime = _this12.getSecondsAsDigitalClock(_this12.timer.secondsRemaining);

            if (_this12.timer.secondsRemaining > 0) {
              _this12.timerTick();
            } else {
              _this12.productIsExpired();

              _this12.timer.hasFinished = true; //console.log(this.id);
            }
          }, 1000);
        }
      }, {
        key: "getSecondsAsDigitalClock",
        value: function getSecondsAsDigitalClock(inputSeconds) {
          var secNum = parseInt(inputSeconds.toString(), 10); // don't forget the second param

          var hours = Math.floor(secNum / 3600);
          var minutes = Math.floor((secNum - hours * 3600) / 60);
          var seconds = secNum - hours * 3600 - minutes * 60;
          var hoursString = '';
          var minutesString = '';
          var secondsString = '';
          hoursString = hours < 10 ? '0' + hours : hours.toString();
          minutesString = minutes < 10 ? '0' + minutes : minutes.toString();
          secondsString = seconds < 10 ? '0' + seconds : seconds.toString();
          return hoursString + 'h:' + minutesString + 'm:' + secondsString + 's';
        }
      }]);

      return TimerComponent;
    }();

    TimerComponent.ctorParameters = function () {
      return [{
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_2__["AppEventsService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('data'), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], TimerComponent.prototype, "product", void 0);
    TimerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-timer',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./timer.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/components/timer/timer.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./timer.component.scss */
      "./src/components/timer/timer.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_2__["AppEventsService"]])], TimerComponent);
    /***/
  },

  /***/
  "./src/providers/user-address/user-address.service.ts":
  /*!************************************************************!*\
    !*** ./src/providers/user-address/user-address.service.ts ***!
    \************************************************************/

  /*! exports provided: UserAddressService */

  /***/
  function srcProvidersUserAddressUserAddressServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserAddressService", function () {
      return UserAddressService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic-native/native-geocoder/ngx */
    "./node_modules/@ionic-native/native-geocoder/ngx/index.js");
    /* harmony import */


    var _config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");

    var UserAddressService = /*#__PURE__*/function () {
      function UserAddressService(config, geolocation, nativeGeocoder) {
        _classCallCheck(this, UserAddressService);

        this.config = config;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        console.log('Hello UserAddressProvider Provider');
      }

      _createClass(UserAddressService, [{
        key: "getCordinates",
        value: function getCordinates() {
          var _this13 = this;

          return new Promise(function (resolve) {
            _this13.geolocation.getCurrentPosition().then(function (resp) {
              // resp.coords.latitude
              // resp.coords.longitude
              console.log(resp);
              resolve({
                "lat": resp.coords.latitude,
                "long": resp.coords.longitude
              });
            })["catch"](function (error) {
              console.log('Error getting location', error);
              resolve("error");
            });
          });
        }
      }, {
        key: "getAddress",
        value: function getAddress() {
          var _this14 = this;

          return new Promise(function (resolve) {
            var options = {
              useLocale: true,
              maxResults: 5
            };

            _this14.getCordinates().then(function (value) {
              _this14.nativeGeocoder.reverseGeocode(value.lat, value["long"], options).then(function (result) {
                resolve(result[0]);
                console.log(result[0]);
              })["catch"](function (error) {
                console.log(error);
                resolve("error");
              });
            });
          });
        }
      }]);

      return UserAddressService;
    }();

    UserAddressService.ctorParameters = function () {
      return [{
        type: _config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"]
      }, {
        type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_2__["NativeGeocoder"]
      }];
    };

    UserAddressService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"], _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_2__["NativeGeocoder"]])], UserAddressService);
    /***/
  }
}]);
//# sourceMappingURL=default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f-es5.js.map