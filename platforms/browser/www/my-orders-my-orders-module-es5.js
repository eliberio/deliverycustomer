function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-orders-my-orders-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/my-orders/my-orders.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/my-orders/my-orders.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMyOrdersMyOrdersPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-buttons slot=\"start\" *ngIf=\"config.appNavigationTabs\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title> {{'Custom Orders'| translate }} </ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button fill=\"clear\" (click)=\"refreshPage()\">\r\n        <ion-icon name=\"refresh\"></ion-icon>\r\n      </ion-button>\r\n\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid class=\"ion-no-padding grid\" *ngIf=\"orders.length==0 && httpRunning==false\">\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col size=\"12\" class=\"ion-no-padding\">\r\n        <ion-icon name=\"briefcase\"></ion-icon>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col size=\"12\" class=\"ion-no-padding\">\r\n        <h4>{{'Your Order List is Empty'|translate}}</h4>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col size=\"12\" class=\"ion-no-padding\">\r\n        <h5>{{'continue shopping'|translate}}</h5>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col size=\"12\" class=\"ion-no-padding\">\r\n        <ion-button (click)=\"openProductsPage()\">{{'Explore'|translate}}</ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <ion-grid>\r\n    <ion-card *ngFor=\"let order of orders\" (click)=\"showOrderDetail(order)\">\r\n      <ion-card-content>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            {{'Orders ID'|translate}}\r\n          </ion-col>\r\n          <ion-col class=\"ion-text-right\" col-6>\r\n            {{'#'+order.orders_id}}\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            {{'Date'|translate}}\r\n          </ion-col>\r\n          <ion-col class=\"ion-text-right\" size=\"6\">\r\n            {{order.last_modified|date}}\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            {{'Price'|translate}}\r\n          </ion-col>\r\n          <ion-col class=\"ion-text-right\" size=\"6\">\r\n            {{order.order_price|curency}}\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row class=\"pending\">\r\n          <ion-col size=\"6\">\r\n            {{'Status'|translate}}\r\n          </ion-col>\r\n          <ion-col class=\"ion-text-right\" size=\"6\">\r\n            <strong>{{order.orders_status}}</strong>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-grid>\r\n  <!-- <ion-infinite-scroll #infinite (ionInfinite)=\"getOrders()\">\r\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\r\n  </ion-infinite-scroll> -->\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/my-orders/my-orders.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/my-orders/my-orders.module.ts ***!
    \***********************************************/

  /*! exports provided: MyOrdersPageModule */

  /***/
  function srcAppMyOrdersMyOrdersModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyOrdersPageModule", function () {
      return MyOrdersPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _my_orders_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./my-orders.page */
    "./src/app/my-orders/my-orders.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _my_orders_page__WEBPACK_IMPORTED_MODULE_6__["MyOrdersPage"]
    }];

    var MyOrdersPageModule = function MyOrdersPageModule() {
      _classCallCheck(this, MyOrdersPageModule);
    };

    MyOrdersPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_my_orders_page__WEBPACK_IMPORTED_MODULE_6__["MyOrdersPage"]]
    })], MyOrdersPageModule);
    /***/
  },

  /***/
  "./src/app/my-orders/my-orders.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/my-orders/my-orders.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppMyOrdersMyOrdersPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content .grid {\n  margin-top: 50%;\n}\nion-content .grid ion-row ion-col {\n  text-align: center;\n}\nion-content .grid ion-row ion-col ion-icon {\n  zoom: 3.9;\n  color: lightgrey;\n}\nion-content .grid ion-row ion-col ion-button {\n  --border-radius: 0px;\n}\nion-content .grid ion-row ion-col h4 {\n  font-size: 16px;\n  font-weight: bold;\n  margin-top: 2px;\n  margin-bottom: 6px;\n}\nion-content .grid ion-row ion-col h5 {\n  font-size: 14px;\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n  margin-top: 0;\n}\nion-content ion-grid ion-card {\n  color: var(--ion-text-color);\n}\n.pending {\n  background-color: var(--ion-color-primary);\n  color: white;\n}\n.cancel {\n  background-color: red;\n  color: white;\n}\n.complete {\n  background-color: green;\n  color: white;\n}\n.inprocess {\n  background-color: green;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktb3JkZXJzL0Q6XFxEb2N1bWVudG9zXFxQcm9ncmFtYWNpb25cXEphdmFzY3JpcHRcXElvbmljXFxkZWxpdmVyeWN1c3RvbWVyL3NyY1xcYXBwXFxteS1vcmRlcnNcXG15LW9yZGVycy5wYWdlLnNjc3MiLCJzcmMvYXBwL215LW9yZGVycy9teS1vcmRlcnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVFO0VBQ0UsZUFBQTtBQ0RKO0FER007RUFDRSxrQkFBQTtBQ0RSO0FERVE7RUFDRSxTQUFBO0VBQ0EsZ0JBQUE7QUNBVjtBREVRO0VBQ0Usb0JBQUE7QUNBVjtBREVRO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQVY7QURFUTtFQUNFLGVBQUE7RUFDQSwyQ0FBQTtFQUNBLGFBQUE7QUNBVjtBRE1JO0VBQ0UsNEJBQUE7QUNKTjtBRFNBO0VBQ0UsMENBQUE7RUFDQSxZQUFBO0FDTkY7QURRQTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtBQ0xGO0FEUUE7RUFDRSx1QkFBQTtFQUNBLFlBQUE7QUNMRjtBRE9BO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FDSkYiLCJmaWxlIjoic3JjL2FwcC9teS1vcmRlcnMvbXktb3JkZXJzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pb24tY29udGVudCB7XHJcbiAgLmdyaWQge1xyXG4gICAgbWFyZ2luLXRvcDogNTAlO1xyXG4gICAgaW9uLXJvdyB7XHJcbiAgICAgIGlvbi1jb2wge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgICB6b29tOiAzLjk7XHJcbiAgICAgICAgICBjb2xvcjogbGlnaHRncmV5O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpb24tYnV0dG9uIHtcclxuICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBoNCB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDZweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaDUge1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIGlvbi1ncmlkIHtcclxuICAgIGlvbi1jYXJkIHtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi10ZXh0LWNvbG9yKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5wZW5kaW5nIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi5jYW5jZWwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgLy8gY29sb3I6IGNvbG9yKCRjb2xvcnMsIGRhbmdlcik7XHJcbn1cclxuLmNvbXBsZXRlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLmlucHJvY2VzcyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW47XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcbiIsImlvbi1jb250ZW50IC5ncmlkIHtcbiAgbWFyZ2luLXRvcDogNTAlO1xufVxuaW9uLWNvbnRlbnQgLmdyaWQgaW9uLXJvdyBpb24tY29sIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaW9uLWNvbnRlbnQgLmdyaWQgaW9uLXJvdyBpb24tY29sIGlvbi1pY29uIHtcbiAgem9vbTogMy45O1xuICBjb2xvcjogbGlnaHRncmV5O1xufVxuaW9uLWNvbnRlbnQgLmdyaWQgaW9uLXJvdyBpb24tY29sIGlvbi1idXR0b24ge1xuICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbn1cbmlvbi1jb250ZW50IC5ncmlkIGlvbi1yb3cgaW9uLWNvbCBoNCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbi10b3A6IDJweDtcbiAgbWFyZ2luLWJvdHRvbTogNnB4O1xufVxuaW9uLWNvbnRlbnQgLmdyaWQgaW9uLXJvdyBpb24tY29sIGg1IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xuICBtYXJnaW4tdG9wOiAwO1xufVxuaW9uLWNvbnRlbnQgaW9uLWdyaWQgaW9uLWNhcmQge1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xufVxuXG4ucGVuZGluZyB7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uY2FuY2VsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5jb21wbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pbnByb2Nlc3Mge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcbiAgY29sb3I6IHdoaXRlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/my-orders/my-orders.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/my-orders/my-orders.page.ts ***!
    \*********************************************/

  /*! exports provided: MyOrdersPage */

  /***/
  function srcAppMyOrdersMyOrdersPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyOrdersPage", function () {
      return MyOrdersPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");

    var MyOrdersPage = /*#__PURE__*/function () {
      function MyOrdersPage(navCtrl, http, config, shared, loading, applicationRef) {
        _classCallCheck(this, MyOrdersPage);

        this.navCtrl = navCtrl;
        this.http = http;
        this.config = config;
        this.shared = shared;
        this.loading = loading;
        this.applicationRef = applicationRef;
        this.page = 1;
        this.orders = new Array();
        this.httpRunning = true;
      }

      _createClass(MyOrdersPage, [{
        key: "refreshPage",
        value: function refreshPage() {
          this.page = 1; //this.infinite.disabled = false;

          this.getOrders();
        }
      }, {
        key: "addCurrecny",
        value: function addCurrecny(order, v2) {
          return order.currency + " " + v2;
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.httpRunning = true;
          this.getOrders();
        }
      }, {
        key: "getOrders",
        value: function getOrders() {
          var _this = this;

          this.httpRunning = true;
          this.orders = [];
          this.loading.show();
          var dat = {};
          dat.customers_id = this.shared.customerData.customers_id;
          dat.language_id = this.config.langId;
          dat.currency_code = this.config.currecnyCode;
          this.config.postHttp('getorders', dat).then(function (data) {
            _this.loading.hide();

            _this.httpRunning = false; //$rootScope.address=response.data.data;

            if (data.success == 1) {
              _this.orders = [];
              _this.orders = data.data;
            } // $scope.$broadcast('scroll.refreshComplete');

          }, function (response) {
            this.loading.hide();
            this.shared.toast("Server Error while Loading Orders");
            console.log(response);
          });
        }
      }, {
        key: "showOrderDetail",
        value: function showOrderDetail(order) {
          this.shared.myOrderDetialPageData = order;
          this.navCtrl.navigateForward(this.config.currentRoute + "/my-order-detail");
        }
      }, {
        key: "openProductsPage",
        value: function openProductsPage() {
          if (this.config.appNavigationTabs) this.navCtrl.navigateForward("tabs/" + this.config.getCurrentHomePage());else this.navCtrl.navigateForward(this.config.getCurrentHomePage());
        }
      }]);

      return MyOrdersPage;
    }();

    MyOrdersPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"], {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])], MyOrdersPage.prototype, "infinite", void 0);
    MyOrdersPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-my-orders',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./my-orders.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/my-orders/my-orders.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./my-orders.page.scss */
      "./src/app/my-orders/my-orders.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"]])], MyOrdersPage);
    /***/
  }
}]);
//# sourceMappingURL=my-orders-my-orders-module-es5.js.map